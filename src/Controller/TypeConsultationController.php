<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\TypeConsultation;
use App\Entity\TypeConsultationGlobal;
use App\Repository\ActeRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\DureeRepository;
use App\Repository\FamilleActeRepository;
use App\Repository\TypeConsultationGlobalRepository;
use App\Repository\TypeConsultationRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class TypeConsultationController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ActeRepository
     */
    private $acteRepository;

    public function __construct(EntityManagerInterface $em,ActeRepository $acteRepository,
                                CentreSoinRepository $centreSoinRepository,
                                FamilleActeRepository $familleActeRepository,
                                DureeRepository $dureeRepository,
                                TypeConsultationGlobalRepository $typeConsultationGlobalRepository,
                                TypeConsultationRepository $typeConsultationRepository
                                )
    {
        $this->em = $em;
        $this->acteRepository = $acteRepository;
        $this->dureeRepository = $dureeRepository;
        $this->centreSoinRepository = $centreSoinRepository;
        $this->typeConsultationGlobalRepository = $typeConsultationGlobalRepository;
        $this->typeConsultationRepository = $typeConsultationRepository;
        $this->familleActeRepository = $familleActeRepository;
    }
    //voilaDoc
    /**
     * @Route("/type_consultations/list", name="type_consultations_list")
     */
    public function listType(TypeConsultationRepository $repo): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$repo->findAll();
        
       return $this->json($actes);
    }

    /**
     * @Route("/type_consultations/global/{libelle}", name="type_consultations_libelle",methods={"GET"})
     */
    public function typeConsultationByLibelle($libelle): Response
    {
        $typeConsultation = $this->typeConsultationGlobalRepository->findByLibelle($libelle);
        $typeConsultation = array_shift($typeConsultation);
        //dd($typeConsultation);
        return $this->json(['id_global'=>$typeConsultation->getId(),
                            'libelle'=>$typeConsultation->getLibelle()]);
    }

    /**
     * @Route("/type_consultations/{centreSoin}/list", name="type_consultations_all_list")
     */
    public function listTypeByCentreSoin($centreSoin): Response
    {
       $typeConsultation =$this->typeConsultationGlobalRepository->findAllByCentreSoin($centreSoin);
       
       return $this->json($typeConsultation);
      // return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
    }

    


    /**
     * @Route("/type_consultations/local/delete/{id}", name="type_consultations_global_delete",methods={"GET"})
     * 
     */
    public function deleteLocal( Request $request, TypeConsultation $typeConsultation ,ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        //dd($typeConsultation);
        $typeConsultation->setActive(false);   
        $this->em->persist($typeConsultation);
        $this->em->flush();
        return $this->json(['id_global'=>$typeConsultation->getId(),
                            //'libelle'=>$typeConsultation->getLibelle()
                        ]);
    }
    /**
     * @Route("/type_consultations/global/insert", name="type_consultations_global_insert",methods={"POST","PUT"})
     * @Route("/type_consultations/global/update/{id}", name="type_consultations_global_update",methods={"POST","PUT"})
     * 
     */
    public function insertGlobal(Request $request, TypeConsultationGlobal $typeConsultation = null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        if(!$typeConsultation){
            $typeConsultation = new TypeConsultationGlobal();
        }
        $typeConsultation->setLibelle($request->get('libelle'));   
        $this->em->persist($typeConsultation);
        $this->em->flush();
        return $this->json(['id_global'=>$typeConsultation->getId(),
                            'libelle'=>$typeConsultation->getLibelle()]);
    }

        /**
     * @Route("/type_consultations/local/insert", name="type_consultations_local_insert",methods={"POST","PUT"})
     * @Route("/type_consultations/local/update/{id}", name="type_consultations_local_update",methods={"POST","PUT"})
     * 
     */
    public function insertLocal(Request $request, TypeConsultation $typeConsultation = null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $familleActe = $request->get('familleActe');
        $id_global = $request->get('id_global');
        $centreSoin = $request->get('centreSoin');
        $duree = $request->get('duree');
        if(!$typeConsultation){
            $typeConsultation = new TypeConsultation();
        }
        if($familleActe){
            $famille = $this->getFamilleActe($familleActe);
            $typeConsultation->setFamilleActe($famille);
            $familleActe = $typeConsultation->getFamilleActe()->getId();
        }
       
        if($id_global){
            $typeConsultationGlobal = $this->getTypeConsultationGlobal($id_global);
            $typeConsultation->setIdGlobal($typeConsultationGlobal);
        }
        if($centreSoin){
            $centre = $this->getCentreSoin($centreSoin);
            $typeConsultation->setCentreSoin($centre);
        }
        if($duree){
            $dure = $this->getDuree($duree);
            $typeConsultation->setDuree($dure);
        }
           
        $typeConsultation->setCouleur($request->get('couleur'));        
        $typeConsultation->setActive($request->get('active'));        
        $typeConsultation->setTarif($request->get('tarif'));        
        $this->em->persist($typeConsultation);
        $this->em->flush();
        return $this->json([
            'libelle'=>$typeConsultation->getIdGlobal()->getLibelle(),
            'duree'=>$typeConsultation->getDuree()->getId(),
            'duree_name'=>$typeConsultation->getDuree()->getName(),
            'couleur'=>$typeConsultation->getCouleur(),
            'tarif'=>$typeConsultation->getTarif(),
            'familleActe'=>$familleActe,
            'id_global'=>$id_global,
            'id_local'=>$typeConsultation->getId()
            
            ]);
    }


    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }
    function getFamilleActe($familleActe){
        return $this->familleActeRepository->find($familleActe);
    }

    function getDuree($duree){
        return $this->dureeRepository->find($duree);
    }
    function getTypeConsultationGlobal($id){
        return $this->typeConsultationGlobalRepository->find($id);
    }
}