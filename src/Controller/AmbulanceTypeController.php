<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\AmbulanceTypeRepository;

class AmbulanceTypeController extends AbstractController
{
    

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var AmbulanceTypeRepository
     */
    private $ambulanceTypeRepository;

    public function __construct(EntityManagerInterface $em,AmbulanceTypeRepository $ambulanceTypeRepository)
    {
        $this->em = $em;
        $this->ambulanceTypeRepository = $ambulanceTypeRepository;
    }

    /**
     * @Route("/ambulance/type/list", name="ambulance_type")
    */
    public function list()
    {
        $allTypes = $this->ambulanceTypeRepository->findAll();
        return $this->json($allTypes);
    }
}
