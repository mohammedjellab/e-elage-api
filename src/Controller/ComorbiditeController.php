<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\Comorbidite;
use App\Repository\ActeRepository;
use App\Repository\ComorbiditeRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class ComorbiditeController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ComorbiditeRepository
     */
    private $comorbiditeRepository;

    public function __construct(EntityManagerInterface $em,ComorbiditeRepository $comorbiditeRepository)
    {
        $this->em = $em;
        $this->comorbiditeRepository = $comorbiditeRepository;
    }

    /**
     * @Route("/comorbidites/search/{term}", name="comorbidite_search")
     */
    public function search($term): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$this->comorbiditeRepository->searchByName($term);
        
       return $this->json($actes);
    }


    /**
     * @Route("/comorbidites/list", name="comorbidite_list")
     */
    public function list(): Response
    {
        $comorbidites =$this->comorbiditeRepository->findAllComorbidite();
        return $this->json($comorbidites);
    }

     /**
     * @Route("/comorbidites/insert", name="comorbidites_insert",methods={"POST"})
     * @Route("/comorbidites/update/{id}", name="comorbidites_update",methods={"POST","PUT"})
     */
    public function insert(Request $request,Comorbidite $comorbidite=null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        
        if(!$comorbidite){
            $comorbidite = new Comorbidite();
        }
        $comorbidite->setLibelle($libelle);
        //$comorbidite->setActive($request->get('active'));
        $this->em->persist($comorbidite);
        $this->em->flush();
        return $this->json([
            'libelle'=>$comorbidite->getLibelle(),
            'id'=>$comorbidite->getId()
        ]);
    }

    /**
     * @Route("/comorbidites/delete/{id}",name="comorbidites_delete", methods={"GET"})
     */
    public function delete(Comorbidite $comorbidite ): Response {
        //$comorbidite->setActive(false);
        $this->em->persist($comorbidite);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json([
            'libelle'=>$comorbidite->getLibelle(),
            'id'=>$comorbidite->getId()
        ]);
    }

}