<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\Consultation;
use App\Entity\ConsultationHasReglemnt;
use App\Entity\Document;
use App\Entity\GPatient;
use App\Entity\LPatient;
use App\Entity\Patient;
use App\Entity\Reglement;
use App\Repository\ActeRepository;
use App\Repository\AgendaEvenementRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\ConsultationRepository;
use App\Repository\DocumentRepository;
use App\Repository\DossierMedicalRepository;
use App\Repository\GPatientRepository;
use App\Repository\LPatientRepository;
use App\Repository\MutuelleRepository;
use App\Repository\PatientConfigCentreRepository;
use App\Repository\PatientRepository;
use App\Repository\PaysRepository;
use App\Repository\ReglementRepository;
use App\Repository\UserRepository;
use App\Repository\VilleRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class RdvReglementController extends AbstractController
{

 



    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var LPatientRepository
     */
    private $lpatientRepository;
    
    /**
     * @var ReglementRepository
     */
    private $reglementRepository;


    /**
     * @var AgendaEvenementRepository
     */
    private $agendaEvenementRepository;

    /**
     * @var CentreSoinRepository
     */
    private $centreSoinRepository;

    private $apiConnector;

    public function __construct(EntityManagerInterface $em,
                                LPatientRepository $lpatientRepository,
                                ReglementRepository $reglementRepository,
                                AgendaEvenementRepository $agendaEvenementRepository,
                                CentreSoinRepository $centreSoinRepository,
                                ConsultationRepository $consultationRepository,
                                ApiConnector $apiConnector)
    {
        $this->em = $em;
        $this->lpatientRepository = $lpatientRepository;
        $this->consultationRepository = $consultationRepository;
        $this->reglementRepository = $reglementRepository;
        $this->agendaEvenementRepository = $agendaEvenementRepository;
        $this->centreSoinRepository = $centreSoinRepository;
        $this->apiConnector = $apiConnector;
    }



    /**
     * @Route("/rdvpaiement/insert", name="rdvpaiement_persist",methods={"POST"})
     */
    public function persist(Request $request,ApiConnector $apiConnector): Response
    { 
        $request = $apiConnector->transformJsonBody($request);
        $centreSoin = $request->get('centreSoin');
        $dateReglement = $request->get('dateReglement');
        $montant = $request->get('montant');
        $lpatient = $request->get('patient');
        $typePaiement = $request->get('typePaiement');
        $consultations = $request->get('consultations');
        
        $reglement = new Reglement();
        if($centreSoin){
            $centre = $this->getCentreSoin($centreSoin);
            $reglement->setCentreSoin($centre);
        }

        if($lpatient){
            $patient = $this->getPatient($lpatient);
            $reglement->setLpatient($patient);
        }

        $reglement->setMontantRegle($montant);
        $reglement->setTypePaiement($typePaiement);
        if($dateReglement){
            $dateReglement = strtotime($dateReglement);
            $dateReglement = date('Y-m-d h:i:s', $dateReglement);
            $dateReglement = \DateTime::createFromFormat('Y-m-d h:i:s', $dateReglement);
            $reglement->setDateRegement($dateReglement);
        }
        
        $this->em->persist($reglement);
        $this->em->flush();
        if(count($consultations)>1){
            foreach($consultations as $key => $consult){
                /*$consultation = $this->getConsultation($consult);
                $reglement->addConsultation($consultation);
                $this->em->persist($reglement);
                $this->em->flush();*/
                $consultation = $this->getConsultation($consult);
                
                $consultationHasReglement = new ConsultationHasReglemnt();
                $consultationHasReglement->setHasConsultation($reglement);
                $consultationHasReglement->setHasReglement($consultation);
                $mt = $consultation->getMontant();
                if($mt<=$montant){
                    $consultationHasReglement->setMontantRegle($mt);
                    $montant = $montant - $mt;
                    
                }else{
                    $consultationHasReglement->setMontantRegle($montant);
                }
                
                $this->em->persist($consultationHasReglement);
                $this->em->flush();
            }
        }else{
            $consultations =array_shift($consultations);
            /*$consultation = $this->getConsultation($consultations);
            $reglement->addConsultation($consultation);
            $this->em->persist($reglement);
            $this->em->flush();*/
            $consultation = $this->getConsultation($consultations);
            $consultationHasReglement = new ConsultationHasReglemnt();
            $consultationHasReglement->setHasConsultation($reglement);
            $consultationHasReglement->setHasReglement($consultation);
            $consultationHasReglement->setMontantRegle($montant);
            $this->em->persist($consultationHasReglement);
            $this->em->flush();
        }
        
        //$patients =$this->patientRepository->getPatients();
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json([
                "id"=>$reglement->getId()
               ]);
    }

    /**
     * @Route("/consultation/situation/{lpatient_id}", name="rdvpaiement_information_consultation_id")
     */
    public function situationFinanciere($lpatient_id)
    { 
        $infors =$this->consultationRepository
                      ->situationFinanciere($lpatient_id);
        
        return $this->json($infors);
    }


    /**
     * @Route("/consultation/reglement/interventions", name="rdvpaiement_information_interventions",methods={"POST"})
     */
    public function mesInterventions(Request $request,ApiConnector $apiConnector)
    { 
        $request = $apiConnector->transformJsonBody($request);
        $date = $request->get('date');
        $typeConsultation = $request->get('typeConsultation');
        
        if($date){
            $date = substr($date,0,10);
        }
        $infors =$this->reglementRepository
                      ->mesInterventions($typeConsultation,$date);
        
        return $this->json($infors);
    }
    /**
     * @Route("/consultation/reglement/{lpatient_id}", name="reglement_information_consultation_id",methods={"GET"})
     */
    public function listReglement($lpatient_id)
    { 
        $infors =$this->consultationRepository
                      ->listReglement($lpatient_id);
        return $this->json($infors);
    }
    /**
     * @Route("/rdvpaiement/agenda/{rdv}", name="reglement_information_agenda_rdv",methods={"GET"})
     */
    public function reglementByRdv($rdv)
    { 
        $infors =$this->consultationRepository
                      ->reglementByRdv($rdv);
        return $this->json($infors);
    }

    /**
     * @Route("/consultation/reglement/list", name="reglement_information_search",methods={"POST"})
     */
    public function searchReglement(Request $request,ApiConnector $apiConnector)
    { 
        $request = $apiConnector->transformJsonBody($request);
        $date = $request->get('date');
        $typeConsultation = $request->get('typeConsultation');
        
        if($date){
            $date = substr($date,0,10);
        }
        $infors =$this->reglementRepository
                      ->searchReglement($typeConsultation,$date);
        
        return $this->json($infors);
    }
    

    function getRdv($rdv){
        return $this->agendaEvenementRepository->find($rdv);
    }

    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }

    function getPatient($patient){
        return $this->lpatientRepository->find($patient);
    }
    function getConsultation($consult){
        return $this->consultationRepository->find($consult);
    }
    function getReglemnt($reg){
        return $this->reglementRepository->find($reg);
    }

    /*function getTypePaiement($type){
        return $this->centreSoinRepository->find($centreSoin);
    }*/

   
    

}