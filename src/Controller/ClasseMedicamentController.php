<?php

namespace App\Controller;

use App\Entity\ClasseMedica;
use App\Repository\ClasseMedicaRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/classemedicaments")
 */
class ClasseMedicamentController extends AbstractController
{
    
     /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;


    /**
     * @var ClasseMedicaRepository
     */
    private $classeMedicaRepository;

    public function __construct(EntityManagerInterface $em, ClasseMedicaRepository $classeMedicaRepos, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->classeMedicaRepository = $classeMedicaRepos;
    }

    /**
     * @Route("/", name="classe-medicament", methods={"GET"})
     */
    public function index(): Response
    {
        $classeMedicaments = $this->classeMedicaRepository->findAll();
        if (empty($classeMedicaments) || count($classeMedicaments) == 0)
            return $this->json(['message' => 'Aucun enregistrement dans la base de données']);
        return $this->json($classeMedicaments);
    }

    /**
     * @Route("/active", name="classe-medicament-active")
     */
    public function getActives(): Response
    {
        $classeMedicaments = $this->classeMedicaRepository->findByActive();
        if (empty($classeMedicaments) || count($classeMedicaments) == 0)
            return $this->json(['message' => 'No ClasseMedica found']); /* Aucun enregistrement dans la base de données*/
        return $this->json($classeMedicaments);
    }

     /**
     * @Route("/add", name="add-classe-medicament", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        try {
            $classeMedicament = $this->serializer->deserialize($request->getContent(), ClasseMedica::class, 'json');
            $classeMedicament->setActive(true);
            $this->em->persist($classeMedicament);
            $this->em->flush();
            return $this->json([
                'message' => 'Successfully added',
                'data' => $classeMedicament
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/edit/{id}", name="update-classeMedicament", methods={"PUT"})
     */
    public function update(Request $request, $id): Response
    {
        try {
            $classeMedicament = $this->serializer->deserialize($request->getContent(), ClasseMedica::class, 'json');
            $searchedClasseMedica = $this->classeMedicaRepository->find($id);
            if (!$searchedClasseMedica) {
                throw $this->createNotFoundException(
                    'No classeMedicament found for id ' . $id
                );
            }

            $searchedClasseMedica->setLibelle($classeMedicament->getLibelle());
            $searchedClasseMedica->setActive(true);
            $this->em->flush($searchedClasseMedica);
            return $this->json([
                'message' => 'Successfully updated',
                'data' => $searchedClasseMedica
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/delete/{id}", name="delete-classeMedicament", methods={"PUT"})
     */
    public function delete($id): Response
    {
        try {
            $searchedClasseMedica = $this->classeMedicaRepository->find($id);
            if (!$searchedClasseMedica) {
                throw $this->createNotFoundException(
                    'No classeMedicament found for id ' . $id
                );
            }


            // $this->em->remove($searchedLabo);
            $searchedClasseMedica->setActive(false);
            $this->em->flush($searchedClasseMedica);
            return $this->json([
                'message' => 'The object with ID ' . $id . ' was successfully deleted'
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }
}
