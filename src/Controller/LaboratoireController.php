<?php

namespace App\Controller;

use App\Entity\Laboratoire;
use App\Repository\LaboratoireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Serializer\SerializerInterface;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @Route("/api/laboratoires")
 */
class LaboratoireController extends AbstractController
{

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;


    /**
     * @var LaboratoireRepository
     */
    private $laboRepository;

    public function __construct(EntityManagerInterface $em, LaboratoireRepository $laboRepos, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->laboRepository = $laboRepos;
    }

    /**
     * @Route("/", name="laboratoire", methods={"GET"})
     */
    public function index(): Response
    {
        $laboratoires = $this->laboRepository->findAll();
        if (empty($laboratoires) || count($laboratoires) == 0)
            return $this->json(['message' => 'Aucun enregistrement dans la base de données']);
        return $this->json($laboratoires);
    }

    /**
     * @Route("/active", name="active-laboratoire", methods={"GET"})
     */
    public function getActives(): Response
    {
        $laboratoires = $this->laboRepository->findByActive();
        if (empty($laboratoires) || count($laboratoires) == 0)
            return $this->json(['message' => 'No labo found']); /* Aucun enregistrement dans la base de données*/
        return $this->json($laboratoires);
    }

    /**
     * @Route("/{id}", name="laboratoire", methods={"GET"})
     */
    public function getLabo($id): Response
    {
        try {
            $laboratoire = $this->laboRepository->find($id);
            if (!$laboratoire)
                throw $this->createNotFoundException('No labo found for id ' . $id); // $this->json(['message' => 'Aucun enregistrement dans la base de données']);
            return $this->json($laboratoire);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/add", name="add-laboratoire", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        try {
            $laboratoire = $this->serializer->deserialize($request->getContent(), Laboratoire::class, 'json');
            $laboratoire->setActive(true);
            $this->em->persist($laboratoire);
            $this->em->flush();
            return $this->json([
                'message' => 'Successfully added',
                'data' => $laboratoire
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/edit/{id}", name="update-laboratoire", methods={"PUT"})
     */
    public function update(Request $request, $id): Response
    {
        try {
            $laboratoire = $this->serializer->deserialize($request->getContent(), Laboratoire::class, 'json');
            $searchedLabo = $this->laboRepository->find($id);
            if (!$searchedLabo) {
                throw $this->createNotFoundException(
                    'No labo found for id ' . $id
                );
            }

            $searchedLabo->setName($laboratoire->getName());
            $searchedLabo->setTel($laboratoire->getTel());
            $searchedLabo->setAdresse($laboratoire->getAdresse());
            $this->em->flush($searchedLabo);
            return $this->json([
                'message' => 'Successfully updated',
                'data' => $searchedLabo
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/delete/{id}", name="delete-laboratoire", methods={"PUT"})
     */
    public function delete($id): Response
    {
        try {
            $searchedLabo = $this->laboRepository->find($id);
            if (!$searchedLabo) {
                throw $this->createNotFoundException(
                    'No labo found for id ' . $id
                );
            }


            // $this->em->remove($searchedLabo);
            $searchedLabo->setActive(false);
            $this->em->flush($searchedLabo);
            return $this->json([
                'message' => 'The object with ID ' . $id . ' was successfully deleted'
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }
}
