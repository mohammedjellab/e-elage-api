<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\MotifConsultation;
use App\Repository\ActeRepository;
use App\Repository\MotifConsultationRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class MotifConsultationController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var MotifConsultationRepository
     */
    private $motifConsultationRepository;

    public function __construct(EntityManagerInterface $em,MotifConsultationRepository $motifConsultationRepository)
    {
        $this->em = $em;
        $this->motifConsultationRepository = $motifConsultationRepository;
    }

    /**
     * @Route("/motif_consultations/search/{term}", name="motif_consultations_search")
     */
    public function search($term): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$this->motifConsultationRepository->searchByName($term);
        
       return $this->json($actes);
    }


    /**
     * @Route("/motif_consultations/list", name="motif_consultations_list")
     */
    public function list(): Response
    {
        $motifs =$this->motifConsultationRepository->findAllMotif();
        return $this->json($motifs);
    }

     /**
     * @Route("/motif_consultations/insert", name="motif_consultations_insert",methods={"POST"})
     * @Route("/motif_consultations/update/{id}", name="motif_consultations_update",methods={"POST","PUT"})
     */
    public function insert(Request $request,MotifConsultation $mutuelle=null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        
        if(!$mutuelle){
            $mutuelle = new MotifConsultation();
        }
        $mutuelle->setLibelle($libelle);
        //$mutuelle->setActive($request->get('active'));
        $this->em->persist($mutuelle);
        $this->em->flush();
        return $this->json([
            'libelle'=>$mutuelle->getLibelle(),
            'id'=>$mutuelle->getId()
        ]);
    }

    /**
     * @Route("/motif_consultations/delete/{id}",name="motif_consultations_delete", methods={"GET"})
     */
    public function delete(MotifConsultation $mutuelle ): Response {
        $mutuelle->setActive(false);
        $this->em->persist($mutuelle);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json([
            'libelle'=>$mutuelle->getLibelle(),
            'id'=>$mutuelle->getId()
        ]);
    }

}