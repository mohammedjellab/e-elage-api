<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Repository\ActeRepository;
use App\Repository\MutuelleRepository;
use App\Repository\PaysRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class PaysController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var PaysRepository
     */
    private $paysRepository;

    public function __construct(EntityManagerInterface $em,PaysRepository $paysRepository)
    {
        $this->em = $em;
        $this->paysRepository = $paysRepository;
    }

    /**
     * @Route("/pays/search/{term}", name="pays_search")
     */
    public function search($term): Response
    {
        $pays =$this->paysRepository->searchByName($term);
        
       return $this->json($pays);
    }



}