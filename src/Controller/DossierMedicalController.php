<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\Comorbidite;
use App\Entity\Document;
use App\Entity\DossierMedical;
use App\Entity\GPatient;
use App\Entity\LPatient;
use App\Entity\Patient;
use App\Repository\ActeRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\DocumentRepository;
use App\Repository\DossierMedicalRepository;
use App\Repository\GPatientRepository;
use App\Repository\LPatientRepository;
use App\Repository\MutuelleRepository;
use App\Repository\PatientConfigCentreRepository;
use App\Repository\PatientRepository;
use App\Repository\PaysRepository;
use App\Repository\UserRepository;
use App\Repository\VilleRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class DossierMedicalController extends AbstractController
{

 



    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var PatientRepository
     */
    private $patientRepository;
    
    /**
     * @var VilleRepository
     */
    private $villeRepository;

    /**
     * @var MutuelleRepository
     */
    private $mutuelleRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var GPatientRepository
     */
    private $gpatientRepository;

    /**
     * @var DocumentRepository
     */
    private $documentRepository;

    /**
     * @var DossierMedicalRepository
     */
    private $dossierMedicalRepository;

    /**
     * @var LPatientRepository
     */
    private $lpatienRepository;

    /**
     * @var CentreSoinRepository
     */
    private $centreSoinRepository;

    private $apiConnector;

    public function __construct(EntityManagerInterface $em,PatientRepository $patientRepository,
                                VilleRepository $villeRepository,
                                GPatientRepository $gpatientRepository,
                                CentreSoinRepository $centreSoinRepository,
                                UserRepository $userRepository,
                                DossierMedicalRepository $dossierMedicalRepository,
                                MutuelleRepository $mutuelleRepository,
                                DocumentRepository $documentRepository,
                                LPatientRepository $lpatienRepository,
                                ApiConnector $apiConnector)
    {
        $this->em = $em;
        $this->patientRepository = $patientRepository;
        $this->villeRepository = $villeRepository;
        $this->mutuelleRepository = $mutuelleRepository;
        $this->centreSoinRepository = $centreSoinRepository;
        $this->gpatientRepository = $gpatientRepository;
        $this->userRepository = $userRepository;
        $this->dossierMedicalRepository = $dossierMedicalRepository;
        $this->documentRepository = $documentRepository;
        $this->lpatienRepository = $lpatienRepository;
        $this->apiConnector = $apiConnector;
    }

    /**
     * @Route("/dmedical/upload", name="upload", methods={"POST"})
     */
    public function uploadFile(Request $request): Response
    { 
        $file = $request->files->get('image');
        $typeDocument = $request->get('typeDocument');
        $patient = $request->get('idDossierMedical');
        $extention = $request->get('extention');
        $date = new \DateTime();
        $nameOrigine=$file->getClientOriginalName();
        $document = new Document();
        $document->setTypeDocument($typeDocument);
        $document->setNameOrigine($nameOrigine);
        $document->setUpdatedAt($date);
        $name= $patient.'-'.$typeDocument.'-'.$date->format('Y-m-dH-i-s').rand().'.'.$extention;    
        $document->setName($name);
        if($patient){
            $patient = $this->getPatient($patient);
            $document->setLpatient($patient);
        }
        $file->move('images/products', $name);
        $this->em->persist($document);
        $this->em->flush();
        //$patients =$this->patientRepository->getPatients();
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($document);
    }

    /**
     * @Route("/dmedical/get/files/{id}", name="get_file_id_dossier", requirements={"id":"\d+"})
     */
    public function getFilesByIdDossierMedical($id): Response
    { 

        $documents = $this->documentRepository->findByLpatients($id);
        //$patients =$this->patientRepository->getPatients();
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($documents);
    }
    

    /**
     * @Route("/dmedical/consultation/description/update/{id}", name="dm_consultation_description_update", methods={"POST"})
     */
    public function updateDescription(Request $request,DossierMedical $dossierMedical, ApiConnector $apiConnector): Response
    { 
        $request = $apiConnector->transformJsonBody($request);
        $description = $request->get('description');
        $dossierMedical->setDescription($description);
        $this->em->persist($dossierMedical);
        $this->em->flush();
        return $this->json(['id'=>$dossierMedical->getId()]);
    }

    /**
     * @Route("/dmedical/show/{id}", name="dm_show")
     */
    public function show(DossierMedical $dossierMedical): Response
    { 
        if($dossierMedical->getComorbidite()){
            $commorbidite = $dossierMedical->getComorbidite()->getLibelle();
        }
        
        return $this->json([
                            'id'=>$dossierMedical->getId(),
                            'body'=>$dossierMedical->getDescription(),
                            'commentaire'=>$dossierMedical->getCommentaire(),
                            'diagnostic'=>$dossierMedical->getDiagnostic(),
                            'commorbidite'=>$commorbidite,
                        ]);
    }

    /**
     * @Route("/dmedical/insert/comorbidite/{lpatient}/{diagnostic}", name="dm_gpatient_comorbidite_update")
     */
    public function saveComorbidite($diagnostic,LPatient $lpatient): Response
    { 
        $lpatient->setDiagnostic($diagnostic);
        $this->em->persist($lpatient);
        $this->em->flush();
        return $this->json(['id'=>$lpatient->getId()]);
    }

    /**
     * @Route("/dmedical/insert/diagnostic/{gpatient}/{comorbidite}", name="dm_gpatient_comorbidite_update")
     */
    public function saveDiagnostic(Comorbidite $comorbidite,GPatient $gpatient): Response
    { 
        
        $gpatient->setComorbidite($comorbidite);
        $this->em->persist($gpatient);
        $this->em->flush();
        return $this->json(['id'=>$gpatient->getId()]);
    }

    function getVille($ville){
        return $this->villeRepository->find($ville);
    }

    function getPays($pays){
        return $this->paysRepository->find($pays);
    }

    function getMutuelle($mutuelle){
        return $this->mutuelleRepository->find($mutuelle);
    }

    function getDossierMedicale($id){
        return $this->dossierMedicalRepository->find($id);
    }
    function getPatient($patient){
        return $this->lpatienRepository->find($patient);
    }
    
    function getGPatients($gpatient){
        return $this->gpatientRepository->find($gpatient);
    }

    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }
    function getUsers($user){
        return $this->userRepository->find($user);
    }

 

}