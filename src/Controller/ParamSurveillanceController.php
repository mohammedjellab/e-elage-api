<?php

namespace App\Controller;

use App\Entity\ParamSurveillance;
use App\Repository\ModeleConsultationRepository;
use App\Repository\ParamSurveillanceRepository;
use App\Repository\UniteRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class ParamSurveillanceController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ParamSurveillanceRepository
     */
    private $paramSurveillanceRepository;

    /**
     * @var UniteRepository
     */
    private $uniteRepository;

    public function __construct(EntityManagerInterface $em,
                                ParamSurveillanceRepository $paramSurveillanceRepository,
                                UniteRepository $uniteRepository)
    {
        $this->em = $em;
        $this->paramSurveillanceRepository = $paramSurveillanceRepository;
        $this->uniteRepository = $uniteRepository;
    }





    /**
     * @Route("/paramserveillance/show/{centre}",name="param_survaillance_show")
     */
    public function show($centre): Response {

        $modele = $this->modeleConsultationRepository->findByCentreSoin($centre);
        $modele = array_shift($modele);
        return $this->json([
                            "body"=>$modele->getBody()
                        ]);
    }

    /**
     * @Route("/paramserveillance/insert",name="param_survaillance_insert")
     * @Route("/paramserveillance/update/{id}",name="param_survaillance_update")
     */
    public function insert(Request $request, ParamSurveillance $paramSurveillance = null, ApiConnector $apiConnector): Response {

        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        $abreviation = $request->get('abreviation');
        $couleur = $request->get('couleur');
        $borneMin = $request->get('borneMin');
        $borneMax = $request->get('borneMax');
        $unite = $request->get('unite');
        if(!$paramSurveillance){
            $paramSurveillance = new ParamSurveillance();
        }
        if($unite){
            $unite = $this->getUnite($unite);
            $paramSurveillance->setUnite($unite);
        }
        $paramSurveillance->setLibelle($libelle);
        $paramSurveillance->setAbreviation($abreviation);
        $paramSurveillance->setCouleur($couleur);
        if($borneMin)
            $paramSurveillance->setBorneMin($borneMin);
        if($borneMax)
            $paramSurveillance->setBorneMax($borneMax);
        $paramSurveillance->setActive(true);
        $this->em->persist($paramSurveillance);
        $this->em->flush();
        return $this->json([
                            "id"=>$paramSurveillance->getId(),
                            "abreviation"=>$paramSurveillance->getAbreviation(),
                            "libelle"=>$paramSurveillance->getLibelle(),
                            "borneMin"=>$paramSurveillance->getBorneMin(),
                            "borneMax"=>$paramSurveillance->getBorneMax(),
                            "unite"=>$unite,
                        ]);
    }

    /**
     * @Route("/paramserveillance",name="paramsurvaillance_show_all")
     */
    public function showAll(): Response {

        $constantes = $this->paramSurveillanceRepository->findAllActive();
        return $this->json($constantes);
    }



    function getUnite($unite){
        return $this->uniteRepository->find($unite);
    }

}