<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Repository\ActeRepository;
use App\Repository\TypeConsultationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class ActeController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ActeRepository
     */
    private $acteRepository;

    public function __construct(EntityManagerInterface $em,ActeRepository $acteRepository)
    {
        $this->em = $em;
        $this->acteRepository = $acteRepository;
    }


    /**
     * @Route("/acte/index", name="acte_index")
     */
    public function index(ActeRepository $repo): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$repo->findAll();
        
       return new JsonResponse($actes);
    }

    /**
     * @Route("/acte/show/{id}", name="acte_show")
     */
    public function show(Acte $acte): Response
    {
        $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        //$acte =$repo->findById(2);

        //dd($acte);
       return new JsonResponse([
        'name' => "Carlos",
        'age' => 22,
        'location' => [
            'city' => 'Bucaramanga',
            'state' => 'Santander',
            'country' => 'Colombia'
        ]
    ]);
    }

      /**
     * @Route("/acte/delete/{id}", name="acte_delete")
     */
    public function delete(Acte $acte): Response
    {
        $this->em->remove($acte);
        $this->em->flush();
        //dd($acte);
       return new JsonResponse($acte);
    }

    /**
     * @Route("/acte/update/{id}", name="acte_update")
     */
    public function update(Acte $acte): Response
    {

        $acte->setLibelle('eee1');
        $acte->setPrix(11);
        $acte->setCompteComptable('13');
        $acte->setNameDisplay('eee');
        $this->em->persist($acte);
        //$this->em->flush();
        $this->em->flush();
       // dd($acte);
       return new JsonResponse($acte);
    }

    /**
     * @Route("/acte/insert", name="acte_insert")
     */
    public function insert()
    {
        $json=null;
        $acte = new Acte();
        $acte->setId(2);
        $acte->setLibelle('new');
        $acte->setPrix(11);
        $acte->setCompteComptable('13');
        $acte->setNameDisplay('new');

        $this->em->persist($acte);
        //$this->em->flush();
        $this->em->flush();
       return new JsonResponse($json);
    }

     /**
     * @Route("/actes/join-famille", name="actes_join_famille")
     */
    public function list()
    {
       $actes =$this->acteRepository->findAllFamilles();
       //dd($actes);
       return $this->json($actes);
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
    }


}