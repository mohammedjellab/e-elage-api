<?php

namespace App\Controller;

use App\Entity\Antecedent;
use App\Entity\LPatient;
use App\Repository\AntecedentRepository;
use App\Repository\LPatientRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class AntecedentController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var AntecedentRepository
     */
    private $antecedentRepository;

    /**
     * @var LPatientRepository
     */
    private $lpatienRepository;

    public function __construct(EntityManagerInterface $em,AntecedentRepository $antecedentRepository,
                                LPatientRepository $lpatienRepository)
    {
        $this->em = $em;
        $this->antecedentRepository = $antecedentRepository;
        $this->lpatienRepository = $lpatienRepository;
    }

    /**
     * @Route("/antecedents/search/{term}", name="antecedent_search")
     */
    public function search($term): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$this->antecedentRepository->searchByName($term);
        
       return $this->json($actes);
    }


    /**
     * @Route("/antecedents/list", name="antecedent_list")
     */
    public function list(): Response
    {
       
        $antecedents =$this->antecedentRepository->findAll();
        $data= [];
        foreach($antecedents as $ant){
            $d = [ "libelle" => $ant->getLibelle(),"id"=> $ant->getId()];
            array_push($data,$d);
        }
       return $this->json($data);
    }
    /**
     * @Route("/antecedents/patient/{lPatient}", name="antecedent_patient_show")
     */
    public function showAntecedantByPatient(LPatient $lPatient): Response
    {
        $antecedents =$this->antecedentRepository->findAll();
        
        $antsPatient = $this->lpatienRepository
                     ->findAllAntecedantByPatient($lPatient)
                     ->getAntecedents();
        $data= [];
        foreach($antecedents as $ant){
            $selected = false;
            foreach($antsPatient as $antPat){
                if($antPat->getId()==$ant->getId() ){
                    $selected = true;
                }else if ($antPat->getId()!=$ant->getId() && !$selected){
                    $selected = false;
                }
            }
            $d = [ "libelle" => $ant->getLibelle(),"id"=> $ant->getId(),"selected"=>$selected];
            array_push($data,$d);
        }
       //die();
       return $this->json($data);
    }

     /**
     * @Route("/antecedents/insert", name="antecedents_insert",methods={"POST"})
     * @Route("/antecedents/update/{id}", name="antecedents_update",methods={"POST","PUT"})
     */
    public function persist(Request $request, ApiConnector $apiConnector,  Antecedent $antecedent=null)
    {
        $request = $apiConnector->transformJsonBody($request);
        if(!$antecedent){
            $antecedent = new Antecedent();
        }
        $antecedent->setLibelle($request->get('libelle'));
        //$antecedent->setActive($request->get('active'));
        $this->em->persist($antecedent);
        $this->em->flush();
        return $this->json([
            'libelle'=>$antecedent->getLibelle(),
            'id'=>$antecedent->getId()
        ]);
    }

    /**
     * @Route("/antecedents/patient/{antecedent2}/{lPatient2}", name="antecedent_patient_insert")
     */
    
    public function insertAntecedantPatient(Antecedent $antecedent2 ,LPatient $lPatient2, ApiConnector $apiConnector)
    {
        $patient = $this->getPatient($lPatient2);
        $antecedent = $this->getAntecedent($antecedent2);
        $lPatient2->addAntecedent($antecedent);
        $antecedent2->addLpatient($patient);
        $this->em->persist($antecedent2);
        $this->em->persist($lPatient2);
        $d =  $this->em->flush();
        return $this->json(['result'=>$d]);
    }


    /**
     * @Route("/antecedents/delete/{id}",name="antecedents_delete", methods={"GET"})
     */
    public function delete(Antecedent $antecedent ): Response {
        $antecedent->setActive(false);
        $this->em->persist($antecedent);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json([
                'libelle'=>$antecedent->getLibelle(),
                'id'=>$antecedent->getId()
            ]);
    }



    function getPatient($patient){
        return $this->lpatienRepository->find($patient);
    }

    function getAntecedent($antecedent){
        return $this->antecedentRepository->find($antecedent);
    }



}