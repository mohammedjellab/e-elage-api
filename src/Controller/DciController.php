<?php

namespace App\Controller;

use App\Entity\Dci;
use App\Repository\DciRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * @Route("/api/dcis")
 */
class DciController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var DciRepository
     */
    private $dciRepository;

    public function __construct(EntityManagerInterface $em, DciRepository $dciRepos, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->dciRepository = $dciRepos;
    }

    /**
     * @Route("/", name="dcis", methods={"GET"})
     */
    public function index(): Response
    {
        $dcis = $this->dciRepository->findAll();
        if (empty($dcis) || count($dcis) == 0)
            return $this->json(['message' => 'Aucun enregistrement dans la base de données']);
        return $this->json($dcis);
    }

    /**
     * @Route("/active", name="active-dcis")
     */
    public function getActives(): Response
    {
        $dcis = $this->dciRepository->findByActive();
        if (empty($dcis) || count($dcis) == 0)
            return $this->json(['message' => 'No Dci found']); /* Aucun enregistrement dans la base de données*/
        return $this->json($dcis);
    }

     /**
     * @Route("/add", name="add-dci", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        try {
            $dci = $this->serializer->deserialize($request->getContent(), Dci::class, 'json');
            $dci->setActive(true);
            $this->em->persist($dci);
            $this->em->flush();
            return $this->json([
                'message' => 'Successfully added',
                'data' => $dci
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/edit/{id}", name="update-dci", methods={"PUT"})
     */
    public function update(Request $request, $id): Response
    {
        try {
            $dci = $this->serializer->deserialize($request->getContent(), Dci::class, 'json');
            $searchedDci = $this->dciRepository->find($id);
            if (!$searchedDci) {
                throw $this->createNotFoundException(
                    'No dci found for id ' . $id
                );
            }

            $searchedDci->setLibelle($dci->getLibelle());
            $searchedDci->setActive(true);
            $this->em->flush($searchedDci);
            return $this->json([
                'message' => 'Successfully updated',
                'data' => $searchedDci
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/delete/{id}", name="delete-dci", methods={"PUT"})
     */
    public function delete($id): Response
    {
        try {
            $searchedDci = $this->dciRepository->find($id);
            if (!$searchedDci) {
                throw $this->createNotFoundException(
                    'No dci found for id ' . $id
                );
            }


            // $this->em->remove($searchedLabo);
            $searchedDci->setActive(false);
            $this->em->flush($searchedDci);
            return $this->json([
                'message' => 'The object with ID ' . $id . ' was successfully deleted'
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }
    
    /**
     * @Route("/list", name="dci_list")
    */
    public function list()
    {
        $allDci = $this->dciRepository->findAll();
        return $this->json($allDci);
    }
}
