<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\GPatient;
use App\Entity\LPatient;
use App\Entity\Patient;
use App\Repository\ActeRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\GPatientRepository;
use App\Repository\MutuelleRepository;
use App\Repository\PatientConfigCentreRepository;
use App\Repository\PatientRepository;
use App\Repository\PaysRepository;
use App\Repository\UserRepository;
use App\Repository\VilleRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class PatientController extends AbstractController
{

 



    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var PatientRepository
     */
    private $patientRepository;
    
    /**
     * @var VilleRepository
     */
    private $villeRepository;

    /**
     * @var MutuelleRepository
     */
    private $mutuelleRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var GPatientRepository
     */
    private $gpatientRepository;

    private $apiConnector;

    public function __construct(EntityManagerInterface $em,PatientRepository $patientRepository,
                                VilleRepository $villeRepository,
                                GPatientRepository $gpatientRepository,
                                CentreSoinRepository $centreSoinRepository,
                                UserRepository $userRepository,
                                MutuelleRepository $mutuelleRepository,
                                ApiConnector $apiConnector)
    {
        $this->em = $em;
        $this->patientRepository = $patientRepository;
        $this->villeRepository = $villeRepository;
        $this->mutuelleRepository = $mutuelleRepository;
        $this->centreSoinRepository = $centreSoinRepository;
        $this->gpatientRepository = $gpatientRepository;
        $this->userRepository = $userRepository;
        $this->apiConnector = $apiConnector;
    }

    /**
     * @Route("/patients/list", name="patients_list")
     */
    public function list(): Response
    {
        /*$conn = $this->getDoctrine()->getManager()->query();
        $query = $this->em->createQuery('SELECT * FROM patient');
        $users = $query->getResult();
        dd($users);*/
        $patients =$this->patientRepository->getPatients();
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($patients);
    }

    /**
     * @Route("/patients/{gpatient}/{centreSoin}", name="patients_show")
     */
    public function show($gpatient,$centreSoin)
    {
        $patient = $this->gpatientRepository->findPatientById($gpatient,$centreSoin);
        return $this->json($patient);
    }

    /**
     * @Route("/lpatient/{lpatient_id}/{centreSoin}", name="lpatient_show_for_dm")
     */
    public function showLpatient($lpatient_id,$centreSoin)
    {
        $patient = $this->gpatientRepository->findPatientByLpatientId($lpatient_id,$centreSoin);
        return $this->json($patient);
    }
    /**
     * @Route("/gpatient/upload/{gpatient}", name="upload", methods={"POST"})
     */
    public function uploadFile(Request $request,GPatient $gpatient): Response
    { 
        $file = $request->files->get('image');
        //$antecedent = $request->files->get('antecedent');

        $extention = $request->get('extention');
        //$extention_antec = $request->get('extention_antec');
        $date = new \DateTime();
        /*$gpatient->setTypeDocument($typeDocument);
        $gpatient->setNameOrigine($nameOrigine);
        $gpatient->setUpdatedAt($date);*/
        $name= $date->format('Y-m-dH-i-s').rand().'.'.$extention;    
        //$name_antec= $date->format('Y-m-dH-i-s').rand().'.'.$extention_antec;    
        $gpatient->setPhoto($name);
       // $gpatient->setAntecedent($name_antec);
        /*if($patient){
            $patient = $this->getPatient($patient);
            $gpatient->setLpatient($patient);
        }*/
        $file->move('images/patient', $name);
        //$antecedent->move('images/antecedent', $name_antec);
        $this->em->persist($gpatient);
        $this->em->flush();
        //$patients =$this->patientRepository->getPatients();
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($gpatient);
    }
    /**
     * @Route("/gpatient/insert", name="gpatient_insert",methods={"POST"})
     * @Route("/gpatient/update/{id}", name="gpatient_update",methods={"POST"})
     */

    // 
    public function persistGPatient(GPatient $gpatient = null,Request $request, ApiConnector $apiConnector)
    {
       
        if(!$gpatient){
            $gpatient = new GPatient();
        }
        
        //dd($dateNaiss);
        $request = $apiConnector->transformJsonBody($request);
        $ville = $request->get('ville');
        $tel = $request->get('tel');
        $pays = $request->get('pays');
        $nbEnfant = $request->get('nbEnfant');
        $mutuelle = $request->get('mutuelle');
        $centreSoin = $request->get('centreSoin');
        $dateNaiss = $request->get('dateNaissance');
        $email = $request->get('email');
        $gpatient->setSexe($request->get('sexe'));
        $gpatient->setFirstname($request->get('firstname'));
        $gpatient->setLastname($request->get('lastname'));
        $gpatient->setCin($request->get('cin'));
        $gpatient->setAdresse($request->get('adresse'));
        //$gpatient->setDateNaissance($dateNaiss);
        if($ville){
            $ville = $this->getVille($ville);
            $gpatient->setVille($ville);
        }
        if($pays){
            $pays = $this->getVille($pays);
            $gpatient->setVille($pays);
        }
        //dd();
        $gpatient->setSituationFamille($request->get('situationFamille'));
        $gpatient->setCivilite($request->get('civilite'));
        if($centreSoin){
            $centre = $this->getCentreSoin($centreSoin);
            $gpatient->setCentreSoin($centre);
        }
        //$gpatient->setCorr($request->get('correspondant'));
        if($nbEnfant){
            $gpatient->setNbEnfant(+$request->get('nbEnfant'));
        }
        if($tel){
            $gpatient->setTel($tel);
        }
        if($email){
            $gpatient->setEmail($email);
        }
        if($mutuelle){
            $mut = $this->getMutuelle($mutuelle);
            $gpatient->setMutuelle($mut);
        }
        $gpatient->setFixe($request->get('fixe'));
        $gpatient->setRamed($request->get('ramed'));
        $gpatient->setDescription($request->get('description'));
        if($request->get('recommendation'))
            $gpatient->setRecommendation($request->get('recommendation'));
        //$gpatient->setRec($request->get('recommendationAutre'));
        //$gpatient->setCin($request->get('lastname_arabe'));
        //$gpatient->setAdressePar($request->get('adressePar'));
        $gpatient->setAllergieConnue($request->get('allergieConnue'));
        $gpatient->setComorbidite($request->get('comorbidite'));
        $gpatient->setFumeur($request->get('fumeur'));
        $gpatient->setGroupSanguin($request->get('groupSanguin'));
        $gpatient->setMatricule($request->get('matricule'));

        if($request->get('photo')){
            $gpatient->setPhoto($request->get('photo'));

        }
        $gpatient->setProfession($request->get('profession'));
        if($dateNaiss){
            $dateNaiss = strtotime($dateNaiss);
            $dateReturned = date('Y-m-d', $dateNaiss);
            $dateNaiss = date('Y-m-d h:i:s', $dateNaiss);
            $dateNaiss = \DateTime::createFromFormat('Y-m-d h:i:s', $dateNaiss);
            $gpatient->setDateNaissance($dateNaiss);
        }


        $this->em->persist($gpatient);
        $this->em->flush();
        return $this->json([
                            'gpatient_id'=>$gpatient->getId(),
                            'firstname'=>$gpatient->getFirstname(),
                            'lastname'=>$gpatient->getLastname(),
                            'cin'=>$gpatient->getCin(),
                            'tel'=>$gpatient->getTel(),
                            'adresse'=>$gpatient->getAdresse(),
                            'dateNaissance'=>$gpatient->getDateNaissance(),
                            'sexe'=>$gpatient->getSexe(),
                            'civilite'=>$gpatient->getCivilite(),
                            'situationFamille'=>$gpatient->getSituationFamille(),
                            'profession'=>$gpatient->getProfession(),
                            'ramed'=>$gpatient->getRamed(),
                            //'mutuelle'=>$gpatient->getMutuelle()->getId(),
                            'fixe'=>$gpatient->getFixe(),
                        ]);
    }

     /**
     * @Route("/lpatient/insert", name="lpatient_insert",methods={"POST"})
     * @Route("/lpatient/update/{id}", name="lpatientc_update",methods={"POST","PUT"})
     */
    public function persistlPatient(Request $request,LPatient $lpatient = null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        if(!$lpatient){
            $lpatient = new LPatient();
        } 
        $gdossierPatient = $request->get('gdossierPatient');
        $centreSoin = $request->get('centreSoin');
        $medecin = $request->get('medecin');
        $adressePar = $request->get('adressePar');
       // dd($centreSoin);
        $lpatient->setAdressePar($adressePar);
        if($gdossierPatient){
            $gdossierPatient = $this->getGPatients($gdossierPatient);
            $lpatient->setGpatient($gdossierPatient);
        }
        if($centreSoin){
            $centre = $this->getCentreSoin($centreSoin);
            $lpatient->setCentreSoin($centre);
        }

        if($medecin){
            $med = $this->getUsers($medecin);
            $lpatient->setPraticien($med);
        }
        
        
        $this->em->persist($lpatient);
        $this->em->flush();
        return $this->json([
            "lpatient_id"=>$lpatient->getId()
        ]);
    }

    /**
     * @Route("/lpatient/config", name="config_patient_list")
     */
    public function configPatient(PatientConfigCentreRepository $repo): Response
    {
        /*$conn = $this->getDoctrine()->getManager()->query();
        $query = $this->em->createQuery('SELECT * FROM patient');
        $users = $query->getResult();
        dd($users);*/
        $config =$repo->findByVisible(true);
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($config);
    }

    /**
     * @Route("/gpatient/search/{term}", name="patient_actes_search")
     */
    public function searchPatient($term): Response
    {
        
       $actes =$this->gpatientRepository->searchPatient($term);
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController']);

       return $this->json($actes);
    }

    /**
     * @Route("/gpatient/search", name="patients_actes_search", methods={"POST"})
     */
    public function searchPatients(
                                    Request $request, 
                                    ApiConnector $apiConnector): Response
    {
        $request = $apiConnector->transformJsonBody($request);
        $date = $request->get('date');
        $centre = $request->get('centre');
        $praticien = $request->get('praticien');
        $telephone = $request->get('telephone');
        $lastname = $request->get('lastname');
        $firstname = $request->get('firstname');
        $num_dossier = $request->get('num_dossier');
        $date =date('Y-m-d',strtotime($date));

        /*$date = null;//$request->get('date');
        $centre = null;//$request->get('centre');
        $praticien = null;//$request->get('praticien');
        $telephone = null;//$request->get('telephone');
        $lastname = null;//$request->get('lastname');
        $firstname = null;//$request->get('firstname');
        $num_dossier = null;//$request->get('num_dossier');*/
        
       $actes =$this->gpatientRepository->searchPatients($date,$centre,$praticien,$telephone,$lastname,$firstname,$num_dossier);
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController']);

       return $this->json($actes);
    }

        

    function getVille($ville){
        return $this->villeRepository->find($ville);
    }

    function getPays($pays){
        return $this->paysRepository->find($pays);
    }

    function getMutuelle($mutuelle){
        return $this->mutuelleRepository->find($mutuelle);
    }

    

    function getGPatients($gpatient){
        return $this->gpatientRepository->find($gpatient);
    }

    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }
    function getUsers($user){
        return $this->userRepository->find($user);
    }

 

}