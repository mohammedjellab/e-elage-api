<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController{



    /**
     * @var ActeRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    /**
     * @Route("/users/correspandant/list", name="correspandant_index")
     * @return Response
     */
    public function correspandant()
    {
       $correspondants =$this->userRepository->findAllMedecin(2);
       //dd($correspondants);
       return $this->json($correspondants);
    }

    /**
     * @Route("/users/correspandant/search/{term}", name="correspandant_search")
     * @return Response
     */
    public function searchCorrespandant($term): Response
    {
       $correspondants =$this->userRepository->searchCorrespandant($term);

       
       return $this->json($correspondants);
    }

    /**
     * @Route("/users/medecin/list", name="medecin_index")
     * @return Response
     */
    public function medecin(UserRepository $userRepository)
    {
       $med =$userRepository->findAllMedecin(1);

       return $this->json($med);
    } 

   /**
     * @Route("/users/medecin/list/{centre}", name="medecin_index")
     * @return Response
     */
    public function medecinByEtablissment(UserRepository $userRepository,$centre)
    {
       $med =$userRepository->findAllMedecinByEtablissment(1,$centre);

       return $this->json($med);
    }

   /**
    * @Route("/users/list", name="users_list")
    */
   public function list()
   {
       $user =$this->userRepository->findAllUsers();
       //dd($actes);
       return $this->json($user);
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
   }

   /**
     * @Route("/users/delete/{id}",name="users_delete", methods={"GET"})
     */
   public function delete(User $user ): Response {
      $user->setActive(false);
      $this->em->persist($user);
      $this->em->flush();
      //$salle = $this->salleRepository->find($id);
      return $this->json($user);
   }

   /**
     * @Route("/users/type/{query}",name="name-specialite", methods={"GET"})
     */
    public function user_type( $query): Response {
      if(strlen($query) < 3)
         return $this->json(null);
      $user =$this->userRepository->finUsersByType($query);
       //dd($actes);
       return $this->json($user);
   }
   
}