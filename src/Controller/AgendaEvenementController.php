<?php

namespace App\Controller;

use App\Entity\AgendaEvenement;
use App\Repository\AgendaEvenementRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\HolidayRepository;
use App\Repository\LegendeRepository;
use App\Repository\LPatientRepository;
use App\Repository\SalleRepository;
use App\Repository\TypeConsultationRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class AgendaEvenementController extends AbstractController
{

    /**
     * @var AgendaEvenementRepository
     */
    private $agendaEvenementRepository;

    
    /**
     * @var SalleRepository
     */
    private $salleRepository;

        
    /**
     * @var LPatientRepository
     */
    private $lpatienRepository;

    /**
     * @var TypeConsultationRepository
     */
    private $typeConsultationRepository;

    
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CentreSoinRepository
     */
    private $centreSoinRepository;

        /**
     * @var LegendeRepository
     */
    private $legendeRepository;

    private $apiConnector;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(EntityManagerInterface $em,
                                AgendaEvenementRepository $agendaEvenementRepository,
                                SalleRepository $salleRepository,
                                HolidayRepository $holidayRepository,
                                CentreSoinRepository $centreSoinRepository,
                                LPatientRepository $lpatienRepository,
                                TypeConsultationRepository $typeConsultationRepository,
                                UserRepository $userRepository,
                                LegendeRepository $legendeRepository,
                                ApiConnector $apiConnector)
    {
        $this->em = $em;
        $this->agendaEvenementRepository = $agendaEvenementRepository;
        $this->salleRepository = $salleRepository;
        $this->lpatienRepository = $lpatienRepository;
        $this->centreSoinRepository = $centreSoinRepository;
        $this->typeConsultationRepository = $typeConsultationRepository;
        $this->userRepository = $userRepository;
        $this->legendeRepository =$legendeRepository;
        $this->holidayRepository =  $holidayRepository;
        $this->apiConnector = $apiConnector;
    }

    /**
     * @Route("/agenda/evenement", name="agenda_evenement")
     */
    public function index(): Response
    {
        return $this->render('agenda_evenement/index.html.twig', [
            'controller_name' => 'AgendaEvenementController',
        ]);
    }

     /**
     * @Route("/agenda_evenements/list/{centre}", name="nbre_agenda_evenement_centre")
     */
    //nbre de rdv par type consultation
    public function nbreRdvByTypeConsultation($centre): Response
    {
        $nbres = $this->agendaEvenementRepository->nbreRdvByTypeConsultation($centre);
        return $this->json($nbres);
    }
    
    /**
     * @Route("/agenda_evenements/holiday", name="nbre_agenda_evenement_centre")
     */
    //nbre de rdv par type consultation
    public function getHolidays(): Response
    {
        $nbres = $this->holidayRepository->getHolidays();
        return $this->json($nbres);
    }


    /**
     * @Route("/agenda_evenements/calendarrdv", name="calendarrdv_agenda_evenement")
     */
    //search rdv par date, salle, medecin, 
    public function calendarRdv(
        Request $request,
        ApiConnector $apiConnector): Response
    {
    $request = $apiConnector->transformJsonBody($request);
    $date = $request->get('date2');
    $start = $request->get('start'); 
    $end = $request->get('end');
    $salle = $request->get('salle');
    $praticien = $request->get('praticien');
    $term = $request->get('term');
    $en_salle = $request->get('en_salle');
    $centre = $request->get('centre');
    $type_journee = $request->get('type_journee');
    if(!$start){
        $start = date("Y-m-d", strtotime('monday this week'));
    }
    if(!$end){
        $end = date("Y-m-d", strtotime('sunday this week'));
        
    }
    $end = substr($end,0,10);
    $start = substr($start,0,10);
    // $date = new \DateTime();
    //$date=date_format($date,"Y-m-d");

  
    /* $date = "07/07/2021";//$request->get('date');
    $salle = null;//$request->get('salle');
    $praticien = null;//$request->get('praticien');
    $term = null;//$request->get('term');
    $term = null;//$request->get('term');
    $en_salle = null;//$request->get('en_salle');
    $centre = null;//$request->get('centre');
    $type_journee = null;//$request->get('type_journee');*/

    //$date = $this->getDate($this->getYear($date),$this->getMonth($date),$this->getDay($date));


    //dd($type_journee);
    $listes = $this->agendaEvenementRepository->calendarRdv($date,$start,$end,$centre,$salle,$praticien,$type_journee,$term,$en_salle);
    return $this->json($listes);
    /*return $this->render('agenda_evenement/index.html.twig', [
    'controller_name' => 'AgendaEvenementController',
    ]);*/

    }
    /**
     * @Route("/agenda_evenements/listrdv", name="search_agenda_evenement_by")
     */
    //search rdv par date, salle, medecin, 
    public function searchRdv(
                                Request $request,
                                ApiConnector $apiConnector): Response
    {
        $request = $apiConnector->transformJsonBody($request);
        $date = $request->get('date2');
        $salle = $request->get('salle');
        $praticien = $request->get('praticien');
        $term = $request->get('term');
        $en_salle = $request->get('en_salle');
        $centre = $request->get('centre');
        $type_journee = $request->get('type_journee');
        //$date = new \DateTime($date);
        //$date=date_format($date,"Y-m-d");
       // $date = strtotime($date);
        //$date = date('Y-m-d', $date);
        //$date = \DateTime::createFromFormat('Y-m-d', $date);
        //dd($date);
         /*$date = "09/01/2021";//$request->get('date');
        $salle = null;//$request->get('salle');
        $praticien = null;//$request->get('praticien');
        $term = null;//$request->get('term');
        $term = null;//$request->get('term');
        $en_salle = null;//$request->get('en_salle');
        $centre = null;//$request->get('centre');
        $type_journee = null;//$request->get('type_journee');*/

        //$date = $this->getDate($this->getYear($date),$this->getMonth($date),$this->getDay($date));
        
        
       //dd($type_journee);
        $listes = $this->agendaEvenementRepository->searchRdv($date,$centre,$salle,$praticien,$type_journee,$term,$en_salle);
        
        return $this->json($listes);
        /*return $this->render('agenda_evenement/index.html.twig', [
            'controller_name' => 'AgendaEvenementController',
        ]);*/
        
    }


    /**
     * @Route("/agenda_evenements/rdv_patient/{patient}/{id}", name="rdv_patient_agenda_evenement")
     */
    //search rdv par date, salle, medecin, 
    public function rdvByPatient($patient,$id): Response
    {
        $listes = $this->agendaEvenementRepository->rdvBypatient($patient,$id);
        return $this->json($listes);
    }

    /**
     * @Route("/agenda_evenements/annuler/rdv/{id}", name="rdv_annuler_agenda_evenement")
     */
    //search rdv par date, salle, medecin, 
    public function annulerRdv($id): Response
    {
        //dd('ddd');
        $rdv = null;
        if($id){
           $rdv = $this->getRdv($id);
           $rdv->setActive(false);
        }
        $this->em->persist($rdv);
        $this->em->flush();

        return $this->json($rdv->getId());
    }



    /**
     * @Route("/agenda_evenements/show/{id}", name="rdv_show_agenda_evenement")
     */
    public function showRdv($id): Response
    {
        $listes = $this->agendaEvenementRepository->findRdvById($id);
        /*return $this->render('agenda_evenement/index.html.twig', [
            'controller_name' => 'AgendaEvenementController',
        ]);*/
       

        return $this->json($listes);
    }
    /**
     * @Route("/agenda_evenements/update/{agendaEvenement}/{status}/{legende}", name="rdv_update_status_agenda_evenement")
     */
    public function updateStatus(AgendaEvenement $agendaEvenement ,$status,$legende): Response
    {
        $agendaEvenement->setStatut($status);
        //legende
        if($legende){
            $legende = $this->getLegende($legende);
            $agendaEvenement->setLegende($legende);
        }
        $this->em->persist($agendaEvenement);
        $this->em->flush();
        //$listes = $this->agendaEvenementRepository->findRdvById($id);
        /*return $this->render('agenda_evenement/index.html.twig', [
            'controller_name' => 'AgendaEvenementController',
        ]);*/
       

        return $this->json([
            "id"=>$agendaEvenement->getId()
            
        ]);
    }

    /**
     * @Route("/agenda_evenements/nbre/{start}/{end}", name="type_consultations_nbre_by_acte_day")
     */
    public function nbreActeByDayAndTypeConsultation($start,$end): Response
    {
        
       $nbres =$this->agendaEvenementRepository->nbreActeByDayAndTypeConsultation($start,$end);
       //dd($nbres);
       //return $this->json($nbres);
      return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
    }

    /**
     * @Route("/agenda_evenements/interval/{start}/{end}", name="type_consultations_nbre_by_interval")
     */
    public function getNbreRdvInIterval($start,$end): Response
    {
        
       $nbres =$this->agendaEvenementRepository->nbreActeByDayAndTypeConsultation($start,$end);
       //dd($nbres);
       //return $this->json($nbres);
      return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
    }

    /**
     * @Route("/agenda_evenements/insert", name="agenda_evenement_insert",methods={"POST"})
     * @Route("/agenda_evenements/update/{id}", name="agenda_evenement_update",methods={"POST","PUT"})
     */
    public function insert(AgendaEvenement $agendaEvenement = null,Request $request, ApiConnector $apiConnector )
    {
        //$agendaEvenement = new AgendaEvenement();
        //dd($agendaEvenement);
        if(!$agendaEvenement){
            $agendaEvenement = new AgendaEvenement();
        }
        $request = $apiConnector->transformJsonBody($request);

        $salle = $request->get('salle');
        $dateDebut = $request->get('dateDebut');
        $dateFin = $request->get('dateFin');

        //dd(gettype(+$this->getDay($dateFin)));
        $description = $request->get('description');
        $heureArrivee = $request->get('heureArrivee');
        //dd($heureArrivee);
        $heureSortie = $request->get('heureSortie');
        $description = $request->get('description');
        $typeConsult = $request->get('typeConsultation');
        //dd($typeConsult);
        $centreSoin = $request->get('centreSoin');
        $legende = $request->get('legende');
        //dd($dateDebut);
        $dateDebut = strtotime($dateDebut);
        $dateDebut = date('Y-m-d h:i:s', $dateDebut);
        
        $dateFin = strtotime($dateFin);
        $dateFin = date('Y-m-d h:i:s', $dateFin);
        $dateDebut = \DateTime::createFromFormat('Y-m-d h:i:s', $dateDebut);
        $dateFin = \DateTime::createFromFormat('Y-m-d h:i:s', $dateFin);
        
        
        $praticien = $request->get('praticien');
        $patient = $request->get('lpatient_id');
        $sansRdv = $request->get('sansRdv');
        $statut = $request->get('statut');
        //dd($patient);
        $heureArrivee = $this->getHeure($this->getHour($heureArrivee),$this->getMinite($heureArrivee));
        $heureSortie = $this->getHeure($this->getHour($heureSortie),$this->getMinite($heureSortie));
        

        
        if($statut){
            $agendaEvenement->setStatut($statut);
        }

        if($centreSoin){
            $centre = $this->getCentreSoin($centreSoin);
            //dd($centre);
            $agendaEvenement->setCentreSoin($centre);
        }
        $salleName = '';
        if($salle){
            $salle = $this->getSalle($salle);
            $agendaEvenement->setSalle($salle);
            $salleName = $agendaEvenement->getSalle()->getLibelle();
        }
        $nom_complet = '';
        if($patient){
            $pat = $this->getPatient($patient);
            $agendaEvenement->setLPatient($pat);
            $nom_complet = $agendaEvenement->getLpatient()->getGpatient()->getCivilite().' '.
            $agendaEvenement->getLpatient()->getGpatient()->getFirstname().' '.
            $agendaEvenement->getLpatient()->getGpatient()->getLastname();
        }

        $acteName = '';
        $color = '';
        if($typeConsult){
            $typeConsult = $this->getTypeConsultation($typeConsult);
            $agendaEvenement->setTypeConsultation($typeConsult);
            $acteName = $agendaEvenement->getTypeConsultation()->getIdGlobal()->getLibelle();
            $color = $agendaEvenement->getTypeConsultation()->getCouleur();
        }

        if($praticien){
            $praticien = $this->getUsers($praticien);
            $agendaEvenement->setPraticien($praticien);
            $praticien= $agendaEvenement->getPraticien()->getId();
        }
       ///// dd($this->getDate($dateDebut));

        $agendaEvenement->setDateDebut($dateDebut);
        $agendaEvenement->setDateFin($dateFin);
        if($description){
            $agendaEvenement->setDescription($description);
        }
        $agendaEvenement->setHeureArrivee($heureArrivee);
        $agendaEvenement->setHeureSortie($heureSortie);
        $agendaEvenement->setSansRdv($sansRdv);
        //legende
        if($legende){
            $legende = $this->getLegende($legende);
            $agendaEvenement->setLegende($legende);
        }

        //Information du patient
        
        $info_patient = $request->get('info_patient');
        if($info_patient){
            $agendaEvenement->setLastname($info_patient["firstname"]);
            $agendaEvenement->setFirstname($info_patient["lastname"]);
            $agendaEvenement->setCivilite($info_patient["civilite"]);

        }
        $agendaEvenement->setActive(true);
        //dd($info_patient["firstname"]);
        $this->em->persist($agendaEvenement);
        $this->em->flush();
        
        return $this->json([
            "id"=>$agendaEvenement->getId(),
            "dateDebut"=> $this->getDate($agendaEvenement->getDateDebut()),
            "dateFin"=> $this->getDate($agendaEvenement->getDateFin()),
            "nom_complet" =>$nom_complet,
            "firstname"=>$agendaEvenement->getFirstname(),
            "lastname"=>$agendaEvenement->getLastname(),
            "legende"=>$agendaEvenement->getLegende()->getStatus(),
            "heureSortie"=>$this->getHours($agendaEvenement->getHeureSortie()),
            "heureArrivee"=>$this->getHours($agendaEvenement->getHeureArrivee()),
            "praticien"=>$praticien,
            "typeConsult"=>$agendaEvenement->getTypeConsultation(),
            "acte"=>$acteName,
            "color"=>$color,
            "statut"=>$agendaEvenement->getStatut(),
            "description"=>$agendaEvenement->getDescription(),
            "salle"=>$salleName,
            "patient"=>$patient,
            "nom_complet_ss_rdv"=>$agendaEvenement->getCivilite().' '.$agendaEvenement->getFirstname().' '.$agendaEvenement->getLastname()
        ]);

        //$agendaEvenement->setHeu($description);

    }


    function getSalle($salle){
        return $this->salleRepository->find($salle);
    }
    function getRdv($id){
        return $this->agendaEvenementRepository->find($id);
    }
    function getLegende($legende){
        return $this->legendeRepository->find($legende);
    }
    function getCentreSoin($centre){
        return $this->centreSoinRepository->find($centre);
    }
    function getPatient($patient){
        return $this->lpatienRepository->find($patient);
    }
    function getTypeConsultation($type){
        return $this->typeConsultationRepository->find($type);
    }

    function getUsers($user){
        return $this->userRepository->find($user);
    }

    function getYear($date)
    {
        return substr($date,6);
    }

    function getMonth($date)
    {
        return substr($date,3,2);
    }

    function getDay($date)
    {
        return substr($date,0,2);
    }

    function getHour($heure)
    {
        return substr($heure,0,2);
    }

    function getMinite($heure)
    {
        return substr($heure,3);
    }

    function getDateTime($year,$month,$day){
        $date = new \DateTime();
        $date->setDate($year,$month,$day);
        return $date;
    }
    

    function getHeure($hour,$minute){
        $heure = new \DateTime();
        
        $heure->setTime($hour,$minute);
        return $heure;
    }

    function getHours($d){
        $d = $d->getTimestamp();
        return  date("h:i", $d);
    }
    function getDate($d){
        return  $d->format('Y-m-d');
    }




}
