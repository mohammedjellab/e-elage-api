<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Repository\ActeRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class DefaultController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ActeRepository
     */
    private $acteRepository;

    public function __construct(EntityManagerInterface $em,ActeRepository $acteRepository)
    {
        $this->em = $em;
        $this->acteRepository = $acteRepository;
    }
    //voilaDoc
    /**
     * @Route("/default/sysdate", name="sysdate")
     */
    public function index(ActeRepository $repo): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
       // $actes =$repo->findAll();
        
       return $this->json(date("Y/m/d"));
    }

}