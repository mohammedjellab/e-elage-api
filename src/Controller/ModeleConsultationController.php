<?php

namespace App\Controller;

use App\Repository\ModeleConsultationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class ModeleConsultationController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ModeleConsultationRepository
     */
    private $modeleConsultation;

    public function __construct(EntityManagerInterface $em,ModeleConsultationRepository $modeleConsultationRepository)
    {
        $this->em = $em;
        $this->modeleConsultationRepository = $modeleConsultationRepository;
    }





    /**
     * @Route("/modele_consultation/show/{centre}",name="modele_consultation_show")
     */
    public function show($centre): Response {

        $modele = $this->modeleConsultationRepository->findByCentreSoin($centre);
        $modele = array_shift($modele);
        return $this->json([
                            "body"=>$modele->getBody()
                        ]);
    }


}