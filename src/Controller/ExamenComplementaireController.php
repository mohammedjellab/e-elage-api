<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\ExamenComplementaire;
use App\Entity\Test;
use App\Repository\ActeRepository;
use App\Repository\ExamenComplementaireRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class ExamenComplementaireController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ExamenComplementaireRepository
     */
    private $examenComplementaireRepository;

    public function __construct(EntityManagerInterface $em,ExamenComplementaireRepository $examenComplementaireRepository)
    {
        $this->em = $em;
        $this->examenComplementaireRepository = $examenComplementaireRepository;
    }
    
    /**
     * @Route("/examen_complementaires/insert", name="examen_complementaire_insert",methods={"POST"})
     */
    public function persistLSpecialite(Request $request, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        $examen = new ExamenComplementaire();
        $examen->setLibelle($libelle);
        $this->em->persist($examen);
        $this->em->flush();
        return $this->json($examen);
    }

    /**
     * @Route("/test/insert", name="test_insert",methods={"POST"})
     */
    public function test(Request $request, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        $id = $request->get('id');
        $examen = new Test();
        $examen->setName($libelle);

        $id=$this->getExamen($id);
        
        $examen->setExamen($id);
        //dd($examen);
        $this->em->persist($examen);
        $this->em->flush();
        return $this->json($examen);
    }

    function getExamen($examen){
        return $this->examenComplementaireRepository->find($examen);
    }
    

}