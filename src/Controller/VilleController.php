<?php

namespace App\Controller;

use App\Repository\VilleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VilleController extends AbstractController{


    /**
     * @var VilleRepository
     */
    private $villeRepository;

    public function __construct(VilleRepository $villeRepository){
        $this->villeRepository = $villeRepository;
    }

    /**
     * @Route("/villes/search/{term}",name="ville_search")
     */
    public function search($term){

        $villes = $this->villeRepository->searchVilleByName($term);
        return $this->json($villes);
    }

    /**
     * @Route("/villes/list",name="ville_list")
     */
    public function list(){

        $villes = $this->villeRepository->findAll();
        $data =[];
        foreach($villes as $ville){
            $d = [ "name" => $ville->getName(),"id"=> $ville->getId()];
            array_push($data,$d);
        }
        return $this->json($data);
    }

}