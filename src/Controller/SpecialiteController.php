<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\CentreSoin;
use App\Entity\GSpecialite;
use App\Entity\LSpecialite;
use App\Entity\Specialite;
use App\Repository\ActeRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\GSpecialiteRepository;
use App\Repository\LSpecialiteRepository;
use App\Repository\SpecialiteRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use App\Service\CentreSoin as ServiceCentreSoin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class SpecialiteController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SpecialiteRepository
     */
    private $gspecialiteRepository;
    /**
     * @var LpecialiteRepository
     */
    private $lspecialiteRepository;

    /**
     *
     * @var CentreSoinRepository
     */
    private $centreSoinRepository ;

    public function __construct(EntityManagerInterface $em,
                                GSpecialiteRepository $gspecialiteRepository,
                                LSpecialiteRepository $lspecialiteRepository,
                                SpecialiteRepository $specialiteRepository,
                                CentreSoinRepository $centreSoinRepository)
    {
        $this->em = $em;
        $this->gspecialiteRepository = $gspecialiteRepository;
        $this->specialiteRepository = $specialiteRepository;
        $this->centreSoinRepository = $centreSoinRepository;
    }
    
    /**
     * @Route("/gspecialite/insert", name="gspecialite_insert",methods={"POST"})
     */
    public function persistGSpecialite(Request $request, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $gspecialite = new GSpecialite();
        $gspecialite->setName($request->get('name'));
        $this->em->persist($gspecialite);
        $this->em->flush();
       return $this->json($gspecialite);
    }

    /**
     * @Route("/lspecialite/insert", name="lspecialite_insert",methods={"POST"})
     */
    public function persistLSpecialite(Request $request, ApiConnector $apiConnector)
    {
       
        $request = $apiConnector->transformJsonBody($request);
        $listCentreSoin = $request->get('centreSoin');
        $gspecialite = $this->getGspecialite($request->get('gspecialiteId'));
        //dd($gspecialite);
        foreach( $listCentreSoin as $centre){
            $centreSoin = $this->getCentreSoin($centre);
            $lspecialite = new LSpecialite();
            $lspecialite->setCentreSoin($centreSoin);
            $lspecialite->setGspecialite($gspecialite);
            $lspecialite->setActive($request->get('active'));
            $this->em->persist($lspecialite);
            $this->em->flush();
        }
       
       return $this->json($lspecialite);
    }

    /**
     * @Route("/lspecialite/list/{centre}", name="specialite_list")
     */
    public function listSpecialiteByCentre($centre, LSpecialiteRepository $repo)
    {
       $specialites = $repo->getSpecialiteByCentre($centre);
       return $this->json($specialites);
    }

    /**
     * @Route("/lspecialite/list", name="specialite_list_all")
     */
    public function listLSpecialiteAll(LSpecialiteRepository $repo)
    {
       $specialites = $repo->getLSpecialiteAndCentre();
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($specialites);
    }

    /**
     * @Route("/specialites/list", name="specialites_list_all")
     */
    public function listSpecialite()
    {
       $specialites = $this->specialiteRepository->findAllSpecialite();
        //dd($specialites);
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($specialites);
    }

    /**
     * @Route("/specialites/search/{term}", name="specialites_search")
     */
    public function search($term): Response
    {
       $specialites =$this->specialiteRepository->searchByName($term);
       return $this->json($specialites);
    }

    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
        
    }

    function getGspecialite($gspecialite){
        return $this->gspecialiteRepository->find($gspecialite);
    }


    /**
     * @Route("/gspecialite/list", name="gspecialite_list")
     */
    public function getGSpecialiteList( GSpecialiteRepository $repo)
    {
       $specialites = $repo->findAll();
       return $this->json($specialites);
    }



      /**
     * @Route("/specialites/update/{id}",name="specialites_update", methods={"POST","PUT"})
     * @Route("/specialites/insert",name="specialites_insert", methods={"POST"})
     */
    public function update(Specialite $specialite = null,Request $request, ApiConnector $apiConnector): Response {
        
        if(!$specialite){
            $specialite = new Specialite();
        }
        
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        $centreSoin = $request->get('centreSoin');
        if($centreSoin){
            $centreSoin = $this->getCentreSoin($centreSoin);
            $specialite->setCentreSoin($centreSoin);
        }

        $specialite->setName($libelle);
       
        $this->em->persist($specialite);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json(
            [
                'libelle'=>$specialite->getName(),
                'id'=>$specialite->getId()
            ]
        );
    }

    /**
     * @Route("/specialites/delete/{id}",name="specialites_delete", methods={"GET"})
     */
    public function delete(Specialite $specialites ): Response {
        $specialites->setActive(false);
        $this->em->persist($specialites);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json($specialites);
    }

 





}