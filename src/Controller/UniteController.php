<?php

namespace App\Controller;

use App\Entity\Unite;
use App\Repository\ModeleConsultationRepository;
use App\Repository\ParamSurveillanceRepository;
use App\Repository\UniteRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class UniteController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var UniteRepository
     */
    private $uniteRepository;

    public function __construct(EntityManagerInterface $em,UniteRepository $uniteRepository)
    {
        $this->em = $em;
        $this->uniteRepository = $uniteRepository;
    }


    /**
     * @Route("/unite",name="unites_show")
     */
    public function showAll(): Response {

        $unites = $this->uniteRepository->findAllActive();
        return $this->json($unites);
    }



    /**
     * @Route("/unite/insert",name="unites_insert",methods={"POST"})
     * @Route("/unite/update/{id}",name="unites_update",methods={"POST","PUT"})
     */
    public function insert(Request $request, Unite $unite = null, ApiConnector $apiConnector): Response {

        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        $abreviation = $request->get('abreviation');
        if(!$unite){
            $unite = new Unite();
        }
        $unite->setLibelle($libelle);
        $unite->setAbreviation($abreviation);
        $this->em->persist($unite);
        $this->em->flush();
        return $this->json([
                            "id"=>$unite->getId(),
                            "abreviation"=>$unite->getAbreviation(),
                            "libelle"=>$unite->getLibelle(),
                        ]);
    }

    /**
     * @Route("/unite/delete/{id}",name="unites_delete")
     */
    public function delete( Unite $unite = null): Response {

        
        $unite->setActive(false);
        $this->em->persist($unite);
        $this->em->flush();
        return $this->json([
                            "id"=>$unite->getId(),
                            "abreviation"=>$unite->getAbreviation(),
                            "libelle"=>$unite->getLibelle(),
                        ]);
    }


}