<?php

namespace App\Controller;

use App\Entity\ParamSurveillance;
use App\Entity\SurveillancePatient;
use App\Entity\Unite;
use App\Repository\LPatientRepository;
use App\Repository\ParamSurveillanceRepository;
use App\Repository\SurveillancePatientRepository;
use App\Service\ApiConnector;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class SurveillancePatientController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SurveillancePatientRepository
     */
    private $surveillancePatientRepository;

    /**
     * @var ParamSurveillanceRepository
     */
    private $paramSurveillanceRepository;
    
    /**
     * @var LPatientRepository
     */
    private $lPatientRepository;

    public function __construct(EntityManagerInterface $em,
                                SurveillancePatientRepository $surveillancePatientRepository,
                                LPatientRepository $lPatientRepository,
                                ParamSurveillanceRepository $paramSurveillanceRepository)
    {
        $this->em = $em;
        $this->surveillancePatientRepository = $surveillancePatientRepository;
        $this->lPatientRepository = $lPatientRepository;
        $this->paramSurveillanceRepository = $paramSurveillanceRepository;
    }






    /**
     * @Route("/surveillance/patient/insert",name="surveillance_patient_insert",methods={"POST"})
     * @Route("/surveillance/patient/{id}",name="surveillance_patient_update",methods={"POST","PUT"})
     */
    public function insert(Request $request, SurveillancePatient $surveillancePatient = null, ApiConnector $apiConnector): Response {
        $conts = $this->showAll();
        $request = $apiConnector->transformJsonBody($request);
        $patient = $request->get("patient");
        $patient = $this->getPatient($patient);
        
        $inserted = false;
        $poids = null;
        $taille = null;
        foreach($conts as $key=>$r){
            $id = $r['id'];
            //different de imc
            if($id!=3){
                $value = +$request->get($id);
                $param = $this->getParamSurveill($id);
                $surveillancePatient = new SurveillancePatient();
                $surveillancePatient->setDateConsult(new DateTime());
                $surveillancePatient->setValeur($value);
                $surveillancePatient->setParamSurveillance($param);
                $surveillancePatient->setLpatient($patient);
                //dd($surveillancePatient);
                $this->em->persist($surveillancePatient);
                $this->em->flush();
            }
            if($id==1){
                $poids   = +$request->get($id);
            }else if($id==2){
                $taille  = +$request->get($id);
            }
            $inserted = true;
            //var_dump($key);
        }

        //dd($this->calculIMC($poids,$taille));
        $param = $this->getParamSurveill(3);
        $surveillancePatient = new SurveillancePatient();
        $surveillancePatient->setDateConsult(new DateTime());
        $surveillancePatient->setValeur($this->calculIMC($poids,$taille));
        $surveillancePatient->setParamSurveillance($param);
        $surveillancePatient->setLpatient($patient);
        //dd($taille);
        //dd($this->calculIMC($poids,$taille));
        $this->em->persist($surveillancePatient);
        $this->em->flush();

        
        return $this->json(['inserted'=>$inserted
                        ]);
    }

    /**
     * @Route("/unite/delete/{id}",name="unites_delete")
     */
    public function delete( Unite $unite = null): Response {

        
        $unite->setActive(false);
        $this->em->persist($unite);
        $this->em->flush();
        return $this->json([
                            "id"=>$unite->getId(),
                            "abreviation"=>$unite->getAbreviation(),
                            "libelle"=>$unite->getLibelle(),
                        ]);
    }

        /**
     * @Route("surveillance/patient/mini/graph/{lpatient}",name="mini_graph")
     */
    public function miniGraph($lpatient): Response {
        $const = $this->surveillancePatientRepository->getConstanteMiniGraph($lpatient);
        return $this->json($const);
    }

    public function showAll() {
        return $this->paramSurveillanceRepository->findAllActive();
    }

    
    function getParamSurveill($const){
        return $this->paramSurveillanceRepository->find($const);
    }
    function getPatient($patient){
        return $this->lPatientRepository->find($patient);
    }

    function calculIMC($poids,$taille){
        $taille= ($taille/100)*($taille/100);
        return $poids/$taille;
    }


}