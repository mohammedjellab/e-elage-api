<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\CentreSoin;
use App\Entity\GSpecialite;
use App\Entity\LSpecialite;
use App\Repository\ActeRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\GSpecialiteRepository;
use App\Repository\LSpecialiteRepository;
use App\Repository\SpecialiteRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use App\Service\CentreSoin as ServiceCentreSoin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class CentreSoinController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SpecialiteRepository
     */
    private $gspecialiteRepository;
    /**
     * @var LpecialiteRepository
     */
    private $lspecialiteRepository;

    /**
     *
     * @var CentreSoinRepository
     */
    private $centreSoinRepository ;

    public function __construct(EntityManagerInterface $em,
                                GSpecialiteRepository $gspecialiteRepository,
                                LSpecialiteRepository $lspecialiteRepository,
                                SpecialiteRepository $specialiteRepository,
                                CentreSoinRepository $centreSoinRepository)
    {
        $this->em = $em;
        $this->gspecialiteRepository = $gspecialiteRepository;
        $this->specialiteRepository = $specialiteRepository;
        $this->centreSoinRepository = $centreSoinRepository;
    }
    
    /**
     * @Route("/centre_soins/delete/{id}", name="centre_soins_delete",methods={"GET"})
     */
    public function delete(Request $request, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $centreSoin = new CentreSoin();
        $centreSoin->setActive(false);
        $this->em->persist($centreSoin);
        $this->em->flush();
        return $this->json([ 
                        'etablissement'=>$centreSoin->getEtablissement(),
                        'code'=>$centreSoin->getCode(),
                        'etatConvention'=>$centreSoin->getEtatConvention(),
                        'ice'=>$centreSoin->getIce(),
                        'inpe'=>$centreSoin->getInpe(),
                        'adresse'=>$centreSoin->getAdresse(),
                        'activite'=>$centreSoin->getActivite(),
                        'id'=>$centreSoin->getId(),
        
        ]);
    }

    /**
     * @Route("/centre_soins/insert", name="centre_soins_insert",methods={"POST"})
     * @Route("/centre_soins/update/{id}", name="centre_soins_update",methods={"POST","PUT"})
     */
    public function persistLSpecialite(Request $request, CentreSoin $centreSoin =null, ApiConnector $apiConnector)
    {
       
        $request = $apiConnector->transformJsonBody($request);
        $activite = $request->get('activite');
        $adresse = $request->get('adresse');
        $code = $request->get('code');
        $etablissement = $request->get('etablissement');
        $etatConvention = $request->get('etatConvention');
        $ice = $request->get('ice');
        $inpe = $request->get('inpe');
        if(!$centreSoin){
            $centreSoin = new CentreSoin();
        }
        if($activite){
            $centreSoin->setActivite($activite);
        }
        if($adresse){
            $centreSoin->setAdresse($adresse);
        }
        if($code){
            $centreSoin->setCode($code);
        }
        if($etablissement){
            $centreSoin->setEtablissement($etablissement);
        }
        if($etatConvention){
            $centreSoin->setEtatConvention($etatConvention);
        }
        if($inpe){
            $centreSoin->setInpe($inpe);
        }if($ice){
            $centreSoin->setIce($ice);
        }
        $this->em->persist($centreSoin);
        $this->em->flush();       
        return $this->json([ 
                            'etablissement'=>$centreSoin->getEtablissement(),
                            'code'=>$centreSoin->getCode(),
                            'etatConvention'=>$centreSoin->getEtatConvention(),
                            'ice'=>$centreSoin->getIce(),
                            'inpe'=>$centreSoin->getInpe(),
                            'adresse'=>$centreSoin->getAdresse(),
                            'activite'=>$centreSoin->getActivite(),
                            'id'=>$centreSoin->getId(),
                            
                        ]);
    }

    /**
     * @Route("/lspecialite/list/{centre}", name="specialite_list")
     */
    public function listSpecialiteByCentre($centre, LSpecialiteRepository $repo)
    {
       $specialites = $repo->getSpecialiteByCentre($centre);
       return $this->json($specialites);
    }

    /**
     * @Route("/lspecialite/list", name="specialite_list_all")
     */
    public function listLSpecialiteAll(LSpecialiteRepository $repo)
    {
       $specialites = $repo->getLSpecialiteAndCentre();
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($specialites);
    }

    /**
     * @Route("/centre_soins/list", name="centre_soins_list_all")
     */
    public function list()
    {
       $specialites = $this->centreSoinRepository->findAllEtablissment();
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($specialites);
    }

    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
        
    }

    function getGspecialite($gspecialite){
        return $this->gspecialiteRepository->find($gspecialite);
    }


    /**
     * @Route("/gspecialite/list", name="gspecialite_list")
     */
    public function getGSpecialiteList( GSpecialiteRepository $repo)
    {
       $specialites = $repo->findAll();
       return $this->json($specialites);
    }
    
    /**
     * @Route("/centre_soins/type/{type}/{query}", name="soin-by_type")
     */
    public function get_centre_soin_by_type($type, $query,  GSpecialiteRepository $repo)
    {
        $centres= [];
       if($type == "pharmacie"){
        $centres = $this->centreSoinRepository->findPharmacies($query);
        
       }
       else if($type == "laboratoire"){
        $centres = $this->centreSoinRepository->findLaboratories($query);
       }
       return $this->json($centres);
    }



 





}