<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\Annuaire;
use App\Entity\Holiday;
use App\Repository\ActeRepository;
use App\Repository\AnnuaireRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\HolidayRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class HolidayController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var HolidayRepository
     */
    private $annuaireRepository;

    public function __construct(EntityManagerInterface $em,HolidayRepository $holidayRepository, CentreSoinRepository $centreSoinRepository)
    {
        $this->em = $em;
        $this->holidayRepository = $holidayRepository;
        $this->centreSoinRepository = $centreSoinRepository;
    }

    /**
     * @Route("/annuaires/search/{term}", name="annuaire_search")
     */
    public function search($term): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$this->annuaireRepository->searchByName($term);
        
       return $this->json($actes);
    }


    /**
     * @Route("/holidays/list", name="holidays_list")
     */
    public function list(): Response
    {
        $holidays =$this->holidayRepository->findAllHoliday();
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
        return $this->json($holidays);
    }

     /**
     * @Route("/holidays/insert", name="holidays_insert",methods={"POST"})
     * @Route("/holidays/update/{id}", name="holidays_update",methods={"POST","PUT"})
     */
    public function insert(Request $request,Holiday $holiday=null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $start = $request->get('start');
        $end = $request->get('end');
        $type = $request->get('type');
        $couleur = $request->get('couleur');
        
        if(!$holiday){
            $holiday = NEW Holiday();
        }
        //dd($this->getDateYmdHis($start));
        if($start)
            $holiday->setStart($this->getDateYmdHis($start));
        if($end)
            $holiday->setEnd($this->getDateYmdHis($end));
        if($couleur)
            $holiday->setCouleur($couleur);
        if($type)
            $holiday->setType($type);
        
        //$annuaire->setActive($request->get('active'));
        $this->em->persist($holiday);
        $this->em->flush();
        return $this->json([
            'start'=>$holiday->getStart(),
            'end'=>$holiday->getEnd(),
            'couleur'=>$holiday->getCouleur(),
            'type'=>$holiday->getType(),
            'id'=>$holiday->getId()
        ]);
    }

    public function getDateYmdHis($start){
        $date = strtotime($start);
        $date = date('Y-m-d h:i:s', $date);
        return  \DateTime::createFromFormat('Y-m-d h:i:s', $date);
    }
    /**
     * @Route("/holidays/delete/{id}",name="holidays_delete", methods={"GET"})
     */
    public function delete(Holiday $holiday ): Response {
        $holiday->setActive(false);
        $this->em->persist($holiday);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json([
            'start'=>$holiday->getStart(),
            'end'=>$holiday->getEnd(),
            'type'=>$holiday->getType(),
            'id'=>$holiday->getId()
        ]);
    }
    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }
}