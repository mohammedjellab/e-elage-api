<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\Annuaire;
use App\Repository\ActeRepository;
use App\Repository\AnnuaireRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class AnnuaireController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var AnnuaireRepository
     */
    private $annuaireRepository;

    public function __construct(EntityManagerInterface $em,AnnuaireRepository $annuaireRepository, CentreSoinRepository $centreSoinRepository)
    {
        $this->em = $em;
        $this->annuaireRepository = $annuaireRepository;
        $this->centreSoinRepository = $centreSoinRepository;
    }

    /**
     * @Route("/annuaires/search/{term}", name="annuaire_search")
     */
    public function search($term): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$this->annuaireRepository->searchByName($term);
        
       return $this->json($actes);
    }


    /**
     * @Route("/annuaires/list/{centre}", name="annuaire_list")
     */
    public function list($centre): Response
    {
        $annuaires =$this->annuaireRepository->findAllAnnuaireByCentre($centre);
        return $this->json($annuaires);
    }

     /**
     * @Route("/annuaires/insert", name="annuaires_insert",methods={"POST"})
     * @Route("/annuaires/update/{id}", name="annuaires_update",methods={"POST","PUT"})
     */
    public function insert(Request $request,Annuaire $annuaire=null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $tel = $request->get('tel');
        $gsm = $request->get('gsm');
        $email = $request->get('email');
        $typeRep = $request->get('typeRep');
        $fax = $request->get('fax');
        $centreSoin = $request->get('centreSoin');
     
        if(!$annuaire){
            $annuaire = new Annuaire();
        }
        if($centreSoin){
            $centre = $this->getCentreSoin($centreSoin);
            $annuaire->setCentreSoin($centre);
        }
        if($nom)
            $annuaire->setNom($nom);
        if($prenom)
            $annuaire->setPrenom($prenom);
        if($tel)
            $annuaire->setTel($tel);
        if($gsm)
            $annuaire->setGsm($gsm);
        if($fax)
            $annuaire->setFax($fax);
        if($email)
            $annuaire->setEmail($email);
        if($typeRep)
            $annuaire->setTypRep($typeRep);
        //$annuaire->setActive($request->get('active'));
        $this->em->persist($annuaire);
        $this->em->flush();
        return $this->json([
            'nom'=>$annuaire->getNom(),
            'prenom'=>$annuaire->getPrenom(),
            'tel'=>$annuaire->getTel(),
            'fax'=>$annuaire->getFax(),
            'email'=>$annuaire->getEmail(),
            'typeRep'=>$annuaire->getTypRep(),
            'id'=>$annuaire->getId()
        ]);
    }

    /**
     * @Route("/annuaires/delete/{id}",name="annuaires_delete", methods={"GET"})
     */
    public function delete(Annuaire $annuaire ): Response {
        $annuaire->setActive(false);
        $this->em->persist($annuaire);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json([
            'nom'=>$annuaire->getNom(),
            'prenom'=>$annuaire->getPrenom(),
            'tel'=>$annuaire->getTel(),
            'fax'=>$annuaire->getFax(),
            'email'=>$annuaire->getEmail(),
            'typeRep'=>$annuaire->getTypRep(),
            'id'=>$annuaire->getId()
        ]);
    }
    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }
}