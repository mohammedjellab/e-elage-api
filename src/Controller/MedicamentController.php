<?php

namespace App\Controller;

use App\Entity\Medicament;
use App\Entity\Dci;

use App\Repository\MedicamentRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Psr\Log\LoggerInterface;


/**
 * @Route("/api/medicaments")
 */
class MedicamentController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var MedicamentRepository
     */
    private $medicamentRepository;

    public function __construct(EntityManagerInterface $em, MedicamentRepository $medicamentRepository, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->medicamentRepository = $medicamentRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="medicaments", methods={"GET"})
     */
    public function index(): Response
    {
        $medicaments = $this->medicamentRepository->findAll();
        if (empty($medicaments) || count($medicaments) == 0)
            return $this->json(['message' => 'Aucun enregistrement dans la base de données']);
        return $this->json($medicaments);
    }

    /**
     * @Route("/active", name="active-medicaments")
     */
    public function getActives(): Response
    {
        try {
            $medicaments = $this->medicamentRepository->findByActive();
            if (empty($medicaments) || count($medicaments) == 0)
                return $this->json(['message' => 'No Medicament found']); /* Aucun enregistrement dans la base de données*/
            return $this->json($medicaments);
        }
        catch(Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/add", name="add-medicament", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        try {
            /*$medicament = $this->serializer->deserialize(
                $request->getContent(),
                Medicament::class,
                'json',
                [
                    'circular_reference_limit' => 4,
                    'enable_max_depth' => true,
                    ObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true, 'groups' => ['dcis', 'medicaments'],
                    AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                        return $object->getLibelle();
                    },
                    AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true
                ]
            );
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

            $normalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
            $defaultContext = [
                AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    // $this->logger->info($object);
                    return $object->getDcis();
                },
            ];
            $serializer_ = new Serializer([new ObjectNormalizer(null, null, null, null, null, null, $defaultContext), new GetSetMethodNormalizer(), new ArrayDenormalizer()], [new JsonEncoder()]);
            $medicament = $serializer_->deserialize($request->getContent(), Medicament::class, 'json');
            $medicament->setActive(true);
            $dcis = $this->em->getReference('App\Entity\Dci', )*/

            $obj = json_decode($request->getContent());
            $medicament = $this->json_initialize($obj);
            $this->em->persist($medicament);
            $this->em->flush();

            return $this->json([
                'message' => 'Successfully added',
                'data' => $medicament,
                // 'request' => $request->getContent()
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/edit/{id}", name="update-medicament", methods={"PUT"})
     */
    public function update(Request $request, $id): Response
    {
        try {
            // $medicament = $this->serializer->deserialize($request->getContent(), Medicament::class, 'json', [ObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true]);
            $obj = json_decode($request->getContent());
            $medicament = $this->json_initialize($obj);
            $searchedMedicament = $this->medicamentRepository->find($id);
            if (!$searchedMedicament) {
                throw $this->createNotFoundException(
                    'No medicament found for id ' . $id
                );
            }
            /*$searchedMedicament = */
            $this->initializer($searchedMedicament, $medicament);

            $this->em->flush($searchedMedicament);
            return $this->json([
                'message' => 'Successfully updated',
                'data' => $searchedMedicament
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/delete/{id}", name="delete-medicament", methods={"PUT"})
     */
    public function delete($id): Response
    {
        try {
            $searchedMedicament = $this->medicamentRepository->find($id);
            if (!$searchedMedicament) {
                throw $this->createNotFoundException(
                    'No medicament found for id ' . $id
                );
            }


            // $this->em->remove($searchedLabo);
            $searchedMedicament->setActive(false);
            $this->em->flush($searchedMedicament);
            return $this->json([
                'message' => 'The object with ID ' . $id . ' was successfully deleted'
            ]);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/search/{query}", name="medicament_query")
     */
    public function search($query): Response
    {
        $actes = $this->medicamentRepository->searchInDesignationAndClass($query);
        return $this->json($actes);
    }

    private function initializer(Medicament &$searched, Medicament $medicament)
    {
        $searched->setDesignation($medicament->getDesignation());
        $searched->setUnite($medicament->getUnite());
        $searched->setMedicamentFamille($medicament->getMedicamentFamille());
        $searched->setPosologie($medicament->getPosologie());
        $searched->setForme($medicament->getForme());
        $searched->setDuree($medicament->getDuree());
        $searched->setQuantite($medicament->getQuantite());
        $searched->setConsigne($medicament->getConsigne());
        $searched->setRembourse($medicament->getRembourse());
        $searched->setDosage($medicament->getDosage());
        $searched->setPresentation($medicament->getPresentation());
        $searched->setLaboratoire($medicament->getLaboratoire());
        $searched->setClasseMedical($medicament->getClasseMedical());
        $searched->setPrixVente($medicament->getPrixVente());
        $searched->setPrixAchat($medicament->getPrixAchat());
        $searched->setPrixBRemboursementPPM($medicament->getPrixBRemboursementPPM());
        $searched->setPrixHospitalier($medicament->getPrixHospitalier());
        $searched->setBaseRembousementPPV($medicament->getBaseRembousementPPV());
        $searched->setPrixBaseRemboursement($medicament->getPrixBaseRemboursement());
        $searched->setStatut($medicament->getStatut());
        $searched->setCodeBarre($medicament->getCodeBarre());
        $searched->setType($medicament->getType() ? $medicament->getType() : null);
        $searched->setSurdosage($medicament->getSurdosage());
        $searched->setCodeAtc($medicament->getCodeAtc());
        $searched->setNatureProduit($medicament->getNatureProduit());
        $searched->setUtilisation($medicament->getUtilisation());
        $searched->setContreIndications($medicament->getContreIndications());
        $searched->setEffetsIndesirables($medicament->getEffetsIndesirables());
        $searched->setConditionsPrescription($medicament->getConditionsPrescription());
        $searched->setIndications($medicament->getIndications());
        $searched->setRisquePotentiel($medicament->getRisquePotentiel());
        $searched->setActive(true);
        //return $searched;
    }

    private function json_initialize($obj)
    {
        $medicament = new Medicament();
        $medicament->setNomCommercial($obj->{'nomCommercial'});
        $medicament->setDesignation($obj->{'designation'});
        $medicament->setUnite(isset($obj->{'unite'}) ? $obj->{'unite'} : null);
        $medicament->setMedicamentFamille(isset($obj->{'medicamentFamille'}) ? $obj->{'medicamentFamille'} : null);
        $medicament->setPosologie(isset($obj->{'posologie'}) ? $obj->{'posologie'} : null);
        $medicament->setForme(isset($obj->{'forme'}) ? $obj->{'forme'} : null);
        $medicament->setDuree(isset($obj->{'duree'}) ? $obj->{'duree'} : null);
        $medicament->setQuantite(isset($obj->{'quantite'}) ? $obj->{'quantite'} : null);
        $medicament->setConsigne(isset($obj->{'consigne'}) ? $obj->{'consigne'} : null);
        $medicament->setRembourse(isset($obj->{'rembourse'}) ? $obj->{'rembourse'} : true);
        $medicament->setDosage(isset($obj->{'dosage'}) ? $obj->{'dosage'} : null);
        $medicament->setPresentation(isset($obj->{'presentation'}) ? $obj->{'presentation'} : null);
        $medicament->setLaboratoire(isset($obj->{'laboratoire'}) ? $obj->{'laboratoire'} : null);
        $medicament->setClasseMedical(isset($obj->{'classeMedical'}) ? $obj->{'classeMedical'} : null);
        $medicament->setPrixVente(isset($obj->{'prixVente'}) ? $obj->{'prixVente'} : null);
        $medicament->setPrixAchat(isset($obj->{'prixAchat'}) ? $obj->{'prixAchat'} : null);
        $medicament->setPrixBRemboursementPPM(isset($obj->{'prixBRemboursementPPM'}) ? $obj->{'prixBRemboursementPPM'} : null);
        $medicament->setPrixHospitalier(isset($obj->{'prixHospitalier'}) ? $obj->{'prixHospitalier'} : null);
        $medicament->setBaseRembousementPPV(isset($obj->{'baseRemboursementPPV'}) ? $obj->{'baseRemboursementPPV'} : 'hello');
        $medicament->setPrixBaseRemboursement(isset($obj->{'prixBaseRemboursement'}) ? $obj->{'prixBaseRemboursement'} : null);
        $medicament->setStatut(isset($obj->{'statut'}) ? $obj->{'statut'} : 'commercialisé');
        $medicament->setCodeBarre(isset($obj->{'codeBarre'}) ? $obj->{'codeBarre'} : 'null');
        $medicament->setType(isset($obj->{'type'}) ? $obj->{'type'} : 'null');
        $medicament->setSurdosage(isset($obj->{'surdosage'}) ? $obj->{'surdosage'} : 'null');
        $medicament->setCodeAtc(isset($obj->{'codeAtc'}) ? $obj->{'codeAtc'} : 'null');
        $medicament->setNatureProduit(isset($obj->{'natureProduit'}) ? $obj->{'natureProduit'} : 'null');
        $medicament->setUtilisation(isset($obj->{'utilisation'}) ? $obj->{'utilisation'} : 'null');
        $medicament->setContreIndications(isset($obj->{'contreIndications'}) ? $obj->{'contreIndications'} : 'null');
        $medicament->setEffetsIndesirables(isset($obj->{'effetsIndesirables'}) ? $obj->{'effetsIndesirables'} : 'null');
        $medicament->setConditionsPrescription(isset($obj->{'conditionsPrescription'}) ? $obj->{'conditionsPrescription'} : 'null');
        $medicament->setIndications(isset($obj->{'indications'}) ? $obj->{'indications'} : 'null');
        $medicament->setRisquePotentiel(isset($obj->{'risquePotentiel'}) ? $obj->{'risquePotentiel'} : 'null');
        /*for($i = 0; $i < count((array)$obj->{'dcis'}); $i++){
        	$medicament->addDci($obj->{'dcis'}[$i]);
        }*/
        $medicament->setActive(true);
        /*$dcis = isset($obj->{'dcis'}) ? (array)$obj->{'dcis'} : null;
        // $medicament->setDcis($dcis);
        for($i = 0; $i < count($dcis); $i++)  {
            $dci = new Dci();
            $dci->setId($dcis[$i]->{'id'});
            $dci->setLibelle($dcis[$i]->{'libelle'});
            $dci->setActive($dcis[$i]->{'active'});
            $medicament->addDci($dci);
            // $this->em->persist($dci);
            //$this->em->persist($medicament);
            //$this->em->flush();
        }*/
        // var_dump($medicament);
        
        return $medicament;
    }

}
