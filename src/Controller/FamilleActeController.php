<?php

namespace App\Controller;

use App\Entity\FamilleActe;
use App\Repository\FamilleActeRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class FamilleActeController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var FamilleActeRepository
     */
    private $familleActeRepository;

    public function __construct(EntityManagerInterface $em,FamilleActeRepository $familleActeRepository)
    {
        $this->em = $em;
        $this->familleActeRepository = $familleActeRepository;
    }

    /**
     * @Route("/famille_actes/search/{term}", name="famille_actes_search")
     */
    public function search($term): Response
    {
       $actes =$this->familleActeRepository->searchByName($term);
       return $this->json($actes);
    }


    /**
     * @Route("/famille_actes/list", name="famille_actes_list")
     */
    public function list()
    {
       $familles =$this->familleActeRepository->findAllFamille();
       //dd($actes);
       return $this->json($familles);
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
    }

   /**
     * @Route("/famille_actes/insert", name="famille_actes_insert",methods={"POST"})
     * @Route("/famille_actes/update/{id}", name="famille_actes_update",methods={"POST","PUT"})
     */
    public function insert(Request $request,FamilleActe $familleActe=null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        
        if(!$familleActe){
            $familleActe = new FamilleActe();
        }
        $familleActe->setLibelle($libelle);
        //$mutuelle->setActive($request->get('active'));
        $this->em->persist($familleActe);
        $this->em->flush();
        return $this->json([
            'libelle'=>$familleActe->getLibelle(),
            'id'=>$familleActe->getId()
        ]);
    }

    /**
     * @Route("/famille_actes/delete/{id}",name="famille_actes_delete", methods={"GET"})
     */
   public function delete(FamilleActe $familleActe ): Response {
      $familleActe->setActive(false);
      $this->em->persist($familleActe);
      $this->em->flush();
      //$salle = $this->salleRepository->find($id);
      return $this->json([
         'libelle'=>$familleActe->getLibelle(),
         'id'=>$familleActe->getId()
      ]);
   }

}