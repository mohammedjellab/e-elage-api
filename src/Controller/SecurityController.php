<?php

namespace App\Controller;

use App\Entity\CentreSoin;
use App\Entity\User;
use App\Form\RegistrationType;
use App\Repository\CentreSoinRepository;
use App\Repository\SpecialiteRepository;
use App\Repository\TypeUserRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Repository\RepositoryFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
//use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{

    /**
     * @var UserRepository
     */
     /**
     * @var NormalizerInterface
     */
     
    private $normalizer;

    /**
     * @var ObjectManager
     */
    private $em;
    public function __construct(UserRepository $repository){
        //$this->em=$em;
        $this->repository=$repository;

    }

    /**
     * @Route("/login",name="api_login",methods="POST")
     * @return Response
     */
    public function apiLogin(UserRepository $repository){
        //$user = $repository->find(1);
        //dd($user);
        $user = $this->getUser();
        
        return $this->json([
            'id'=> $user->getId(),
            'username'=> $user->getUsername(),
            'lastname'=> $user->getLastname(),
            'firstname'=> $user->getFirstname(),
            'tel'=> $user->getTel(),
            'roles'=> $user->getRoles(),
            
            'centreSoin'=> $user->getCentreSoin()->getId()
        ]);
    }


    /**
     * @Route("/apip/login2", name="app_login2",methods="POST")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $user = $this->getUser();
        return $this->json([
            'id'=> $user->getId(),
            'username'=> $user->getUsername(),
            'lastname'=> $user->getLastname(),
            'firstname'=> $user->getFirstname(),
            'tel'=> $user->getTel(),
            'roles'=> $user->getRoles(),
            'centreSoin'=> $user->getCentreSoin()
        ]);
    }

    /**
     * @Route("/api/logout", name="app_logout")
     */
    public function logout(): Response {
        return $this->json(['logout'=>true]);
        //throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/api/register",name="register",methods={"POST"})
     * @Route("/api/register/{id}/edit",name="user-edit")
     * @return Response
     */
    public function register(User $user=null,Request $request,
                             UserPasswordEncoderInterface $encoders,
                             SerializerInterface $serializer,
                             EntityManagerInterface $em,
                             CentreSoinRepository $centreSoinRepository,
                             SpecialiteRepository $specialiteRepository,
                             TypeUserRepository $typeUserRepository
                             )
    {         
        $json = $request->getContent();
        //dd($json);die();
        $user = $serializer->deserialize($json,User::class,'json');
        //$user->setCreatedAt(new \DateTime());
        
        $hash=$encoders->encodePassword($user,$user->getPassword());
        $user->setPassword($hash);
        //dd($user);
        $em->persist($user);
        $em->flush();
        //dd($user);
        /*$response = new Response($user,201,[
            'Content-Type'=>  'application/json',
            'Access-Control-Allow-Origin'=> '*',
            'Access-Control-Allow-Headers'=> 'Content-Type',
            'Access-Control-Allow-Methods'=> 'GET, POST, DELETE, PUT, OPTIONS'
        ]);*/
        return $this->json([
            'id'=>$user->getId(),
            'lastname'=>$user->getLastname(),
            'firstname'=>$user->getFirstname(),
            'tel'=>$user->getTel(),
            'username'=>$user->getUsername(),
            'email'=>$user->getEmail(),
        ]);
    }

    /**
     * @Route("/users/upload/{user}", name="users_upload", methods={"POST"})
     */
    public function uploadFile(Request $request,User $user): Response
    { 
        $file = $request->files->get('image');
        $extention = $request->get('extention');
        $date = new \DateTime();
        $name= $date->format('Y-m-dH-i-s').rand().'.'.$extention;    
        //dd($file);
        $user->setDocument($name);

        $file->move('images/users', $name);
        $this->em->persist($user);
        $this->em->flush();
       return $this->json($user);
    }
}