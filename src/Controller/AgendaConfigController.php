<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\AgendaConfig;
use App\Repository\ActeRepository;
use App\Repository\AgendaConfigRepository;
use App\Repository\TypeConsultationRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class AgendaConfigController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;


    public function __construct(EntityManagerInterface $em,AgendaConfigRepository $agendaConfigRepository)
    {
        $this->em = $em;
        $this->agendaConfigRepository = $agendaConfigRepository;
    }


    /**
     * @Route("/agenda_config/{centreSoin}", name="agenda_config_list")
     */
    public function show($centreSoin): Response
    {
       $config =$this->agendaConfigRepository->configByCentreSoin($centreSoin);
       return $this->json($config);
    }

 
    /**
     * @Route("/agenda_config/insert", name="agenda_config_insert",methods={"POST"})
     * @Route("/agenda_config/update/{id}", name="agenda_config_update",methods={"POST","PUT"})
     */
    public function insert(AgendaConfig $agendaConfig = null,Request $request, ApiConnector $apiConnector )
    {

        if(!$agendaConfig){
            $agendaConfig = new AgendaConfig();
        }
        
        $request = $apiConnector->transformJsonBody($request);
        $firstDay = $request->get('firstDay');
        $granularite = $request->get('granularite');
        $heureDebutOuvrable = $request->get('heureDebutOuvrable');
        $heureFermeture = $request->get('heureFermeture');
        $heureFinOuvrable = $request->get('heureFinOuvrable');
        $heureOuverture = $request->get('heureOuverture');
        $modeAffichage = $request->get('modeAffichage');
        $zoom = $request->get('zoom');
        $maxActe = $request->get('maxActe');

        $heureFinOuvrable = $this->convertDate($heureFinOuvrable);
        $heureOuverture = $this->convertDate($heureOuverture);
        $heureFermeture = $this->convertDate($heureFermeture);
        $heureDebutOuvrable = $this->convertDate($heureDebutOuvrable);
        $granularite = $this->convertDate($granularite);

        $agendaConfig->setHeureDebutOuvrable($heureDebutOuvrable);
        $agendaConfig->setHeureOuverture($heureOuverture);
        $agendaConfig->setGranularite($granularite);
        $agendaConfig->setModeAffichage($modeAffichage);
        $agendaConfig->setHeureFermeture($heureFermeture);
        $agendaConfig->setHeureFinOuvrable($heureFinOuvrable);
        $agendaConfig->setFirstDay($firstDay);
        $agendaConfig->setZoom($zoom);
        $agendaConfig->setMaxActe($maxActe);
        $this->em->persist($agendaConfig);
        $this->em->flush();
        //dd($agendaEvenement);
        return $this->json([
            "id"=>$agendaConfig->getId()
        ]);

    }

    function convertDate($date){
        //convert date heureFinOuvrable
        $date = strtotime($date);
        $date = date('H:i', $date);     
        $date = \DateTime::createFromFormat('H:i', $date);
        return $date;
    }



  

   




}