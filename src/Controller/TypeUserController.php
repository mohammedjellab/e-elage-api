<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\CentreSoin;
use App\Entity\GSpecialite;
use App\Entity\LSpecialite;
use App\Entity\TypeUser;
use App\Repository\ActeRepository;
use App\Repository\CentreSoinRepository;
use App\Repository\GSpecialiteRepository;
use App\Repository\LSpecialiteRepository;
use App\Repository\SpecialiteRepository;
use App\Repository\TypeUserRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use App\Service\CentreSoin as ServiceCentreSoin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class TypeUserController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;


    /**
     *
     * @var CentreSoinRepository
     */
    private $centreSoinRepository ;

    public function __construct(EntityManagerInterface $em,
                                
                                TypeUserRepository $typeUserRepository,
                                CentreSoinRepository $centreSoinRepository)
    {
        $this->em = $em;
        $this->centreSoinRepository = $centreSoinRepository;
        $this->typeUserRepository = $typeUserRepository;
    }
    
  



    /**
     * @Route("/type_users/list", name="type_users_list")
     */
    public function list()
    {
       $types = $this->typeUserRepository->getListTypeUser();
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json($types);
    }

    

  /**
     * @Route("/type_users/insert", name="type_users_insert",methods={"POST"})
     * @Route("/type_users/update/{id}", name="type_users_update",methods={"POST","PUT"})
     */
    public function insert(Request $request,TypeUser $typeUser=null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        
        if(!$typeUser){
            $typeUser = new TypeUser();
        }
        $typeUser->setLibelle($libelle);
        //$typeUser->setActive($request->get('active'));
        $this->em->persist($typeUser);
        $this->em->flush();
        return $this->json([
            'libelle'=>$typeUser->getLibelle(),
            'id'=>$typeUser->getId()
        ]);
    }

    /**
     * @Route("/type_users/delete/{id}",name="type_users_delete", methods={"GET"})
     */
    public function delete(TypeUser $typeUser ): Response {
        $typeUser->setActive(false);
        $this->em->persist($typeUser);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json([
            'libelle'=>$typeUser->getLibelle(),
            'id'=>$typeUser->getId()
        ]);
    }





}