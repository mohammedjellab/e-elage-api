<?php

namespace App\Controller;

use App\Entity\Acte;
use App\Entity\Mutuelle;
use App\Repository\ActeRepository;
use App\Repository\MutuelleRepository;
use App\Repository\UserRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class MutuelleController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var MutuelleRepository
     */
    private $mutuelleRepository;

    public function __construct(EntityManagerInterface $em,MutuelleRepository $mutuelleRepository)
    {
        $this->em = $em;
        $this->mutuelleRepository = $mutuelleRepository;
    }

    /**
     * @Route("/mutuelles/search/{term}", name="mutuelle_search")
     */
    public function search($term): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $actes =$this->mutuelleRepository->searchByName($term);
        
       return $this->json($actes);
    }


    /**
     * @Route("/mutuelles/list", name="mutuelle_list")
     */
    public function list(): Response
    {
        $mutuelles =$this->mutuelleRepository->findAllMutuelle();
        return $this->json($mutuelles);
    }

     /**
     * @Route("/mutuelles/insert", name="mutuelles_insert",methods={"POST"})
     * @Route("/mutuelles/update/{id}", name="mutuelles_update",methods={"POST","PUT"})
     */
    public function insert(Request $request,Mutuelle $mutuelle=null, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        
        if(!$mutuelle){
            $mutuelle = new Mutuelle();
        }
        $mutuelle->setLibelle($libelle);
        //$mutuelle->setActive($request->get('active'));
        $this->em->persist($mutuelle);
        $this->em->flush();
        return $this->json([
            'libelle'=>$mutuelle->getLibelle(),
            'id'=>$mutuelle->getId()
        ]);
    }

    /**
     * @Route("/mutuelles/delete/{id}",name="mutuelles_delete", methods={"GET"})
     */
    public function delete(Mutuelle $mutuelle ): Response {
        $mutuelle->setActive(false);
        $this->em->persist($mutuelle);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json([
            'libelle'=>$mutuelle->getLibelle(),
            'id'=>$mutuelle->getId()
        ]);
    }

}