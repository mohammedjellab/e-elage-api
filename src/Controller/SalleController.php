<?php

namespace App\Controller;

use App\Entity\Salle;
use App\Repository\CentreSoinRepository;
use App\Repository\SalleRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class SalleController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SalleRepository
     */
    private $salleRepository;

    public function __construct(EntityManagerInterface $em,
                                CentreSoinRepository $centreSoinRepository,
                                SalleRepository $salleRepository)
    {
        $this->em = $em;
        $this->salleRepository = $salleRepository;
        $this->centreSoinRepository = $centreSoinRepository;
    }

    /**
     * @Route("/salles/search/{term}", name="salle_search")
     */
    public function search($term): Response
    {
       $actes =$this->salleRepository->searchByName($term);
       return $this->json($actes);
    }

    /**
     * @Route("/salles/{centre}/list", name="salle_list")
     */
    public function list($centre)
    {
       $salle =$this->salleRepository->sallesByCentre($centre);
       //dd($salle);
       return $this->json($salle);
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
    }

    /**
     * @Route("/salles/update/{id}",name="salles_update", methods={"POST","PUT"})
     * @Route("/salles/insert",name="salles_insert", methods={"POST"})
     */
    public function update(Salle $salle = null,Request $request, ApiConnector $apiConnector): Response {
        
        if(!$salle){
            $salle = new Salle();
        }
        
        $request = $apiConnector->transformJsonBody($request);
        $libelle = $request->get('libelle');
        $centreSoin = $request->get('centreSoin');
        if($centreSoin){
            $centreSoin = $this->getCentreSoin($centreSoin);
            $salle->setCentreSoin($centreSoin);
        }

        $salle->setLibelle($libelle);
       
        $this->em->persist($salle);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json($salle);
    }

    /**
     * @Route("/salles/delete/{id}",name="salles_delete", methods={"GET"})
     */
    public function delete(Salle $salle ): Response {
        $salle->setActive(false);
        $this->em->persist($salle);
        $this->em->flush();
        //$salle = $this->salleRepository->find($id);
        return $this->json($salle);
    }

    /**
     * @Route("/salles/{id}",name="salless_show")
     */
    public function show(Salle $salle): Response {
        //$salle = $this->salleRepository->find($id);
        return $this->json($salle);
    }
    /**
     * @Route("/salles/centre/{centre}",name="salles_centre")
     */
    public function salleByCentre($centre): Response {
        $salle = $this->salleRepository->sallesByCentre($centre);
        return $this->json($salle);
    }

    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }

}