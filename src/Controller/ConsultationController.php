<?php

namespace App\Controller;

use App\Entity\Consultation;
use App\Entity\Patient;
use App\Repository\CentreSoinRepository;
use App\Repository\ConsultationRepository;
use App\Repository\LPatientRepository;
use App\Repository\TypeConsultationRepository;
use App\Service\ApiConnector;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class ConsultationController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ConsultationRepository
     */
    private $consultationRepository;

    /**
     * @var LPatientRepository
     */
    private $lpatienRepository;

    /**
     * @var CentreSoinRepository
     */
    private $centreSoinRepository;
    public function __construct(EntityManagerInterface $em,
                                LPatientRepository $lpatienRepository,
                                CentreSoinRepository $centreSoinRepository,
                                TypeConsultationRepository $typeConsultationRepository,
                                ConsultationRepository $consultationRepository)
    {
        $this->em = $em;
        $this->consultationRepository = $consultationRepository;
        $this->typeConsultationRepository = $typeConsultationRepository;
        $this->lpatienRepository = $lpatienRepository;
        $this->centreSoinRepository = $centreSoinRepository;
        
    }

    /**
     * @Route("/api/consultations/list/{patient}", name="consultations_patient")
     */
    public function list(Patient $patient): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $consultations =$this->consultationRepository->listConsultations($patient);
        //dd($mutuelleFaites);
        
       return $this->json($consultations);
       //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
    }

    /**
     * @Route("/consultation/list/{patient}", name="consultations_list_patient")
     */
    public function listConsultationByLpatient($patient): Response
    {
       $consultations =$this->consultationRepository->listConsultationsByLpatient($patient);
       
       return $this->json($consultations);
    }


    /**
     * @Route("/consultation/insert", name="consultation_insert",methods={"POST"})
     * @Route("/consultation/update/{id}", name="consultation_update",methods={"POST","PUT"})
     */
    public function persist(Request $request, ApiConnector $apiConnector)
    {
        $request = $apiConnector->transformJsonBody($request);
        $consultation = new Consultation();
        $montant = $request->get('montant');
        $centreSoin = $request->get('centreSoin');
        $lpatient = $request->get('lpatient');
        $typeConsultation = $request->get('typeConsultation');
        if($centreSoin){
            $centreSoin = $this->getCentreSoin($centreSoin);
            $consultation->setCentreSoin($centreSoin);
        }

        if($lpatient){
            $lpatient = $this->getPatient($lpatient);
            $consultation->setLpatient($lpatient);
        }
        if($typeConsultation){
            $typeConsultation = $this->getTypeConsultation($typeConsultation);
            $consultation->setTypeConsultation($typeConsultation);
        }
        $consultation->setMontant($montant);
        $consultation->setDateConsultation(new DateTime());
        $this->em->persist($consultation);
        $this->em->flush();
        return $this->json(
               [
                   'id'=>$consultation->getId(),
                   'montant'=>$consultation->getMontant(),
               ]);
    }


    function getPatient($patient){
        return $this->lpatienRepository->find($patient);
    }
    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }

    function getTypeConsultation($typeConsultation){
        return $this->typeConsultationRepository->find($typeConsultation);
    }

}