<?php

namespace App\Controller;

use App\Entity\LPatient;
use App\Entity\Patient;
use App\Entity\MutuelleFaite;
use App\Repository\CentreSoinRepository;
use App\Repository\LPatientRepository;
use App\Repository\MutuelleFaiteRepository;
use App\Repository\MutuelleRepository;
use App\Service\ApiConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class MutuelleFaiteController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var MutuelleFaiteRepository
     */
    private $mutuelleFaiteRepository;

    public function __construct(EntityManagerInterface $em,
                                MutuelleFaiteRepository $mutuelleFaiteRepository,
                                LPatientRepository $lpatientRepository,
                                CentreSoinRepository $centreSoinRepository,
                                MutuelleRepository $mutuelleRepository,
                                ApiConnector $apiConnector
                                )
    {
        $this->em = $em;
        $this->mutuelleFaiteRepository = $mutuelleFaiteRepository;
        $this->mutuelleRepository = $mutuelleRepository;
        $this->lpatientRepository = $lpatientRepository;
        $this->centreSoinRepository = $centreSoinRepository;
        $this->apiConnector = $apiConnector;

    }

    /**
     * @Route("/mutuelle_faites/list/{patient}", name="mutuelle_faites_patient")
     */
    public function list(LPatient $patient): Response
    {
       // $repo= $this->getDoctrine()->getRepository(Acte::class);
        //$actes= $repo->findAll();
        //$actes =$this->acteRepository->findById(2);
        $mutuelleFaites =$this->mutuelleFaiteRepository->findByPatient($patient);
        //dd($mutuelleFaites);
       // return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController']);
       return $this->json($mutuelleFaites);
    }

      /**
     * @Route("/mutuelle_faites/insert", name="mutuelle_faites_insert")
     */
    public function persist(Request $request,ApiConnector $apiConnector): Response
    {
        $request = $apiConnector->transformJsonBody($request);
        $centreSoin = $request->get('centreSoin');
        $observation = $request->get('observation');
        $remplie = $request->get('remplie');
        $lpatient_id = $request->get('lpatient_id');
        $dateMut = $request->get('dateMut');
        $mutuelle = $request->get('mutuelle');
        $valider = $request->get('valider');

      
        $mutuelleFaite = new MutuelleFaite();
        if($centreSoin){
            $centre = $this->getCentreSoin($centreSoin);
            $mutuelleFaite->setCentreSoin($centre);
        }

        if($lpatient_id){
            $patient = $this->getPatient($lpatient_id);
            $mutuelleFaite->setLpatient($patient);
        }
        if($mutuelle){
            $mutuelle = $this->getMutuelle($mutuelle);
            $mutuelleFaite->setMutuelle($mutuelle);
        }
        if($remplie){
            $mutuelleFaite->setFaite($remplie);
        }
        if($valider){
            $mutuelleFaite->setValider($valider);
        }
        if($observation){
            $mutuelleFaite->setObservation($observation);
        }
        if($dateMut){
            $dateMut = strtotime($dateMut);
            $dateReturned = date('Y-m-d', $dateMut);
            $dateMut = date('Y-m-d h:i:s', $dateMut);
            $dateMut = \DateTime::createFromFormat('Y-m-d h:i:s', $dateMut);
            $mutuelleFaite->setDate($dateMut);
        }
        
        $this->em->persist($mutuelleFaite);
        $this->em->flush();

        
        //$patients =$this->patientRepository->getPatients();
        //return $this->render('agenda_evenement/index.html.twig', ['controller_name' => 'AgendaEvenementController',]);
       return $this->json([
                "id"=>$mutuelleFaite->getId(),
                "remplie"=>$mutuelleFaite->getFaite(),
                "valider"=>$mutuelleFaite->getValider(),
                "date"=>$dateReturned,
               ]);
    }

    function getCentreSoin($centreSoin){
        return $this->centreSoinRepository->find($centreSoin);
    }
    function getMutuelle($mutuelle){
        return $this->mutuelleRepository->find($mutuelle);
    }

    function getPatient($patient){
        return $this->lpatientRepository->find($patient);
    }


}