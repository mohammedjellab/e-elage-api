<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ParamSurveillanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ParamSurveillanceRepository::class)
 */
class ParamSurveillance
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $abreviation;

    /**
     * @ORM\ManyToOne(targetEntity=Unite::class, inversedBy="paramSurveillances")
     */
    private $unite;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $couleur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $borneMax;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $BorneMin;

    /**
     * @ORM\OneToMany(targetEntity=SurveillancePatient::class, mappedBy="paramSurveillance")
     */
    private $surveillancePatients;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->surveillancePatients = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getAbreviation(): ?string
    {
        return $this->abreviation;
    }

    public function setAbreviation(string $abreviation): self
    {
        $this->abreviation = $abreviation;

        return $this;
    }

    public function getUnite(): ?Unite
    {
        return $this->unite;
    }

    public function setUnite(?Unite $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getBorneMax(): ?string
    {
        return $this->borneMax;
    }

    public function setBorneMax(string $borneMax): self
    {
        $this->borneMax = $borneMax;

        return $this;
    }

    public function getBorneMin(): ?string
    {
        return $this->BorneMin;
    }

    public function setBorneMin(string $BorneMin): self
    {
        $this->BorneMin = $BorneMin;

        return $this;
    }

    /**
     * @return Collection|SurveillancePatient[]
     */
    public function getSurveillancePatients(): Collection
    {
        return $this->surveillancePatients;
    }

    public function addSurveillancePatient(SurveillancePatient $surveillancePatient): self
    {
        if (!$this->surveillancePatients->contains($surveillancePatient)) {
            $this->surveillancePatients[] = $surveillancePatient;
            $surveillancePatient->setParamSurveillance($this);
        }

        return $this;
    }

    public function removeSurveillancePatient(SurveillancePatient $surveillancePatient): self
    {
        if ($this->surveillancePatients->removeElement($surveillancePatient)) {
            // set the owning side to null (unless already changed)
            if ($surveillancePatient->getParamSurveillance() === $this) {
                $surveillancePatient->setParamSurveillance(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
