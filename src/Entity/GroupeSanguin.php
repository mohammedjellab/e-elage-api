<?php

namespace App\Entity;

use App\Repository\GroupeSanguinRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GroupeSanguinRepository::class)
 */
class GroupeSanguin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $libelle;



    /**
     * @ORM\OneToMany(targetEntity=GPatient::class, mappedBy="groupSanguin")
     */
    private $gPatients;

    public function __construct()
    {
        $this->gPatients = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }



    /**
     * @return Collection|GPatient[]
     */
    public function getGPatients(): Collection
    {
        return $this->gPatients;
    }

    public function addGPatient(GPatient $gPatient): self
    {
        if (!$this->gPatients->contains($gPatient)) {
            $this->gPatients[] = $gPatient;
            $gPatient->setGroupSanguin($this);
        }

        return $this;
    }

    public function removeGPatient(GPatient $gPatient): self
    {
        if ($this->gPatients->removeElement($gPatient)) {
            // set the owning side to null (unless already changed)
            if ($gPatient->getGroupSanguin() === $this) {
                $gPatient->setGroupSanguin(null);
            }
        }

        return $this;
    }

}
