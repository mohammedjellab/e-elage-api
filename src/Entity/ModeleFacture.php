<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ModeleFactureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ModeleFactureRepository::class)
 */
class ModeleFacture
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $typeMime;

    /**
     * @ORM\OneToMany(targetEntity=Devis::class, mappedBy="modeleDevis")
     */
    private $devis;

    /**
     * @ORM\ManyToOne(targetEntity=Facture::class, inversedBy="modeleFacture")
     */
    private $modeleFacture;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->devis = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getTypeMime(): ?string
    {
        return $this->typeMime;
    }

    public function setTypeMime(string $typeMime): self
    {
        $this->typeMime = $typeMime;

        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis(): Collection
    {
        return $this->devis;
    }

    public function addDevi(Devis $devi): self
    {
        if (!$this->devis->contains($devi)) {
            $this->devis[] = $devi;
            $devi->setModeleDevis($this);
        }

        return $this;
    }

    public function removeDevi(Devis $devi): self
    {
        if ($this->devis->removeElement($devi)) {
            // set the owning side to null (unless already changed)
            if ($devi->getModeleDevis() === $this) {
                $devi->setModeleDevis(null);
            }
        }

        return $this;
    }

    public function getModeleFacture(): ?Facture
    {
        return $this->modeleFacture;
    }

    public function setModeleFacture(?Facture $modeleFacture): self
    {
        $this->modeleFacture = $modeleFacture;

        return $this;
    }
}
