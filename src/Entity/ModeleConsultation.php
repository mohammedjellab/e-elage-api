<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ModeleConsultationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ModeleConsultationRepository::class)
 */
class ModeleConsultation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="modeleConsultations")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $couleur;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isGras;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $police;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $taille;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $format;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(?string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getIsGras(): ?bool
    {
        return $this->isGras;
    }

    public function setIsGras(?bool $isGras): self
    {
        $this->isGras = $isGras;

        return $this;
    }

    public function getPolice(): ?string
    {
        return $this->police;
    }

    public function setPolice(?string $police): self
    {
        $this->police = $police;

        return $this;
    }

    public function getTaille(): ?int
    {
        return $this->taille;
    }

    public function setTaille(?int $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(?string $format): self
    {
        $this->format = $format;

        return $this;
    }
}
