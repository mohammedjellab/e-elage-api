<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ConsultationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ConsultationRepository::class)
 */
class Consultation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateConsultation;

    /**
     * @ORM\ManyToOne(targetEntity=TypeConsultation::class, inversedBy="typeConsultation")
     */
    private $typeConsultation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Source;

    /**
     * @ORM\OneToMany(targetEntity=Consultation::class, mappedBy="patient")
     */
    private $consultations;

    /**
     * @ORM\OneToMany(targetEntity=Courier::class, mappedBy="consultation")
     */
    private $couriers;

    /**
     * @ORM\OneToMany(targetEntity=HistoPaiement::class, mappedBy="consultation")
     */
    private $histoPaiements;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="consultations")
     */
    private $centreSoin;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=LPatient::class, inversedBy="consultations")
     */
    private $lpatient;

    /**
     * @ORM\ManyToMany(targetEntity=Reglement::class, mappedBy="consultation")
     */
    private $reglements;

    /**
     * @ORM\OneToMany(targetEntity=ConsultationHasReglemnt::class, mappedBy="hasReglement")
     */
    private $hasReglement;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class, inversedBy="consultationAgenda")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agenda;

    /**
     * @ORM\OneToMany(targetEntity=Consultation::class, mappedBy="agenda")
     */
    private $consultationAgenda;


    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->consultations = new ArrayCollection();
        $this->couriers = new ArrayCollection();
        $this->histoPaiements = new ArrayCollection();
        $this->hasReglement = new ArrayCollection();
        $this->consultationAgenda = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDateConsultation(): ?\DateTimeInterface
    {
        return $this->dateConsultation;
    }

    public function setDateConsultation(\DateTimeInterface $dateConsultation): self
    {
        $this->dateConsultation = $dateConsultation;

        return $this;
    }

    public function getTypeConsultation(): ?TypeConsultation
    {
        return $this->typeConsultation;
    }

    public function setTypeConsultation(?TypeConsultation $typeConsultation): self
    {
        $this->typeConsultation = $typeConsultation;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->Source;
    }

    public function setSource(string $Source): self
    {
        $this->Source = $Source;

        return $this;
    }



    /**
     * @return Collection|self[]
     */
    public function getConsultations(): Collection
    {
        return $this->consultations;
    }

    public function addConsultation(self $consultation): self
    {
        if (!$this->consultations->contains($consultation)) {
            $this->consultations[] = $consultation;
        }

        return $this;
    }

    public function removeConsultation(self $consultation): self
    {
        if ($this->consultations->removeElement($consultation)) {
            // set the owning side to null (unless already changed)
           
        }

        return $this;
    }




 

    /**
     * @return Collection|Courier[]
     */
    public function getCouriers(): Collection
    {
        return $this->couriers;
    }

    public function addCourier(Courier $courier): self
    {
        if (!$this->couriers->contains($courier)) {
            $this->couriers[] = $courier;
            $courier->setConsultation($this);
        }

        return $this;
    }

    public function removeCourier(Courier $courier): self
    {
        if ($this->couriers->removeElement($courier)) {
            // set the owning side to null (unless already changed)
            if ($courier->getConsultation() === $this) {
                $courier->setConsultation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HistoPaiement[]
     */
    public function getHistoPaiements(): Collection
    {
        return $this->histoPaiements;
    }

    public function addHistoPaiement(HistoPaiement $histoPaiement): self
    {
        if (!$this->histoPaiements->contains($histoPaiement)) {
            $this->histoPaiements[] = $histoPaiement;
            $histoPaiement->setConsultation($this);
        }

        return $this;
    }

    public function removeHistoPaiement(HistoPaiement $histoPaiement): self
    {
        if ($this->histoPaiements->removeElement($histoPaiement)) {
            // set the owning side to null (unless already changed)
            if ($histoPaiement->getConsultation() === $this) {
                $histoPaiement->setConsultation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SurveillancePatient[]
     */
    public function getSurveillancePatients(): Collection
    {
        return $this->surveillancePatients;
    }





 

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }



    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(?float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getLpatient(): ?LPatient
    {
        return $this->lpatient;
    }

    public function setLpatient(?LPatient $lpatient): self
    {
        $this->lpatient = $lpatient;

        return $this;
    }

    /**
     * @return Collection|Reglement[]
     */
    public function getReglements(): Collection
    {
        return $this->reglements;
    }

    public function addReglement(Reglement $reglement): self
    {
        if (!$this->reglements->contains($reglement)) {
            $this->reglements[] = $reglement;
            $reglement->addConsultation($this);
        }

        return $this;
    }

    public function removeReglement(Reglement $reglement): self
    {
        if ($this->reglements->removeElement($reglement)) {
            $reglement->removeConsultation($this);
        }

        return $this;
    }

    /**
     * @return Collection|ConsultationHasReglemnt[]
     */
    public function getHasReglement(): Collection
    {
        return $this->hasReglement;
    }

    public function addHasReglement(ConsultationHasReglemnt $hasReglement): self
    {
        if (!$this->hasReglement->contains($hasReglement)) {
            $this->hasReglement[] = $hasReglement;
            $hasReglement->setHasReglement($this);
        }

        return $this;
    }

    public function removeHasReglement(ConsultationHasReglemnt $hasReglement): self
    {
        if ($this->hasReglement->removeElement($hasReglement)) {
            // set the owning side to null (unless already changed)
            if ($hasReglement->getHasReglement() === $this) {
                $hasReglement->setHasReglement(null);
            }
        }

        return $this;
    }

    public function getAgenda(): ?self
    {
        return $this->agenda;
    }

    public function setAgenda(?self $agenda): self
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getConsultationAgenda(): Collection
    {
        return $this->consultationAgenda;
    }

    public function addConsultationAgenda(self $consultationAgenda): self
    {
        if (!$this->consultationAgenda->contains($consultationAgenda)) {
            $this->consultationAgenda[] = $consultationAgenda;
            $consultationAgenda->setAgenda($this);
        }

        return $this;
    }

    public function removeConsultationAgenda(self $consultationAgenda): self
    {
        if ($this->consultationAgenda->removeElement($consultationAgenda)) {
            // set the owning side to null (unless already changed)
            if ($consultationAgenda->getAgenda() === $this) {
                $consultationAgenda->setAgenda(null);
            }
        }

        return $this;
    }

  




}
