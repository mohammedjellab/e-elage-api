<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CentreSoinRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CentreSoinRepository::class)
 */
class CentreSoin
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $etablissement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $activite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $etatConvention;

    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="centreSoin")
     */
    private $factures;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="centreSoin")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="centreSoin")
     */
    private $patients;

    /**
     * @ORM\OneToMany(targetEntity=Consultation::class, mappedBy="centreSoin")
     */
    private $consultations;

    /**
     * @ORM\OneToMany(targetEntity=TypeUser::class, mappedBy="centreSoin")
     */
    private $typeUsers;

    /**
     * @ORM\OneToMany(targetEntity=Specialite::class, mappedBy="centreSoin")
     */
    private $specialites;

    /**
     * @ORM\OneToMany(targetEntity=FamilleActe::class, mappedBy="centreSoin")
     */
    private $familleActes;

    /**
     * @ORM\OneToMany(targetEntity=Mutuelle::class, mappedBy="centreSoin")
     */
    private $mutuelles;



    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $ice;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $inpe;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $logo;

    /**
     * @ORM\ManyToOne(targetEntity=Ville::class, inversedBy="centreSoins")
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity=LPatient::class, mappedBy="centreSoin")
     */
    private $lPatients;

    /**
     * @ORM\OneToMany(targetEntity=LSpecialite::class, mappedBy="centreSoin")
     */
    private $lSpecialites;

    /**
     * @ORM\OneToMany(targetEntity=GPatient::class, mappedBy="centreSoin")
     */
    private $gPatients;

    /**
     * @ORM\OneToMany(targetEntity=Salle::class, mappedBy="centreSoin")
     */
    private $salles;

    /**
     * @ORM\OneToMany(targetEntity=AgendaEvenement::class, mappedBy="centreSoin")
     */
    private $agendaEvenements;

    /**
     * @ORM\OneToMany(targetEntity=DossierMedical::class, mappedBy="centreSoin")
     */
    private $dossierMedicals;

    /**
     * @ORM\OneToMany(targetEntity=ModeleConsultation::class, mappedBy="centreSoin")
     */
    private $modeleConsultations;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="centreSoin")
     */
    private $reglements;

    /**
     * @ORM\OneToMany(targetEntity=MutuelleFaite::class, mappedBy="centreSoin")
     */
    private $mutuelleFaites;

    /**
     * @ORM\OneToMany(targetEntity=AgendaConfig::class, mappedBy="centreSoin")
     */
    private $agendaConfigs;

    /**
     * @ORM\OneToMany(targetEntity=TypeConsultation::class, mappedBy="centreSoin")
     */
    private $typeConsultations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity=Annuaire::class, mappedBy="centreSoin")
     */
    private $annuaires;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoinType::class, inversedBy="centreSoins")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;



    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->active= true;
        $this->factures = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->patients = new ArrayCollection();
        $this->consultations = new ArrayCollection();
        $this->typeUsers = new ArrayCollection();
        $this->specialites = new ArrayCollection();
        $this->familleActes = new ArrayCollection();
        $this->mutuelles = new ArrayCollection();
        $this->lPatients = new ArrayCollection();
        $this->lSpecialites = new ArrayCollection();
        $this->gPatients = new ArrayCollection();
        $this->salles = new ArrayCollection();
        $this->agendaEvenements = new ArrayCollection();
        $this->dossierMedicals = new ArrayCollection();
        $this->modeleConsultations = new ArrayCollection();
        $this->reglements = new ArrayCollection();
        $this->mutuelleFaites = new ArrayCollection();
        $this->agendaConfigs = new ArrayCollection();
        $this->typeConsultations = new ArrayCollection();
        $this->annuaires = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getEtablissement(): ?string
    {
        return $this->etablissement;
    }

    public function setEtablissement(string $etablissement): self
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    public function getActivite(): ?string
    {
        return $this->activite;
    }

    public function setActivite(string $activite): self
    {
        $this->activite = $activite;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getEtatConvention(): ?string
    {
        return $this->etatConvention;
    }

    public function setEtatConvention(string $etatConvention): self
    {
        $this->etatConvention = $etatConvention;

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures(): Collection
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture): self
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setCentreSoin($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture): self
    {
        if ($this->factures->removeElement($facture)) {
            // set the owning side to null (unless already changed)
            if ($facture->getCentreSoin() === $this) {
                $facture->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCentreSoin($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCentreSoin() === $this) {
                $user->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->patients->contains($patient)) {
            $this->patients[] = $patient;
            $patient->setCentreSoin($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->patients->removeElement($patient)) {
            // set the owning side to null (unless already changed)
            if ($patient->getCentreSoin() === $this) {
                $patient->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Consultation[]
     */
    public function getConsultations(): Collection
    {
        return $this->consultations;
    }

    public function addConsultation(Consultation $consultation): self
    {
        if (!$this->consultations->contains($consultation)) {
            $this->consultations[] = $consultation;
            $consultation->setCentreSoin($this);
        }

        return $this;
    }

    public function removeConsultation(Consultation $consultation): self
    {
        if ($this->consultations->removeElement($consultation)) {
            // set the owning side to null (unless already changed)
            if ($consultation->getCentreSoin() === $this) {
                $consultation->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypeUser[]
     */
    public function getTypeUsers(): Collection
    {
        return $this->typeUsers;
    }

    public function addTypeUser(TypeUser $typeUser): self
    {
        if (!$this->typeUsers->contains($typeUser)) {
            $this->typeUsers[] = $typeUser;
            $typeUser->setCentreSoin($this);
        }

        return $this;
    }

    public function removeTypeUser(TypeUser $typeUser): self
    {
        if ($this->typeUsers->removeElement($typeUser)) {
            // set the owning side to null (unless already changed)
            if ($typeUser->getCentreSoin() === $this) {
                $typeUser->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Specialite[]
     */
    public function getSpecialites(): Collection
    {
        return $this->specialites;
    }

    public function addSpecialite(Specialite $specialite): self
    {
        if (!$this->specialites->contains($specialite)) {
            $this->specialites[] = $specialite;
            $specialite->setCentreSoin($this);
        }

        return $this;
    }

    public function removeSpecialite(Specialite $specialite): self
    {
        if ($this->specialites->removeElement($specialite)) {
            // set the owning side to null (unless already changed)
            if ($specialite->getCentreSoin() === $this) {
                $specialite->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FamilleActe[]
     */
    public function getFamilleActes(): Collection
    {
        return $this->familleActes;
    }

    public function addFamilleActe(FamilleActe $familleActe): self
    {
        if (!$this->familleActes->contains($familleActe)) {
            $this->familleActes[] = $familleActe;
            $familleActe->setCentreSoin($this);
        }

        return $this;
    }

    public function removeFamilleActe(FamilleActe $familleActe): self
    {
        if ($this->familleActes->removeElement($familleActe)) {
            // set the owning side to null (unless already changed)
            if ($familleActe->getCentreSoin() === $this) {
                $familleActe->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Mutuelle[]
     */
    public function getMutuelles(): Collection
    {
        return $this->mutuelles;
    }

    public function addMutuelle(Mutuelle $mutuelle): self
    {
        if (!$this->mutuelles->contains($mutuelle)) {
            $this->mutuelles[] = $mutuelle;
            $mutuelle->setCentreSoin($this);
        }

        return $this;
    }

    public function removeMutuelle(Mutuelle $mutuelle): self
    {
        if ($this->mutuelles->removeElement($mutuelle)) {
            // set the owning side to null (unless already changed)
            if ($mutuelle->getCentreSoin() === $this) {
                $mutuelle->setCentreSoin(null);
            }
        }

        return $this;
    }

 

    public function getIce(): ?string
    {
        return $this->ice;
    }

    public function setIce(string $ice): self
    {
        $this->ice = $ice;

        return $this;
    }

    public function getInpe(): ?string
    {
        return $this->inpe;
    }

    public function setInpe(string $inpe): self
    {
        $this->inpe = $inpe;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|LPatient[]
     */
    public function getLPatients(): Collection
    {
        return $this->lPatients;
    }

    public function addLPatient(LPatient $lPatient): self
    {
        if (!$this->lPatients->contains($lPatient)) {
            $this->lPatients[] = $lPatient;
            $lPatient->setCentreSoin($this);
        }

        return $this;
    }

    public function removeLPatient(LPatient $lPatient): self
    {
        if ($this->lPatients->removeElement($lPatient)) {
            // set the owning side to null (unless already changed)
            if ($lPatient->getCentreSoin() === $this) {
                $lPatient->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LSpecialite[]
     */
    public function getLSpecialites(): Collection
    {
        return $this->lSpecialites;
    }

    public function addLSpecialite(LSpecialite $lSpecialite): self
    {
        if (!$this->lSpecialites->contains($lSpecialite)) {
            $this->lSpecialites[] = $lSpecialite;
            $lSpecialite->setCentreSoin($this);
        }

        return $this;
    }

    public function removeLSpecialite(LSpecialite $lSpecialite): self
    {
        if ($this->lSpecialites->removeElement($lSpecialite)) {
            // set the owning side to null (unless already changed)
            if ($lSpecialite->getCentreSoin() === $this) {
                $lSpecialite->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GPatient[]
     */
    public function getGPatients(): Collection
    {
        return $this->gPatients;
    }

    public function addGPatient(GPatient $gPatient): self
    {
        if (!$this->gPatients->contains($gPatient)) {
            $this->gPatients[] = $gPatient;
            $gPatient->setCentreSoin($this);
        }

        return $this;
    }

    public function removeGPatient(GPatient $gPatient): self
    {
        if ($this->gPatients->removeElement($gPatient)) {
            // set the owning side to null (unless already changed)
            if ($gPatient->getCentreSoin() === $this) {
                $gPatient->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Salle[]
     */
    public function getSalles(): Collection
    {
        return $this->salles;
    }

    public function addSalle(Salle $salle): self
    {
        if (!$this->salles->contains($salle)) {
            $this->salles[] = $salle;
            $salle->setCentreSoin($this);
        }

        return $this;
    }

    public function removeSalle(Salle $salle): self
    {
        if ($this->salles->removeElement($salle)) {
            // set the owning side to null (unless already changed)
            if ($salle->getCentreSoin() === $this) {
                $salle->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgendaEvenement[]
     */
    public function getAgendaEvenements(): Collection
    {
        return $this->agendaEvenements;
    }

    public function addAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if (!$this->agendaEvenements->contains($agendaEvenement)) {
            $this->agendaEvenements[] = $agendaEvenement;
            $agendaEvenement->setCentreSoin($this);
        }

        return $this;
    }

    public function removeAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if ($this->agendaEvenements->removeElement($agendaEvenement)) {
            // set the owning side to null (unless already changed)
            if ($agendaEvenement->getCentreSoin() === $this) {
                $agendaEvenement->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DossierMedical[]
     */
    public function getDossierMedicals(): Collection
    {
        return $this->dossierMedicals;
    }

    public function addDossierMedical(DossierMedical $dossierMedical): self
    {
        if (!$this->dossierMedicals->contains($dossierMedical)) {
            $this->dossierMedicals[] = $dossierMedical;
            $dossierMedical->setCentreSoin($this);
        }

        return $this;
    }

    public function removeDossierMedical(DossierMedical $dossierMedical): self
    {
        if ($this->dossierMedicals->removeElement($dossierMedical)) {
            // set the owning side to null (unless already changed)
            if ($dossierMedical->getCentreSoin() === $this) {
                $dossierMedical->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ModeleConsultation[]
     */
    public function getModeleConsultations(): Collection
    {
        return $this->modeleConsultations;
    }

    public function addModeleConsultation(ModeleConsultation $modeleConsultation): self
    {
        if (!$this->modeleConsultations->contains($modeleConsultation)) {
            $this->modeleConsultations[] = $modeleConsultation;
            $modeleConsultation->setCentreSoin($this);
        }

        return $this;
    }

    public function removeModeleConsultation(ModeleConsultation $modeleConsultation): self
    {
        if ($this->modeleConsultations->removeElement($modeleConsultation)) {
            // set the owning side to null (unless already changed)
            if ($modeleConsultation->getCentreSoin() === $this) {
                $modeleConsultation->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reglement[]
     */
    public function getReglements(): Collection
    {
        return $this->reglements;
    }

    public function addReglement(Reglement $reglement): self
    {
        if (!$this->reglements->contains($reglement)) {
            $this->reglements[] = $reglement;
            $reglement->setCentreSoin($this);
        }

        return $this;
    }

    public function removeReglement(Reglement $reglement): self
    {
        if ($this->reglements->removeElement($reglement)) {
            // set the owning side to null (unless already changed)
            if ($reglement->getCentreSoin() === $this) {
                $reglement->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MutuelleFaite[]
     */
    public function getMutuelleFaites(): Collection
    {
        return $this->mutuelleFaites;
    }

    public function addMutuelleFaite(MutuelleFaite $mutuelleFaite): self
    {
        if (!$this->mutuelleFaites->contains($mutuelleFaite)) {
            $this->mutuelleFaites[] = $mutuelleFaite;
            $mutuelleFaite->setCentreSoin($this);
        }

        return $this;
    }

    public function removeMutuelleFaite(MutuelleFaite $mutuelleFaite): self
    {
        if ($this->mutuelleFaites->removeElement($mutuelleFaite)) {
            // set the owning side to null (unless already changed)
            if ($mutuelleFaite->getCentreSoin() === $this) {
                $mutuelleFaite->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgendaConfig[]
     */
    public function getAgendaConfigs(): Collection
    {
        return $this->agendaConfigs;
    }

    public function addAgendaConfig(AgendaConfig $agendaConfig): self
    {
        if (!$this->agendaConfigs->contains($agendaConfig)) {
            $this->agendaConfigs[] = $agendaConfig;
            $agendaConfig->setCentreSoin($this);
        }

        return $this;
    }

    public function removeAgendaConfig(AgendaConfig $agendaConfig): self
    {
        if ($this->agendaConfigs->removeElement($agendaConfig)) {
            // set the owning side to null (unless already changed)
            if ($agendaConfig->getCentreSoin() === $this) {
                $agendaConfig->setCentreSoin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypeConsultation[]
     */
    public function getTypeConsultations(): Collection
    {
        return $this->typeConsultations;
    }

    public function addTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if (!$this->typeConsultations->contains($typeConsultation)) {
            $this->typeConsultations[] = $typeConsultation;
            $typeConsultation->setCentreSoin($this);
        }

        return $this;
    }

    public function removeTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if ($this->typeConsultations->removeElement($typeConsultation)) {
            // set the owning side to null (unless already changed)
            if ($typeConsultation->getCentreSoin() === $this) {
                $typeConsultation->setCentreSoin(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|Annuaire[]
     */
    public function getAnnuaires(): Collection
    {
        return $this->annuaires;
    }

    public function addAnnuaire(Annuaire $annuaire): self
    {
        if (!$this->annuaires->contains($annuaire)) {
            $this->annuaires[] = $annuaire;
            $annuaire->setCentreSoin($this);
        }

        return $this;
    }

    public function removeAnnuaire(Annuaire $annuaire): self
    {
        if ($this->annuaires->removeElement($annuaire)) {
            // set the owning side to null (unless already changed)
            if ($annuaire->getCentreSoin() === $this) {
                $annuaire->setCentreSoin(null);
            }
        }

        return $this;
    }

    public function getType(): ?CentreSoinType
    {
        return $this->type;
    }

    public function setType(?CentreSoinType $type): self
    {
        $this->type = $type;

        return $this;
    }




}
