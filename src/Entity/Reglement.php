<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReglementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ReglementRepository::class)
 */
class Reglement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateRegement;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $observation;



    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $alacaisse;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=6)
     */
    private $montantRegle;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $numRecu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type;

 



    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="reglements")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $typePaiement;

    /**
     * @ORM\ManyToOne(targetEntity=Lpatient::class, inversedBy="reglements")
     */
    private $lpatient;

    /**
     * @ORM\ManyToMany(targetEntity=Consultation::class, inversedBy="reglements")
     */
    private $consultation;

    /**
     * @ORM\OneToMany(targetEntity=ConsultationHasReglemnt::class, mappedBy="hasConsultation")
     */
    private $hasConsultation;



   

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->consultation = new ArrayCollection();
        $this->hasConsultation = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDateRegement(): ?\DateTimeInterface
    {
        return $this->dateRegement;
    }

    public function setDateRegement(\DateTimeInterface $dateRegement): self
    {
        $this->dateRegement = $dateRegement;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }



    public function getAlacaisse(): ?int
    {
        return $this->alacaisse;
    }

    public function setAlacaisse(int $alacaisse): self
    {
        $this->alacaisse = $alacaisse;

        return $this;
    }

    public function getMontantRegle(): ?string
    {
        return $this->montantRegle;
    }

    public function setMontantRegle(string $montantRegle): self
    {
        $this->montantRegle = $montantRegle;

        return $this;
    }

    public function getNumRecu(): ?string
    {
        return $this->numRecu;
    }

    public function setNumRecu(string $numRecu): self
    {
        $this->numRecu = $numRecu;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }





    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    


    public function getTypePaiement(): ?string
    {
        return $this->typePaiement;
    }

    public function setTypePaiement(string $typePaiement): self
    {
        $this->typePaiement = $typePaiement;

        return $this;
    }

    public function getLpatient(): ?Lpatient
    {
        return $this->lpatient;
    }

    public function setLpatient(?Lpatient $lpatient): self
    {
        $this->lpatient = $lpatient;

        return $this;
    }

    /**
     * @return Collection|Consultation[]
     */
    public function getConsultation(): Collection
    {
        return $this->consultation;
    }

    public function addConsultation(Consultation $consultation): self
    {
        if (!$this->consultation->contains($consultation)) {
            $this->consultation[] = $consultation;
        }

        return $this;
    }

    public function removeConsultation(Consultation $consultation): self
    {
        $this->consultation->removeElement($consultation);

        return $this;
    }

    /**
     * @return Collection|ConsultationHasReglemnt[]
     */
    public function getHasConsultation(): Collection
    {
        return $this->hasConsultation;
    }

    public function addHasConsultation(ConsultationHasReglemnt $hasConsultation): self
    {
        if (!$this->hasConsultation->contains($hasConsultation)) {
            $this->hasConsultation[] = $hasConsultation;
            $hasConsultation->setHasConsultation($this);
        }

        return $this;
    }

    public function removeHasConsultation(ConsultationHasReglemnt $hasConsultation): self
    {
        if ($this->hasConsultation->removeElement($hasConsultation)) {
            // set the owning side to null (unless already changed)
            if ($hasConsultation->getHasConsultation() === $this) {
                $hasConsultation->setHasConsultation(null);
            }
        }

        return $this;
    }


    
}
