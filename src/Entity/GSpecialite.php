<?php

namespace App\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Repository\GSpecialiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=GSpecialiteRepository::class)
 * @UniqueEntity("name")
 */
class GSpecialite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * 
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=LSpecialite::class, mappedBy="gspecialite")
     */
    private $lSpecialites;

    public function __construct()
    {
        $this->lSpecialites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|LSpecialite[]
     */
    public function getLSpecialites(): Collection
    {
        return $this->lSpecialites;
    }

    public function addLSpecialite(LSpecialite $lSpecialite): self
    {
        if (!$this->lSpecialites->contains($lSpecialite)) {
            $this->lSpecialites[] = $lSpecialite;
            $lSpecialite->setGspecialite($this);
        }

        return $this;
    }

    public function removeLSpecialite(LSpecialite $lSpecialite): self
    {
        if ($this->lSpecialites->removeElement($lSpecialite)) {
            // set the owning side to null (unless already changed)
            if ($lSpecialite->getGspecialite() === $this) {
                $lSpecialite->setGspecialite(null);
            }
        }

        return $this;
    }
}
