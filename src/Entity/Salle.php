<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SalleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SalleRepository::class)
 */
class Salle
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=AgendaEvenement::class, mappedBy="salle")
     */
    private $salle;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="salles")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $active;

    public function __construct(){
        $this->id = Uuid::v4();
        $this->active = true;
        $this->salle = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|AgendaEvenement[]
     */
    public function getSalle(): Collection
    {
        return $this->salle;
    }

    public function addSalle(AgendaEvenement $salle): self
    {
        if (!$this->salle->contains($salle)) {
            $this->salle[] = $salle;
            $salle->setSalle($this);
        }

        return $this;
    }

    public function removeSalle(AgendaEvenement $salle): self
    {
        if ($this->salle->removeElement($salle)) {
            // set the owning side to null (unless already changed)
            if ($salle->getSalle() === $this) {
                $salle->setSalle(null);
            }
        }

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
