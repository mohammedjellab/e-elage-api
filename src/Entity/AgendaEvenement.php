<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AgendaEvenementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AgendaEvenementRepository::class)
 */
class AgendaEvenement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Salle::class, inversedBy="salle")
     */
    private $salle;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="agendaEvenements")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $auteur;





   

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="time")
     */
    private $heureArrivee;



    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="agendaEvenements")
     */
    private $praticien;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sansRdv;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heureSortie;

    /**
     * @ORM\ManyToOne(targetEntity=TypeConsultation::class, inversedBy="agendaEvenements")
     */
    private $typeConsultation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFin;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="agendaEvenements")
     */
    private $centreSoin;

    /**
     * @ORM\ManyToOne(targetEntity=LPatient::class, inversedBy="agendaEvenements")
     */
    private $lpatient;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity=Legende::class, inversedBy="agendaEvenements")
     */
    private $legende;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $civilite;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;





    public function __construct(){
        $this->id = Uuid::v4();
    }



    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSalle(): ?Salle
    {
        return $this->salle;
    }

    public function setSalle(?Salle $salle): self
    {
        $this->salle = $salle;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    


    

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getHeureArrivee(): ?\DateTimeInterface
    {
        return $this->heureArrivee;
    }

    public function setHeureArrivee(\DateTimeInterface $heureArrivee): self
    {
        $this->heureArrivee = $heureArrivee;

        return $this;
    }


    public function getPraticien(): ?User
    {
        return $this->praticien;
    }

    public function setPraticien(?User $praticien): self
    {
        $this->praticien = $praticien;

        return $this;
    }

    public function getSansRdv(): ?bool
    {
        return $this->sansRdv;
    }

    public function setSansRdv(?bool $sansRdv): self
    {
        $this->sansRdv = $sansRdv;

        return $this;
    }

    public function getHeureSortie(): ?\DateTimeInterface
    {
        return $this->heureSortie;
    }

    public function setHeureSortie(?\DateTimeInterface $heureSortie): self
    {
        $this->heureSortie = $heureSortie;

        return $this;
    }

    public function getTypeConsultation(): ?TypeConsultation
    {
        return $this->typeConsultation;
    }

    public function setTypeConsultation(?TypeConsultation $typeConsultation): self
    {
        $this->typeConsultation = $typeConsultation;

        return $this;
    }

    public function getDateDebut(): ?\DateTime
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTime $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getLpatient(): ?LPatient
    {
        return $this->lpatient;
    }

    public function setLpatient(?LPatient $lpatient): self
    {
        $this->lpatient = $lpatient;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getLegende(): ?Legende
    {
        return $this->legende;
    }

    public function setLegende(?Legende $legende): self
    {
        $this->legende = $legende;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }







  
}
