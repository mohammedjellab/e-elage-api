<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeCrOperatoireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TypeCrOperatoireRepository::class)
 */
class TypeCrOperatoire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="smallint")
     */
    private $sync;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $typeMime;

    /**
     * @ORM\ManyToOne(targetEntity=FamilleCro::class, inversedBy="familleCro")
     */
    private $familleCro;

    /**
     * @ORM\OneToMany(targetEntity=CrOperatoire::class, mappedBy="typeCrOperatoire")
     */
    private $date_enregistement;


    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->date_enregistement = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSync(): ?int
    {
        return $this->sync;
    }

    public function setSync(int $sync): self
    {
        $this->sync = $sync;

        return $this;
    }

    public function getTypeMime(): ?string
    {
        return $this->typeMime;
    }

    public function setTypeMime(string $typeMime): self
    {
        $this->typeMime = $typeMime;

        return $this;
    }

    public function getFamilleCro(): ?FamilleCro
    {
        return $this->familleCro;
    }

    public function setFamilleCro(?FamilleCro $familleCro): self
    {
        $this->familleCro = $familleCro;

        return $this;
    }

    /**
     * @return Collection|CrOperatoire[]
     */
    public function getDateEnregistement(): Collection
    {
        return $this->date_enregistement;
    }

    public function addDateEnregistement(CrOperatoire $dateEnregistement): self
    {
        if (!$this->date_enregistement->contains($dateEnregistement)) {
            $this->date_enregistement[] = $dateEnregistement;
            $dateEnregistement->setTypeCrOperatoire($this);
        }

        return $this;
    }

    public function removeDateEnregistement(CrOperatoire $dateEnregistement): self
    {
        if ($this->date_enregistement->removeElement($dateEnregistement)) {
            // set the owning side to null (unless already changed)
            if ($dateEnregistement->getTypeCrOperatoire() === $this) {
                $dateEnregistement->setTypeCrOperatoire(null);
            }
        }

        return $this;
    }

 



}
