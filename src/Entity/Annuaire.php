<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AnnuaireRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AnnuaireRepository::class)
 */
class Annuaire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80,  nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=80,  nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=30,  nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=20,  nullable=true)
     */
    private $gsm;

    /**
     * @ORM\Column(type="string", length=50,  nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50,  nullable=true)
     */
    private $typRep;

    /**
     * @ORM\Column(type="string", length=16,  nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="annuaires")
     */
    private $centreSoin;
    public function __construct(){
        $this->id = Uuid::v4();
        $this->active =true;
    }

    
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getGsm(): ?string
    {
        return $this->gsm;
    }

    public function setGsm(string $gsm): self
    {
        $this->gsm = $gsm;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTypRep(): ?string
    {
        return $this->typRep;
    }

    public function setTypRep(string $typRep): self
    {
        $this->typRep = $typRep;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }
}
