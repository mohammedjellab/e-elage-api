<?php

namespace App\Entity;

use App\Repository\PraticienRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=PraticienRepository::class)
 */
class Praticien
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $ice;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $inp;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $taxeProfessionnel;

    /**
     * @ORM\Column(type="boolean")
     */
    private $patente;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_correspondant;


    /**
     * @ORM\Column(type="string", length=40)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cachet;

    /**
     * @ORM\OneToMany(targetEntity=Courier::class, mappedBy="user")
     */
    private $couriers;

    /**
     * @ORM\OneToMany(targetEntity=AgendaEvenement::class, mappedBy="user")
     */
    private $agendaEvenements;



    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->couriers = new ArrayCollection();
        $this->agendaEvenements = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIce(): ?string
    {
        return $this->ice;
    }

    public function setIce(?string $ice): self
    {
        $this->ice = $ice;

        return $this;
    }

    public function getInp(): ?string
    {
        return $this->inp;
    }

    public function setInp(?string $inp): self
    {
        $this->inp = $inp;

        return $this;
    }

    public function getTaxeProfessionnel(): ?string
    {
        return $this->taxeProfessionnel;
    }

    public function setTaxeProfessionnel(?string $taxeProfessionnel): self
    {
        $this->taxeProfessionnel = $taxeProfessionnel;

        return $this;
    }

    public function getPatente(): ?bool
    {
        return $this->patente;
    }

    public function setPatente(bool $patente): self
    {
        $this->patente = $patente;

        return $this;
    }

    public function getIsCorrespondant(): ?bool
    {
        return $this->is_correspondant;
    }

    public function setIsCorrespondant(bool $is_correspondant): self
    {
        $this->is_correspondant = $is_correspondant;

        return $this;
    }



    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCachet(): ?string
    {
        return $this->cachet;
    }

    public function setCachet(string $cachet): self
    {
        $this->cachet = $cachet;

        return $this;
    }

    /**
     * @return Collection|Courier[]
     */
    public function getCouriers(): Collection
    {
        return $this->couriers;
    }

    public function addCourier(Courier $courier): self
    {
        if (!$this->couriers->contains($courier)) {
            $this->couriers[] = $courier;
            $courier->setPraticien($this);
        }

        return $this;
    }

    public function removeCourier(Courier $courier): self
    {
        if ($this->couriers->removeElement($courier)) {
            // set the owning side to null (unless already changed)
            if ($courier->getPraticien() === $this) {
                $courier->setPraticien(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgendaEvenement[]
     */
    public function getAgendaEvenements(): Collection
    {
        return $this->agendaEvenements;
    }

    public function addAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if (!$this->agendaEvenements->contains($agendaEvenement)) {
            $this->agendaEvenements[] = $agendaEvenement;
            $agendaEvenement->setPraticien($this);
        }

        return $this;
    }

    public function removeAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if ($this->agendaEvenements->removeElement($agendaEvenement)) {
            // set the owning side to null (unless already changed)
            if ($agendaEvenement->getPraticien() === $this) {
                $agendaEvenement->setPraticien(null);
            }
        }

        return $this;
    }


}
