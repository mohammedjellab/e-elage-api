<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FamilleActeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FamilleActeRepository::class)
 */
class FamilleActe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=TypeConsultation::class, mappedBy="familleActe")
     */
    private $familleActe;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="familleActes")
     */
    private $centreSoin;

    /**
     * @ORM\OneToMany(targetEntity=Acte::class, mappedBy="familleActe")
     */
    private $actes;

    

  
    public function __construct()
    {
        $this->active =true;
        $this->familleActe = new ArrayCollection();
        $this->actes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|TypeConsultation[]
     */
    public function getFamilleActe(): Collection
    {
        return $this->familleActe;
    }

    public function addFamilleActe(TypeConsultation $familleActe): self
    {
        if (!$this->familleActe->contains($familleActe)) {
            $this->familleActe[] = $familleActe;
            $familleActe->setFamilleActe($this);
        }

        return $this;
    }

    public function removeFamilleActe(TypeConsultation $familleActe): self
    {
        if ($this->familleActe->removeElement($familleActe)) {
            // set the owning side to null (unless already changed)
            if ($familleActe->getFamilleActe() === $this) {
                $familleActe->setFamilleActe(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    /**
     * @return Collection|Acte[]
     */
    public function getActes(): Collection
    {
        return $this->actes;
    }

    public function addActe(Acte $acte): self
    {
        if (!$this->actes->contains($acte)) {
            $this->actes[] = $acte;
            $acte->setFamilleActe($this);
        }

        return $this;
    }

    public function removeActe(Acte $acte): self
    {
        if ($this->actes->removeElement($acte)) {
            // set the owning side to null (unless already changed)
            if ($acte->getFamilleActe() === $this) {
                $acte->setFamilleActe(null);
            }
        }

        return $this;
    }

   

 

  

  
}
