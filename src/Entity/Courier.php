<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CourierRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CourierRepository::class)
 */
class Courier
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCourier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $objectCourier;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $destinataire;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $source;

    /**
     * @ORM\ManyToOne(targetEntity=TypeCourrier::class, inversedBy="couriers")
     */
    private $typeCourier;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $courrierFtx;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="couriers")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class, inversedBy="couriers")
     */
    private $consultation;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="couriers")
     */
    private $user;
    public function __construct()
    {
        $this->id = Uuid::v4();
    }
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDateCourier(): ?\DateTimeInterface
    {
        return $this->dateCourier;
    }

    public function setDateCourier(\DateTimeInterface $dateCourier): self
    {
        $this->dateCourier = $dateCourier;

        return $this;
    }

    public function getObjectCourier(): ?string
    {
        return $this->objectCourier;
    }

    public function setObjectCourier(string $objectCourier): self
    {
        $this->objectCourier = $objectCourier;

        return $this;
    }

    public function getDestinataire(): ?string
    {
        return $this->destinataire;
    }

    public function setDestinataire(string $destinataire): self
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getTypeCourier(): ?TypeCourrier
    {
        return $this->typeCourier;
    }

    public function setTypeCourier(?TypeCourrier $typeCourier): self
    {
        $this->typeCourier = $typeCourier;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getCourrierFtx(): ?string
    {
        return $this->courrierFtx;
    }

    public function setCourrierFtx(string $courrierFtx): self
    {
        $this->courrierFtx = $courrierFtx;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
