<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CertificatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CertificatRepository::class)
 */
class Certificat
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateCertificat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $objectifCertif;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $certificatFtx;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $typeMime;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="certificat")
     */
    private $Patient;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="certificat")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=TypeCertificat::class, inversedBy="certificats")
     */
    private $typeCertificat;

    /**
     * @ORM\OneToMany(targetEntity=LPatient::class, mappedBy="certificat")
     */
    private $lPatients;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->Patient = new ArrayCollection();
        $this->lPatients = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDateCertificat(): ?\DateTimeInterface
    {
        return $this->dateCertificat;
    }

    public function setDateCertificat(?\DateTimeInterface $dateCertificat): self
    {
        $this->dateCertificat = $dateCertificat;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getObjectifCertif(): ?string
    {
        return $this->objectifCertif;
    }

    public function setObjectifCertif(string $objectifCertif): self
    {
        $this->objectifCertif = $objectifCertif;

        return $this;
    }

    public function getCertificatFtx(): ?string
    {
        return $this->certificatFtx;
    }

    public function setCertificatFtx(string $certificatFtx): self
    {
        $this->certificatFtx = $certificatFtx;

        return $this;
    }

    public function getTypeMime(): ?string
    {
        return $this->typeMime;
    }

    public function setTypeMime(string $typeMime): self
    {
        $this->typeMime = $typeMime;

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPatient(): Collection
    {
        return $this->Patient;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->Patient->contains($patient)) {
            $this->Patient[] = $patient;
            $patient->setCertificat($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->Patient->removeElement($patient)) {
            // set the owning side to null (unless already changed)
            if ($patient->getCertificat() === $this) {
                $patient->setCertificat(null);
            }
        }

        return $this;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getTypeCertificat(): ?TypeCertificat
    {
        return $this->typeCertificat;
    }

    public function setTypeCertificat(?TypeCertificat $typeCertificat): self
    {
        $this->typeCertificat = $typeCertificat;

        return $this;
    }

    /**
     * @return Collection|LPatient[]
     */
    public function getLPatients(): Collection
    {
        return $this->lPatients;
    }

    public function addLPatient(LPatient $lPatient): self
    {
        if (!$this->lPatients->contains($lPatient)) {
            $this->lPatients[] = $lPatient;
            $lPatient->setCertificat($this);
        }

        return $this;
    }

    public function removeLPatient(LPatient $lPatient): self
    {
        if ($this->lPatients->removeElement($lPatient)) {
            // set the owning side to null (unless already changed)
            if ($lPatient->getCertificat() === $this) {
                $lPatient->setCertificat(null);
            }
        }

        return $this;
    }
}
