<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ActeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ActeRepository::class)
 */
class Acte
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=30 ,nullable=true)
     */
    private $compteComptable;

    /**
     * @ORM\Column(type="string", length=100 ,nullable=true)
     */
    private $nameDisplay;

    /**
     * @ORM\OneToMany(targetEntity=LigneFacture::class, mappedBy="acte")
     */
    private $ligneFactures;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $couleur;

    /**
     * @ORM\Column(type="integer")
     */
    private $duree;

    /**
     * @ORM\ManyToOne(targetEntity=FamilleActe::class, inversedBy="actes")
     */
    private $familleActe;



    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->ligneFactures = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setId(int $id){
        $this->id = $id;
        return $this;
    }
    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getCompteComptable(): ?string
    {
        return $this->compteComptable;
    }

    public function setCompteComptable(string $compteComptable): self
    {
        $this->compteComptable = $compteComptable;

        return $this;
    }

    public function getNameDisplay(): ?string
    {
        return $this->nameDisplay;
    }

    public function setNameDisplay(string $nameDisplay): self
    {
        $this->nameDisplay = $nameDisplay;

        return $this;
    }

    /**
     * @return Collection|LigneFacture[]
     */
    public function getLigneFactures(): Collection
    {
        return $this->ligneFactures;
    }

    public function addLigneFacture(LigneFacture $ligneFacture): self
    {
        if (!$this->ligneFactures->contains($ligneFacture)) {
            $this->ligneFactures[] = $ligneFacture;
            $ligneFacture->setActe($this);
        }

        return $this;
    }

    public function removeLigneFacture(LigneFacture $ligneFacture): self
    {
        if ($this->ligneFactures->removeElement($ligneFacture)) {
            // set the owning side to null (unless already changed)
            if ($ligneFacture->getActe() === $this) {
                $ligneFacture->setActe(null);
            }
        }

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getFamilleActe(): ?FamilleActe
    {
        return $this->familleActe;
    }

    public function setFamilleActe(?FamilleActe $familleActe): self
    {
        $this->familleActe = $familleActe;

        return $this;
    }



}
