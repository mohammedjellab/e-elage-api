<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LegendeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=LegendeRepository::class)
 */
class Legende
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=AgendaEvenement::class, mappedBy="legende")
     */
    private $agendaEvenements;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $name;

    public function __construct()
    {
        $this->agendaEvenements = new ArrayCollection();
    }

  

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|AgendaEvenement[]
     */
    public function getAgendaEvenements(): Collection
    {
        return $this->agendaEvenements;
    }

    public function addAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if (!$this->agendaEvenements->contains($agendaEvenement)) {
            $this->agendaEvenements[] = $agendaEvenement;
            $agendaEvenement->setLegende($this);
        }

        return $this;
    }

    public function removeAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if ($this->agendaEvenements->removeElement($agendaEvenement)) {
            // set the owning side to null (unless already changed)
            if ($agendaEvenement->getLegende() === $this) {
                $agendaEvenement->setLegende(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

}
