<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 */
class Patient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=30,nullable=true)
     */
    private $cin;

    /**
     * @ORM\Column(type="string", length=30,nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\ManyToOne(targetEntity=Ville::class, inversedBy="ville")
     */
    private $ville;

    /**
     * @ORM\ManyToOne(targetEntity=Pays::class, inversedBy="pays")
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $sexe;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $situation_famille;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $civilite;





    /**
     * @ORM\ManyToOne(targetEntity=Comorbidite::class, inversedBy="comorbidite")
     */
    private $comorbidite;

    /**
     * @ORM\OneToMany(targetEntity=MutuelleFaite::class, mappedBy="patient")
     */
    private $mutuelleFaites;

    /**
     * @ORM\OneToMany(targetEntity=Certificat::class, mappedBy="patient")
     */
    private $certificat;





    
    /**
     * @ORM\ManyToMany(targetEntity=Ordonnance::class, mappedBy="patient")
     */
    private $ordonnances;

    /**
     * @ORM\OneToMany(targetEntity=Devis::class, mappedBy="patient")
     */
    private $devis;

    /**
     * @ORM\OneToMany(targetEntity=DocPatient::class, mappedBy="patient")
     */
    private $docPatients;

    /**
     * @ORM\OneToMany(targetEntity=Courier::class, mappedBy="patient")
     */
    private $couriers;



    /**
     * @ORM\OneToMany(targetEntity=CrOperatoire::class, mappedBy="patient")
     */
    private $crOperatoires;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $allergieConnue;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $adressePar;

    /**
     * @ORM\Column(type="string", length=150,nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=50,nullable=true)
     */
    private $profession;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $fumeur;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $nbEnfant;

  

    /**
     * @ORM\Column(type="string", length=50,nullable=true)
     */
    private $matricule;





    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="patient")
     */
    private $factures;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="patients")
     */
    private $centreSoin;


    /**
     * @ORM\ManyToOne(targetEntity=Mutuelle::class, inversedBy="patients")
     */
    private $mutuelle;

    /**
     * @ORM\Column(type="json")
     */
    private $recommendation = [];



    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->ordonnances = new ArrayCollection();
        $this->mutuelleFaites = new ArrayCollection();
        $this->certificat = new ArrayCollection();
        $this->devis = new ArrayCollection();
        $this->docPatients = new ArrayCollection();
        $this->couriers = new ArrayCollection();
        $this->crOperatoires = new ArrayCollection();
        $this->factures = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCin(): ?string
    {
        return $this->cin;
    }

    public function setCin(string $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getSexe(): ?bool
    {
        return $this->sexe;
    }

    public function setSexe(?bool $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getSituationFamille(): ?string
    {
        return $this->situation_famille;
    }

    public function setSituationFamille(?string $situation_famille): self
    {
        $this->situation_famille = $situation_famille;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getComorbidite(): ?Comorbidite
    {
        return $this->comorbidite;
    }

    public function setComorbidite(?Comorbidite $comorbidite): self
    {
        $this->comorbidite = $comorbidite;

        return $this;
    }

    public function getMutuelle(): ?Mutuelle
    {
        return $this->mutuelle;
    }

    public function setMutuelle(?Mutuelle $mutuelle): self
    {
        $this->mutuelle = $mutuelle;

        return $this;
    }

    public function getRecommendation(): ?array
    {
        return $this->recommendation;
    }

    public function setRecommendation(array $recommendation): self
    {
        $this->recommendation = $recommendation;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }



    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    
    public function getFumeur(): ?bool
    {
        return $this->fumeur;
    }

    public function setFumeur(bool $fumeur): self
    {
        $this->fumeur = $fumeur;

        return $this;
    }

    public function getNbEnfant(): ?int
    {
        return $this->nbEnfant;
    }

    public function setNbEnfant(int $nbEnfant): self
    {
        $this->nbEnfant = $nbEnfant;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setProfession(string $profession): self
    {
        $this->profession = $profession;

        return $this;
    }
    public function getAdressePar(): ?string
    {
        return $this->adressePar;
    }

    public function setAdressePar(string $adressePar): self
    {
        $this->adressePar = $adressePar;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAllergieConnue(): ?string
    {
        return $this->allergieConnue;
    }

    public function setAllergieConnue(string $allergieConnue): self
    {
        $this->allergieConnue = $allergieConnue;

        return $this;
    }




 
}
