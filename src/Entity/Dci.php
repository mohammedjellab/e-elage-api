<?php

namespace App\Entity;

use App\Repository\DciRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Serializer\Annotation\MaxDepth;


/**
 * @ORM\Entity(repositoryClass=DciRepository::class)
 */
class Dci
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    //private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @MaxDepth(4)
     * @Groups("medicaments")
     * @ORM\ManyToMany(targetEntity=Medicament::class, inversedBy="dcis")
     *
     */
    private $dci;

    public function __construct()
    {
        //$this->uuid = Uuid::v4();
        $this->active = true;
        $this->dci = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    /*public function getUuid(): ?string
    {
        return $this->uuid;
    }*/

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|Medicament[]
     */
    public function getDci(): Collection
    {
        return $this->dci;
    }

    public function addDci(Medicament $dci): self
    {
        if (!$this->dci->contains($dci)) {
            $this->dci[] = $dci;
        }

        return $this;
    }

    public function removeDci(Medicament $dci): self
    {
        $this->dci->removeElement($dci);

        return $this;
    }
}
