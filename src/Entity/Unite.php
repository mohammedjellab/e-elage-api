<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UniteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=UniteRepository::class)
 * @UniqueEntity("libelle")
 */
class Unite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=Medicament::class, mappedBy="unite")
     */
    private $unite;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $abreviation;

    /**
     * @ORM\OneToMany(targetEntity=ParamSurveillance::class, mappedBy="unite")
     */
    private $paramSurveillances;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    public function __construct(){
        $this->id = Uuid::v4();
        $this->unite = new ArrayCollection();
        $this->paramSurveillances = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Medicament[]
     */
    public function getUnite(): Collection
    {
        return $this->unite;
    }

    public function addUnite(Medicament $unite): self
    {
        if (!$this->unite->contains($unite)) {
            $this->unite[] = $unite;
            $unite->setUnite($this);
        }

        return $this;
    }

    public function removeUnite(Medicament $unite): self
    {
        if ($this->unite->removeElement($unite)) {
            // set the owning side to null (unless already changed)
            if ($unite->getUnite() === $this) {
                $unite->setUnite(null);
            }
        }

        return $this;
    }

    public function getAbreviation(): ?string
    {
        return $this->abreviation;
    }

    public function setAbreviation(string $abreviation): self
    {
        $this->abreviation = $abreviation;

        return $this;
    }

    /**
     * @return Collection|ParamSurveillance[]
     */
    public function getParamSurveillances(): Collection
    {
        return $this->paramSurveillances;
    }

    public function addParamSurveillance(ParamSurveillance $paramSurveillance): self
    {
        if (!$this->paramSurveillances->contains($paramSurveillance)) {
            $this->paramSurveillances[] = $paramSurveillance;
            $paramSurveillance->setUnite($this);
        }

        return $this;
    }

    public function removeParamSurveillance(ParamSurveillance $paramSurveillance): self
    {
        if ($this->paramSurveillances->removeElement($paramSurveillance)) {
            // set the owning side to null (unless already changed)
            if ($paramSurveillance->getUnite() === $this) {
                $paramSurveillance->setUnite(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
