<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\HistoPaiementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=HistoPaiementRepository::class)
 */
class HistoPaiement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateConsult;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class, inversedBy="histoPaiements")
     */
    private $consultation;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $montantConsult;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $dejaRegle;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $observation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateConsult(): ?\DateTimeInterface
    {
        return $this->dateConsult;
    }

    public function setDateConsult(?\DateTimeInterface $dateConsult): self
    {
        $this->dateConsult = $dateConsult;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getMontantConsult(): ?string
    {
        return $this->montantConsult;
    }

    public function setMontantConsult(string $montantConsult): self
    {
        $this->montantConsult = $montantConsult;

        return $this;
    }

    public function getDejaRegle(): ?string
    {
        return $this->dejaRegle;
    }

    public function setDejaRegle(string $dejaRegle): self
    {
        $this->dejaRegle = $dejaRegle;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }
}
