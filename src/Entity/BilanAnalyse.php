<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BilanAnalyseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=BilanAnalyseRepository::class)
 */
class BilanAnalyse
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity=Bilan::class, inversedBy="bilan")
     */
    private $bilan;

    /**
     * @ORM\ManyToOne(targetEntity=AnalyseLaboratoire::class, inversedBy="bilanAnalyses")
     */
    private $analyseLaboratoire;



    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getBilan(): ?Bilan
    {
        return $this->bilan;
    }

    public function setBilan(?Bilan $bilan): self
    {
        $this->bilan = $bilan;

        return $this;
    }

    public function getAnalyseLaboratoire(): ?AnalyseLaboratoire
    {
        return $this->analyseLaboratoire;
    }

    public function setAnalyseLaboratoire(?AnalyseLaboratoire $analyseLaboratoire): self
    {
        $this->analyseLaboratoire = $analyseLaboratoire;

        return $this;
    }



 

 
}
