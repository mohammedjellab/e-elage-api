<?php

namespace App\Entity;

use App\Repository\TypeCourrierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=TypeCourrierRepository::class)
 */
class TypeCourrier
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @ORM\Column(type="smallint")
     */
    private $sync;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $typeMime;

    /**
     * @ORM\OneToMany(targetEntity=Courier::class, mappedBy="typeCourier")
     */
    private $couriers;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->couriers = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getSync(): ?int
    {
        return $this->sync;
    }

    public function setSync(int $sync): self
    {
        $this->sync = $sync;

        return $this;
    }

    public function getTypeMime(): ?string
    {
        return $this->typeMime;
    }

    public function setTypeMime(string $typeMime): self
    {
        $this->typeMime = $typeMime;

        return $this;
    }

    /**
     * @return Collection|Courier[]
     */
    public function getCouriers(): Collection
    {
        return $this->couriers;
    }

    public function addCourier(Courier $courier): self
    {
        if (!$this->couriers->contains($courier)) {
            $this->couriers[] = $courier;
            $courier->setTypeCourier($this);
        }

        return $this;
    }

    public function removeCourier(Courier $courier): self
    {
        if ($this->couriers->removeElement($courier)) {
            // set the owning side to null (unless already changed)
            if ($courier->getTypeCourier() === $this) {
                $courier->setTypeCourier(null);
            }
        }

        return $this;
    }
}
