<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MedicamentFamilleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=MedicamentFamilleRepository::class)
 */
class MedicamentFamille
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Medicament::class, mappedBy="medicamentFamille")
     */
    private $medicamentFamille;

   

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->medicamentFamille = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return Collection|Medicament[]
     */
    public function getMedicamentFamille(): Collection
    {
        return $this->medicamentFamille;
    }

    public function addMedicamentFamille(Medicament $medicamentFamille): self
    {
        if (!$this->medicamentFamille->contains($medicamentFamille)) {
            $this->medicamentFamille[] = $medicamentFamille;
            $medicamentFamille->setMedicamentFamille($this);
        }

        return $this;
    }

    public function removeMedicamentFamille(Medicament $medicamentFamille): self
    {
        if ($this->medicamentFamille->removeElement($medicamentFamille)) {
            // set the owning side to null (unless already changed)
            if ($medicamentFamille->getMedicamentFamille() === $this) {
                $medicamentFamille->setMedicamentFamille(null);
            }
        }

        return $this;
    }

    

   
   
}
