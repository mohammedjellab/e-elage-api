<?php

namespace App\Entity;

use App\Repository\LPatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=LPatientRepository::class)
 */
class LPatient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity=Certificat::class, inversedBy="lPatients")
     */
    private $certificat;

    /**
     * @ORM\Column(type="string", length=255 ,  nullable=true)
     */
    private $ordonnance;

    /**
     * @ORM\ManyToOne(targetEntity=GPatient::class, inversedBy="lPatients")
     */
    private $gpatient;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $adressePar;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="lPatients")
     */
    private $centreSoin;

    /**
     * @ORM\OneToMany(targetEntity=AgendaEvenement::class, mappedBy="lpatient")
     */
    private $agendaEvenements;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="lPatients")
     */
    private $praticien;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="lpatient")
     */
    private $reglements;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="lpatient")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity=SurveillancePatient::class, mappedBy="lpatient")
     */
    private $surveillancePatients;

    /**
     * @ORM\ManyToMany(targetEntity=Antecedent::class, mappedBy="Lpatient")
     */
    private $antecedents;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $diagnostic;

    /**
     * @ORM\OneToMany(targetEntity=Consultation::class, mappedBy="lpatient")
     */
    private $consultations;

    /**
     * @ORM\OneToMany(targetEntity=MutuelleFaite::class, mappedBy="lpatient")
     */
    private $mutuelleFaites;





    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->agendaEvenements = new ArrayCollection();
        $this->reglements = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->surveillancePatients = new ArrayCollection();
        $this->antecedents = new ArrayCollection();
        $this->consultations = new ArrayCollection();
        $this->mutuelleFaites = new ArrayCollection();
    }

   



    public function getId(): ?int
    {
        return $this->id;
    }



    public function getCertificat(): ?Certificat
    {
        return $this->certificat;
    }

    public function setCertificat(?Certificat $certificat): self
    {
        $this->certificat = $certificat;

        return $this;
    }





    






    public function getOrdonnance(): ?string
    {
        return $this->ordonnance;
    }

    public function setOrdonnance(string $ordonnance): self
    {
        $this->ordonnance = $ordonnance;

        return $this;
    }

    







    public function getGpatient(): ?GPatient
    {
        return $this->gpatient;
    }

    public function setGpatient(?GPatient $gpatient): self
    {
        $this->gpatient = $gpatient;

        return $this;
    }

    


    public function getAdressePar(): ?string
    {
        return $this->adressePar;
    }

    public function setAdressePar(string $adressePar): self
    {
        $this->adressePar = $adressePar;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    /**
     * @return Collection|AgendaEvenement[]
     */
    public function getAgendaEvenements(): Collection
    {
        return $this->agendaEvenements;
    }

    public function addAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if (!$this->agendaEvenements->contains($agendaEvenement)) {
            $this->agendaEvenements[] = $agendaEvenement;
            $agendaEvenement->setLpatient($this);
        }

        return $this;
    }

    public function removeAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if ($this->agendaEvenements->removeElement($agendaEvenement)) {
            // set the owning side to null (unless already changed)
            if ($agendaEvenement->getLpatient() === $this) {
                $agendaEvenement->setLpatient(null);
            }
        }

        return $this;
    }

    public function getPraticien(): ?User
    {
        return $this->praticien;
    }

    public function setPraticien(?User $praticien): self
    {
        $this->praticien = $praticien;

        return $this;
    }

    

    /**
     * @return Collection|Reglement[]
     */
    public function getReglements(): Collection
    {
        return $this->reglements;
    }

    public function addReglement(Reglement $reglement): self
    {
        if (!$this->reglements->contains($reglement)) {
            $this->reglements[] = $reglement;
            $reglement->setLpatient($this);
        }

        return $this;
    }

    public function removeReglement(Reglement $reglement): self
    {
        if ($this->reglements->removeElement($reglement)) {
            // set the owning side to null (unless already changed)
            if ($reglement->getLpatient() === $this) {
                $reglement->setLpatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setLpatient($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getLpatient() === $this) {
                $document->setLpatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SurveillancePatient[]
     */
    public function getSurveillancePatients(): Collection
    {
        return $this->surveillancePatients;
    }

    public function addSurveillancePatient(SurveillancePatient $surveillancePatient): self
    {
        if (!$this->surveillancePatients->contains($surveillancePatient)) {
            $this->surveillancePatients[] = $surveillancePatient;
            $surveillancePatient->setLpatient($this);
        }

        return $this;
    }

    public function removeSurveillancePatient(SurveillancePatient $surveillancePatient): self
    {
        if ($this->surveillancePatients->removeElement($surveillancePatient)) {
            // set the owning side to null (unless already changed)
            if ($surveillancePatient->getLpatient() === $this) {
                $surveillancePatient->setLpatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Antecedent[]
     */
    public function getAntecedents(): Collection
    {
        return $this->antecedents;
    }

    public function addAntecedent(Antecedent $antecedent): self
    {
        if (!$this->antecedents->contains($antecedent)) {
            $this->antecedents[] = $antecedent;
            $antecedent->addLpatient($this);
        }

        return $this;
    }

    public function removeAntecedent(Antecedent $antecedent): self
    {
        if ($this->antecedents->removeElement($antecedent)) {
            $antecedent->removeLpatient($this);
        }

        return $this;
    }

    public function getDiagnostic(): ?string
    {
        return $this->diagnostic;
    }

    public function setDiagnostic(?string $diagnostic): self
    {
        $this->diagnostic = $diagnostic;

        return $this;
    }

    /**
     * @return Collection|Consultation[]
     */
    public function getConsultations(): Collection
    {
        return $this->consultations;
    }

    public function addConsultation(Consultation $consultation): self
    {
        if (!$this->consultations->contains($consultation)) {
            $this->consultations[] = $consultation;
            $consultation->setLpatient($this);
        }

        return $this;
    }

    public function removeConsultation(Consultation $consultation): self
    {
        if ($this->consultations->removeElement($consultation)) {
            // set the owning side to null (unless already changed)
            if ($consultation->getLpatient() === $this) {
                $consultation->setLpatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MutuelleFaite[]
     */
    public function getMutuelleFaites(): Collection
    {
        return $this->mutuelleFaites;
    }

    public function addMutuelleFaite(MutuelleFaite $mutuelleFaite): self
    {
        if (!$this->mutuelleFaites->contains($mutuelleFaite)) {
            $this->mutuelleFaites[] = $mutuelleFaite;
            $mutuelleFaite->setLpatient($this);
        }

        return $this;
    }

    public function removeMutuelleFaite(MutuelleFaite $mutuelleFaite): self
    {
        if ($this->mutuelleFaites->removeElement($mutuelleFaite)) {
            // set the owning side to null (unless already changed)
            if ($mutuelleFaite->getLpatient() === $this) {
                $mutuelleFaite->setLpatient(null);
            }
        }

        return $this;
    }

    




}
