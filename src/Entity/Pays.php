<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PaysRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PaysRepository::class)
 */
class Pays
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="pays")
     */
    private $pays;

    /**
     * @ORM\OneToMany(targetEntity=GPatient::class, mappedBy="pays")
     */
    private $gPatients;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->pays = new ArrayCollection();
        $this->gPatients = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPays(): Collection
    {
        return $this->pays;
    }

    public function addPay(Patient $pay): self
    {
        if (!$this->pays->contains($pay)) {
            $this->pays[] = $pay;
            $pay->setPays($this);
        }

        return $this;
    }

    public function removePay(Patient $pay): self
    {
        if ($this->pays->removeElement($pay)) {
            // set the owning side to null (unless already changed)
            if ($pay->getPays() === $this) {
                $pay->setPays(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GPatient[]
     */
    public function getGPatients(): Collection
    {
        return $this->gPatients;
    }

    public function addGPatient(GPatient $gPatient): self
    {
        if (!$this->gPatients->contains($gPatient)) {
            $this->gPatients[] = $gPatient;
            $gPatient->setPays($this);
        }

        return $this;
    }

    public function removeGPatient(GPatient $gPatient): self
    {
        if ($this->gPatients->removeElement($gPatient)) {
            // set the owning side to null (unless already changed)
            if ($gPatient->getPays() === $this) {
                $gPatient->setPays(null);
            }
        }

        return $this;
    }
}
