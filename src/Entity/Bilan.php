<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BilanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=BilanRepository::class)
 */
class Bilan
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=BilanAnalyse::class, mappedBy="bilan")
     */
    private $bilan;



    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->bilan = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|BilanAnalyse[]
     */
    public function getBilan(): Collection
    {
        return $this->bilan;
    }

    public function addBilan(BilanAnalyse $bilan): self
    {
        if (!$this->bilan->contains($bilan)) {
            $this->bilan[] = $bilan;
            $bilan->setBilan($this);
        }

        return $this;
    }

    public function removeBilan(BilanAnalyse $bilan): self
    {
        if ($this->bilan->removeElement($bilan)) {
            // set the owning side to null (unless already changed)
            if ($bilan->getBilan() === $this) {
                $bilan->setBilan(null);
            }
        }

        return $this;
    }





}
