<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\InteractionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=InteractionRepository::class)
 */
class Interaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idDciPrinc;

    /**
     * @ORM\Column(type="integer")
     */
    private $idDciSecond;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $typeDesc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdDciPrinc(): ?int
    {
        return $this->idDciPrinc;
    }

    public function setIdDciPrinc(int $idDciPrinc): self
    {
        $this->idDciPrinc = $idDciPrinc;

        return $this;
    }

    public function getIdDciSecond(): ?int
    {
        return $this->idDciSecond;
    }

    public function setIdDciSecond(int $idDciSecond): self
    {
        $this->idDciSecond = $idDciSecond;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTypeDesc(): ?string
    {
        return $this->typeDesc;
    }

    public function setTypeDesc(string $typeDesc): self
    {
        $this->typeDesc = $typeDesc;

        return $this;
    }
}
