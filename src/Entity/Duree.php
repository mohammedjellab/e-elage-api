<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DureeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=DureeRepository::class)
 */
class Duree
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=TypeConsultation::class, mappedBy="duree")
     */
    private $typeConsultations;

    public function __construct()
    {
        $this->typeConsultations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?int
    {
        return $this->name;
    }

    public function setName(int $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|TypeConsultation[]
     */
    public function getTypeConsultations(): Collection
    {
        return $this->typeConsultations;
    }

    public function addTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if (!$this->typeConsultations->contains($typeConsultation)) {
            $this->typeConsultations[] = $typeConsultation;
            $typeConsultation->setDuree($this);
        }

        return $this;
    }

    public function removeTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if ($this->typeConsultations->removeElement($typeConsultation)) {
            // set the owning side to null (unless already changed)
            if ($typeConsultation->getDuree() === $this) {
                $typeConsultation->setDuree(null);
            }
        }

        return $this;
    }
}
