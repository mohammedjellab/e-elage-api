<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OrdonnanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=OrdonnanceRepository::class)
 */
class Ordonnance
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;



    /**
     * @ORM\Column(type="date")
     */
    private $dateOrdonnance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nomDoc;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="ordonnance")
     */
    private $patient;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->patient = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }


    public function getDateOrdonnance(): ?\DateTimeInterface
    {
        return $this->dateOrdonnance;
    }

    public function setDateOrdonnance(\DateTimeInterface $dateOrdonnance): self
    {
        $this->dateOrdonnance = $dateOrdonnance;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getNomDoc(): ?string
    {
        return $this->nomDoc;
    }

    public function setNomDoc(string $nomDoc): self
    {
        $this->nomDoc = $nomDoc;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }
}
