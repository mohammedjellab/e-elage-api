<?php

namespace App\Entity;

use App\Repository\DevisRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DevisRepository::class)
 */
class Devis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDevis;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $numDevis;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $observation;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=6)
     */
    private $acompte;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $etatDevis;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="devis")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=ModeleFacture::class, inversedBy="devis")
     */
    private $modeleDevis;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $typeMime;

    /**
     * @ORM\OneToMany(targetEntity=LigneDevis::class, mappedBy="devis")
     */
    private $ligneDevis;

    public function __construct()
    {
        $this->ligneDevis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDevis(): ?\DateTimeInterface
    {
        return $this->dateDevis;
    }

    public function setDateDevis(\DateTimeInterface $dateDevis): self
    {
        $this->dateDevis = $dateDevis;

        return $this;
    }

    public function getNumDevis(): ?string
    {
        return $this->numDevis;
    }

    public function setNumDevis(string $numDevis): self
    {
        $this->numDevis = $numDevis;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

    public function getAcompte(): ?string
    {
        return $this->acompte;
    }

    public function setAcompte(string $acompte): self
    {
        $this->acompte = $acompte;

        return $this;
    }

    public function getEtatDevis(): ?string
    {
        return $this->etatDevis;
    }

    public function setEtatDevis(string $etatDevis): self
    {
        $this->etatDevis = $etatDevis;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getModeleDevis(): ?ModeleFacture
    {
        return $this->modeleDevis;
    }

    public function setModeleDevis(?ModeleFacture $modeleDevis): self
    {
        $this->modeleDevis = $modeleDevis;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getTypeMime(): ?string
    {
        return $this->typeMime;
    }

    public function setTypeMime(string $typeMime): self
    {
        $this->typeMime = $typeMime;

        return $this;
    }

    /**
     * @return Collection|LigneDevis[]
     */
    public function getLigneDevis(): Collection
    {
        return $this->ligneDevis;
    }

    public function addLigneDevi(LigneDevis $ligneDevi): self
    {
        if (!$this->ligneDevis->contains($ligneDevi)) {
            $this->ligneDevis[] = $ligneDevi;
            $ligneDevi->setDevis($this);
        }

        return $this;
    }

    public function removeLigneDevi(LigneDevis $ligneDevi): self
    {
        if ($this->ligneDevis->removeElement($ligneDevi)) {
            // set the owning side to null (unless already changed)
            if ($ligneDevi->getDevis() === $this) {
                $ligneDevi->setDevis(null);
            }
        }

        return $this;
    }
}
