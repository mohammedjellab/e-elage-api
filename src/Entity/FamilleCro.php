<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FamilleCroRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FamilleCroRepository::class)
 */
class FamilleCro
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="smallint")
     */
    private $sync;

    /**
     * @ORM\OneToMany(targetEntity=TypeCrOperatoire::class, mappedBy="familleCro")
     */
    private $familleCro;

    public function __construct()
    {
        $this->familleCro = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSync(): ?int
    {
        return $this->sync;
    }

    public function setSync(int $sync): self
    {
        $this->sync = $sync;

        return $this;
    }

    /**
     * @return Collection|TypeCrOperatoire[]
     */
    public function getFamilleCro(): Collection
    {
        return $this->familleCro;
    }

    public function addFamilleCro(TypeCrOperatoire $familleCro): self
    {
        if (!$this->familleCro->contains($familleCro)) {
            $this->familleCro[] = $familleCro;
            $familleCro->setFamilleCro($this);
        }

        return $this;
    }

    public function removeFamilleCro(TypeCrOperatoire $familleCro): self
    {
        if ($this->familleCro->removeElement($familleCro)) {
            // set the owning side to null (unless already changed)
            if ($familleCro->getFamilleCro() === $this) {
                $familleCro->setFamilleCro(null);
            }
        }

        return $this;
    }
}
