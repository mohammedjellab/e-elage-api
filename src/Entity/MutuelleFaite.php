<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MutuelleFaiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=MutuelleFaiteRepository::class)
 */
class MutuelleFaite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $faite;

    /**
     * @ORM\ManyToOne(targetEntity=Mutuelle::class, inversedBy="mutuelle")
     */
    private $mutuelle;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $observation;

    /**
     * @ORM\Column(type="date")
     */
    private $date;



    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="mutuelleFaites")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $valider;

    /**
     * @ORM\ManyToOne(targetEntity=LPatient::class, inversedBy="mutuelleFaites")
     */
    private $lpatient;
  

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->mutuelle = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function getFaite(): ?bool
    {
        return $this->faite;
    }

    public function setFaite(?bool $faite): self
    {
        $this->faite = $faite;

        return $this;
    }

    public function getMutuelle(): ?Mutuelle
    {
        return $this->mutuelle;
    }

    public function setMutuelle(?Mutuelle $mutuelle): self
    {
        $this->mutuelle = $mutuelle;

        return $this;
    }


    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }





    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getValider(): ?bool
    {
        return $this->valider;
    }

    public function setValider(?bool $valider): self
    {
        $this->valider = $valider;

        return $this;
    }

    public function getLpatient(): ?LPatient
    {
        return $this->lpatient;
    }

    public function setLpatient(?LPatient $lpatient): self
    {
        $this->lpatient = $lpatient;

        return $this;
    }




 

    


}
