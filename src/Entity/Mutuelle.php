<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MutuelleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=MutuelleRepository::class)
 * @UniqueEntity("libelle")
 */
class Mutuelle
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=MutuelleFaite::class, mappedBy="mutuelle")
     */
    private $mutuelle;

    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="mutuelle")
     */
    private $factures;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="mutuelles")
     */
    private $centreSoin;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="mutuelle")
     */
    private $patients;

    /**
     * @ORM\OneToMany(targetEntity=GPatient::class, mappedBy="mutuelle")
     */
    private $gPatients;

    

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->active = true;
        $this->mutuelle = new ArrayCollection();
        $this->factures = new ArrayCollection();
        $this->patients = new ArrayCollection();
        $this->gPatients = new ArrayCollection();
    }


    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|MutuelleFaite[]
     */
    public function getMutuelle(): Collection
    {
        return $this->mutuelle;
    }

    public function addMutuelle(MutuelleFaite $mutuelle): self
    {
        if (!$this->mutuelle->contains($mutuelle)) {
            $this->mutuelle[] = $mutuelle;
            $mutuelle->setMutuelle($this);
        }

        return $this;
    }

    public function removeMutuelle(MutuelleFaite $mutuelle): self
    {
        if ($this->mutuelle->removeElement($mutuelle)) {
            // set the owning side to null (unless already changed)
            if ($mutuelle->getMutuelle() === $this) {
                $mutuelle->setMutuelle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures(): Collection
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture): self
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setMutuelle($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture): self
    {
        if ($this->factures->removeElement($facture)) {
            // set the owning side to null (unless already changed)
            if ($facture->getMutuelle() === $this) {
                $facture->setMutuelle(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->patients->contains($patient)) {
            $this->patients[] = $patient;
            $patient->setMutuelle($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->patients->removeElement($patient)) {
            // set the owning side to null (unless already changed)
            if ($patient->getMutuelle() === $this) {
                $patient->setMutuelle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GPatient[]
     */
    public function getGPatients(): Collection
    {
        return $this->gPatients;
    }

    public function addGPatient(GPatient $gPatient): self
    {
        if (!$this->gPatients->contains($gPatient)) {
            $this->gPatients[] = $gPatient;
            $gPatient->setMutuelle($this);
        }

        return $this;
    }

    public function removeGPatient(GPatient $gPatient): self
    {
        if ($this->gPatients->removeElement($gPatient)) {
            // set the owning side to null (unless already changed)
            if ($gPatient->getMutuelle() === $this) {
                $gPatient->setMutuelle(null);
            }
        }

        return $this;
    }

    

    

    

  
}
