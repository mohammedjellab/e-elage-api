<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ComorbiditeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ComorbiditeRepository::class)
 */
class Comorbidite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="comorbidite")
     */
    private $comorbidite;

    /**
     * @ORM\OneToMany(targetEntity=GPatient::class, mappedBy="comorbidite")
     */
    private $gPatients;

    /**
     * @ORM\OneToMany(targetEntity=DossierMedical::class, mappedBy="comorbidite")
     */
    private $dossierMedicals;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->active = true;
        $this->comorbidite = new ArrayCollection();
        $this->gPatients = new ArrayCollection();
        $this->dossierMedicals = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getComorbidite(): Collection
    {
        return $this->comorbidite;
    }

    public function addComorbidite(Patient $comorbidite): self
    {
        if (!$this->comorbidite->contains($comorbidite)) {
            $this->comorbidite[] = $comorbidite;
            $comorbidite->setComorbidite($this);
        }

        return $this;
    }

    public function removeComorbidite(Patient $comorbidite): self
    {
        if ($this->comorbidite->removeElement($comorbidite)) {
            // set the owning side to null (unless already changed)
            if ($comorbidite->getComorbidite() === $this) {
                $comorbidite->setComorbidite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GPatient[]
     */
    public function getGPatients(): Collection
    {
        return $this->gPatients;
    }

    public function addGPatient(GPatient $gPatient): self
    {
        if (!$this->gPatients->contains($gPatient)) {
            $this->gPatients[] = $gPatient;
            $gPatient->setComorbidite($this);
        }

        return $this;
    }

    public function removeGPatient(GPatient $gPatient): self
    {
        if ($this->gPatients->removeElement($gPatient)) {
            // set the owning side to null (unless already changed)
            if ($gPatient->getComorbidite() === $this) {
                $gPatient->setComorbidite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DossierMedical[]
     */
    public function getDossierMedicals(): Collection
    {
        return $this->dossierMedicals;
    }

    public function addDossierMedical(DossierMedical $dossierMedical): self
    {
        if (!$this->dossierMedicals->contains($dossierMedical)) {
            $this->dossierMedicals[] = $dossierMedical;
            $dossierMedical->setComorbidite($this);
        }

        return $this;
    }

    public function removeDossierMedical(DossierMedical $dossierMedical): self
    {
        if ($this->dossierMedicals->removeElement($dossierMedical)) {
            // set the owning side to null (unless already changed)
            if ($dossierMedical->getComorbidite() === $this) {
                $dossierMedical->setComorbidite(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
