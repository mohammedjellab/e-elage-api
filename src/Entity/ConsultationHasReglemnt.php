<?php

namespace App\Entity;

use App\Repository\ConsultationHasReglemntRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=ConsultationHasReglemntRepository::class)
 */
class ConsultationHasReglemnt
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $montantRegle;

    /**
     * @ORM\ManyToOne(targetEntity=Reglement::class, inversedBy="hasConsultation")
     */
    private $hasConsultation;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class, inversedBy="hasReglement")
     */
    private $hasReglement;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getMontantRegle(): ?string
    {
        return $this->montantRegle;
    }

    public function setMontantRegle(?string $montantRegle): self
    {
        $this->montantRegle = $montantRegle;

        return $this;
    }

    public function getHasConsultation(): ?Reglement
    {
        return $this->hasConsultation;
    }

    public function setHasConsultation(?Reglement $hasConsultation): self
    {
        $this->hasConsultation = $hasConsultation;

        return $this;
    }

    public function getHasReglement(): ?Consultation
    {
        return $this->hasReglement;
    }

    public function setHasReglement(?Consultation $hasReglement): self
    {
        $this->hasReglement = $hasReglement;

        return $this;
    }
}
