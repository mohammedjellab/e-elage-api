<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeConsultationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TypeConsultationRepository::class)
 * @Table(name="type_consultation", 
 *    uniqueConstraints={
 *        @UniqueConstraint(name="centre_soin_global_id", 
 *            columns={"centre_soin_id", "id_global_id"})
 *    }
 * )
 */
class TypeConsultation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $couleur;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=6)
     */
    private $tarif;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $typeMime;

    /**
     * @ORM\ManyToOne(targetEntity=FamilleActe::class, inversedBy="familleActe")
     */
    private $familleActe;

    /**
     * @ORM\OneToMany(targetEntity=Consultation::class, mappedBy="typeConsultation")
     */
    private $typeConsultation;

  

    /**
     * @ORM\ManyToOne(targetEntity=TypeConsultationGlobal::class, inversedBy="typeConsultations")
     */
    private $idGlobal;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="typeConsultations")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=Duree::class, inversedBy="typeConsultations")
     */
    private $duree;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbreParJour;

  



    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->typeConsultation = new ArrayCollection();
    }

  

    public function getId(): ?string
    {
        return $this->id;
    }




    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getTarif(): ?string
    {
        return $this->tarif;
    }

    public function setTarif(string $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getTypeMime(): ?string
    {
        return $this->typeMime;
    }

    public function setTypeMime(string $typeMime): self
    {
        $this->typeMime = $typeMime;

        return $this;
    }

    public function getFamilleActe(): ?FamilleActe
    {
        return $this->familleActe;
    }

    public function setFamilleActe(?FamilleActe $familleActe): self
    {
        $this->familleActe = $familleActe;

        return $this;
    }

    /**
     * @return Collection|Consultation[]
     */
    public function getTypeConsultation(): Collection
    {
        return $this->typeConsultation;
    }

    public function addTypeConsultation(Consultation $typeConsultation): self
    {
        if (!$this->typeConsultation->contains($typeConsultation)) {
            $this->typeConsultation[] = $typeConsultation;
            $typeConsultation->setTypeConsultation($this);
        }

        return $this;
    }

    public function removeTypeConsultation(Consultation $typeConsultation): self
    {
        if ($this->typeConsultation->removeElement($typeConsultation)) {
            // set the owning side to null (unless already changed)
            if ($typeConsultation->getTypeConsultation() === $this) {
                $typeConsultation->setTypeConsultation(null);
            }
        }

        return $this;
    }



    

    public function getIdGlobal(): ?TypeConsultationGlobal
    {
        return $this->idGlobal;
    }

    public function setIdGlobal(?TypeConsultationGlobal $idGlobal): self
    {
        $this->idGlobal = $idGlobal;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDuree(): ?Duree
    {
        return $this->duree;
    }

    public function setDuree(?Duree $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getNbreParJour(): ?int
    {
        return $this->nbreParJour;
    }

    public function setNbreParJour(?int $nbreParJour): self
    {
        $this->nbreParJour = $nbreParJour;

        return $this;
    }



    

   

 
}
