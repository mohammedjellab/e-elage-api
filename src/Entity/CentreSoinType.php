<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CentreSoinTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CentreSoinTypeRepository::class)
 */
class CentreSoinType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=CentreSoin::class, mappedBy="type")
     */
    private $centreSoins;

    public function __construct()
    {
        $this->centreSoins = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|CentreSoin[]
     */
    public function getCentreSoins(): Collection
    {
        return $this->centreSoins;
    }

    public function addCentreSoin(CentreSoin $centreSoin): self
    {
        if (!$this->centreSoins->contains($centreSoin)) {
            $this->centreSoins[] = $centreSoin;
            $centreSoin->setType($this);
        }

        return $this;
    }

    public function removeCentreSoin(CentreSoin $centreSoin): self
    {
        if ($this->centreSoins->removeElement($centreSoin)) {
            // set the owning side to null (unless already changed)
            if ($centreSoin->getType() === $this) {
                $centreSoin->setType(null);
            }
        }

        return $this;
    }
}
