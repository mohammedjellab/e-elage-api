<?php

namespace App\Entity;

use App\Repository\GPatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GPatientRepository::class)
 */
class GPatient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $cin;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\ManyToOne(targetEntity=Ville::class, inversedBy="gPatients")
     */
    private $ville;

    /**
     * @ORM\ManyToOne(targetEntity=Pays::class, inversedBy="gPatients")
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $sexe;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $situationFamille;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $civilite;

    /**
     * @ORM\ManyToOne(targetEntity=Comorbidite::class, inversedBy="gPatients")
     */
    private $comorbidite;

    /**
     * @ORM\OneToMany(targetEntity=LPatient::class, mappedBy="gpatient")
     */
    private $lPatients;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $allergieConnue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $profession;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fumeur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbEnfant;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $matricule;

    /**
     * @ORM\ManyToOne(targetEntity=GroupeSanguin::class, inversedBy="gPatients")
     */
    private $groupSanguin;

   


    

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $recommendation;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $fixe;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $ramed;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="gPatients")
     */
    private $centreSoin;

    /**
     * @ORM\ManyToOne(targetEntity=Mutuelle::class, inversedBy="gPatients")
     */
    private $mutuelle;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $antecedent;

    public function __construct()
    {
        $this->active =true;
        $this->lPatients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCin(): ?string
    {
        return $this->cin;
    }

    public function setCin(?string $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(?string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getSituationFamille(): ?string
    {
        return $this->situationFamille;
    }

    public function setSituationFamille(?string $situationFamille): self
    {
        $this->situationFamille = $situationFamille;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getComorbidite(): ?Comorbidite
    {
        return $this->comorbidite;
    }

    public function setComorbidite(?Comorbidite $comorbidite): self
    {
        $this->comorbidite = $comorbidite;

        return $this;
    }

    /**
     * @return Collection|LPatient[]
     */
    public function getLPatients(): Collection
    {
        return $this->lPatients;
    }

    public function addLPatient(LPatient $lPatient): self
    {
        if (!$this->lPatients->contains($lPatient)) {
            $this->lPatients[] = $lPatient;
            $lPatient->setGpatient($this);
        }

        return $this;
    }

    public function removeLPatient(LPatient $lPatient): self
    {
        if ($this->lPatients->removeElement($lPatient)) {
            // set the owning side to null (unless already changed)
            if ($lPatient->getGpatient() === $this) {
                $lPatient->setGpatient(null);
            }
        }

        return $this;
    }

    public function getAllergieConnue(): ?string
    {
        return $this->allergieConnue;
    }

    public function setAllergieConnue(?string $allergieConnue): self
    {
        $this->allergieConnue = $allergieConnue;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setProfession(?string $profession): self
    {
        $this->profession = $profession;

        return $this;
    }

    public function getFumeur(): ?bool
    {
        return $this->fumeur;
    }

    public function setFumeur(?bool $fumeur): self
    {
        $this->fumeur = $fumeur;

        return $this;
    }

    public function getNbEnfant(): ?int
    {
        return $this->nbEnfant;
    }

    public function setNbEnfant(?int $nbEnfant): self
    {
        $this->nbEnfant = $nbEnfant;

        return $this;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(?string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getGroupSanguin(): ?GroupeSanguin
    {
        return $this->groupSanguin;
    }

    public function setGroupSanguin(?GroupeSanguin $groupSanguin): self
    {
        $this->groupSanguin = $groupSanguin;

        return $this;
    }





   

    public function getRecommendation(): ?string
    {
        return $this->recommendation;
    }

    public function setRecommendation(string $recommendation): self
    {
        $this->recommendation = $recommendation;

        return $this;
    }

    public function getFixe(): ?string
    {
        return $this->fixe;
    }

    public function setFixe(?string $fixe): self
    {
        $this->fixe = $fixe;

        return $this;
    }

    public function getRamed(): ?string
    {
        return $this->ramed;
    }

    public function setRamed(?string $ramed): self
    {
        $this->ramed = $ramed;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getMutuelle(): ?Mutuelle
    {
        return $this->mutuelle;
    }

    public function setMutuelle(?Mutuelle $mutuelle): self
    {
        $this->mutuelle = $mutuelle;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getAntecedent(): ?string
    {
        return $this->antecedent;
    }

    public function setAntecedent(?string $antecedent): self
    {
        $this->antecedent = $antecedent;

        return $this;
    }
}
