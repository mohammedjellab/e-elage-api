<?php

namespace App\Entity;

use App\Repository\AgendaConfigRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=AgendaConfigRepository::class)
 */
class AgendaConfig
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $firstDay;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $modeAffichage;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heureOuverture;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heureFermeture;

    /**
     * @ORM\Column(type="time")
     */
    private $heureDebutOuvrable;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heureFinOuvrable;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $granularite;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="agendaConfigs")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $zoom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxActe;
    
    public function __construct(){
        $this->maxActe=5;
        $this->id = Uuid::v4();
    }
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFirstDay(): ?string
    {
        return $this->firstDay;
    }

    public function setFirstDay(?string $firstDay): self
    {
        $this->firstDay = $firstDay;

        return $this;
    }

    public function getModeAffichage(): ?string
    {
        return $this->modeAffichage;
    }

    public function setModeAffichage(string $modeAffichage): self
    {
        $this->modeAffichage = $modeAffichage;

        return $this;
    }

    public function getHeureOuverture(): ?\DateTimeInterface
    {
        return $this->heureOuverture;
    }

    public function setHeureOuverture(?\DateTimeInterface $heureOuverture): self
    {
        $this->heureOuverture = $heureOuverture;

        return $this;
    }

    public function getHeureFermeture(): ?\DateTimeInterface
    {
        return $this->heureFermeture;
    }

    public function setHeureFermeture(?\DateTimeInterface $heureFermeture): self
    {
        $this->heureFermeture = $heureFermeture;

        return $this;
    }

    public function getHeureDebutOuvrable(): ?\DateTimeInterface
    {
        return $this->heureDebutOuvrable;
    }

    public function setHeureDebutOuvrable(\DateTimeInterface $heureDebutOuvrable): self
    {
        $this->heureDebutOuvrable = $heureDebutOuvrable;

        return $this;
    }

    public function getHeureFinOuvrable(): ?\DateTimeInterface
    {
        return $this->heureFinOuvrable;
    }

    public function setHeureFinOuvrable(?\DateTimeInterface $heureFinOuvrable): self
    {
        $this->heureFinOuvrable = $heureFinOuvrable;

        return $this;
    }

    public function getGranularite(): ?\DateTimeInterface
    {
        return $this->granularite;
    }

    public function setGranularite(?\DateTimeInterface $granularite): self
    {
        $this->granularite = $granularite;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getZoom(): ?string
    {
        return $this->zoom;
    }

    public function setZoom(?string $zoom): self
    {
        $this->zoom = $zoom;

        return $this;
    }

    public function getMaxActe(): ?int
    {
        return $this->maxActe;
    }

    public function setMaxActe(?int $maxActe): self
    {
        $this->maxActe = $maxActe;

        return $this;
    }
}
