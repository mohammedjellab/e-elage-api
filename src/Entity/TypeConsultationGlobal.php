<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeConsultationGlobalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TypeConsultationGlobalRepository::class)
 * @UniqueEntity("libelle")
 */
class TypeConsultationGlobal
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     * 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=TypeConsultation::class, mappedBy="idGlobal")
     */
    private $typeConsultations;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->typeConsultations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|TypeConsultation[]
     */
    public function getTypeConsultations(): Collection
    {
        return $this->typeConsultations;
    }

    public function addTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if (!$this->typeConsultations->contains($typeConsultation)) {
            $this->typeConsultations[] = $typeConsultation;
            $typeConsultation->setIdGlobal($this);
        }

        return $this;
    }

    public function removeTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if ($this->typeConsultations->removeElement($typeConsultation)) {
            // set the owning side to null (unless already changed)
            if ($typeConsultation->getIdGlobal() === $this) {
                $typeConsultation->setIdGlobal(null);
            }
        }

        return $this;
    }
}
