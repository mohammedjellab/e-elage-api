<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MedicamentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Serializer\Annotation\MaxDepth;


/**
 *
 * @ORM\Entity(repositoryClass=MedicamentRepository::class)
 */
class Medicament
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomCommercial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @ORM\ManyToOne(targetEntity=Unite::class, inversedBy="unite")
     */
    private $unite;

    /**
     * @ORM\ManyToOne(targetEntity=MedicamentFamille::class, inversedBy="medicamentFamille")
     */
    private $medicamentFamille;

    /**
     * @MaxDepth(2)
     * @Groups("dcis")
     * @ORM\ManyToMany(targetEntity=Dci::class, mappedBy="dci")
     */
    private $dcis;

    /**
     * @ORM\ManyToOne(targetEntity=Laboratoire::class, inversedBy="medicaments")
     */
    private $laboratoire;

    /**
     * @ORM\ManyToOne(targetEntity=ClasseMedica::class, inversedBy="medicaments")
     */
    private $classeMedical;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $posologie;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $forme;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixVente;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixAchat;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixBRemboursementPPM;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixHospitalier;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $baseRembousementPPV;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixBaseRemboursement;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duree;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $consigne;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rembourse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dosage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $presentation;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('commercialisé', 'non commércialisé')", nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codeBarre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $surdosage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codeAtc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $natureProduit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $substancePsychoactive;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $utilisation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contreIndications;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $effetsIndesirables;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $conditionsPrescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $indications;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $risquePotentiel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notePerso;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    public function __construct()
    {
        // $this->id = Uuid::v4();
        $this->dcis = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCommercial(): ?string
    {
        return $this->nomCommercial;
    }

    public function setNomCommercial(string $nomCommercial): self
    {
        $this->nomCommercial = $nomCommercial;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getUnite(): ?Unite
    {
        return $this->unite;
    }

    public function setUnite(?Unite $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    public function getMedicamentFamille(): ?MedicamentFamille
    {
        return $this->medicamentFamille;
    }

    public function setMedicamentFamille(?MedicamentFamille $medicamentFamille): self
    {
        $this->medicamentFamille = $medicamentFamille;

        return $this;
    }

    /**
     * @return Collection|Dci[]
     */
    public function getDcis(): Collection
    {
        return $this->dcis;
    }

    public function addDci(Dci $dci): self
    {
        if (!$this->dcis->contains($dci)) {
            $this->dcis[] = $dci;
            $dci->addDci($this);
        }

        return $this;
    }

    /*public function setDcis($dcis): self
    {
        // print('count' . count($dcis));
        $this->dcis = new ArrayCollection($dcis);
        
        return $this;
    }*/

    public function removeDci(Dci $dci): self
    {
        if ($this->dcis->removeElement($dci)) {
            $dci->removeDci($this);
        }

        return $this;
    }

    /*public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(string $valeur): self
    {
        $this->valeur = $valeur;

        return $this; 
    }*/

    public function getPosologie(): ?string
    {
        return $this->posologie;
    }

    public function setPosologie(string $posologie): self
    {
        $this->posologie = $posologie;

        return $this;
    }

    public function getForme(): ?string
    {
        return $this->forme;
    }

    public function setForme(string $forme): self
    {
        $this->forme = $forme;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getQuantite(): ?float
    {
        return $this->quantite;
    }

    public function setQuantite(float $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getConsigne(): ?string
    {
        return $this->consigne;
    }

    public function setConsigne(string $consigne): self
    {
        $this->consigne = $consigne;

        return $this;
    }

    public function getRembourse(): ?bool
    {
        return $this->rembourse;
    }

    public function setRembourse(bool $rembourse): self
    {
        $this->rembourse = $rembourse;

        return $this;
    }

    public function getDosage(): ?string
    {
        return $this->dosage;
    }

    public function setDosage(string $dosage): self
    {
        $this->dosage = $dosage;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getLaboratoire(): ?Laboratoire
    {
        return $this->laboratoire;
    }

    public function setLaboratoire(?Laboratoire $laboratoire): self
    {
        $this->laboratoire = $laboratoire;

        return $this;
    }

    public function getClasseMedical(): ?ClasseMedica
    {
        return $this->classeMedical;
    }

    public function setClasseMedical(?ClasseMedica $classeMedical): self
    {
        $this->classeMedical = $classeMedical;

        return $this;
    }

    public function getNotePerso(): ?string
    {
        return $this->notePerso;
    }

    public function setNotePerso(string $notePerso): self
    {
        $this->notePerso = $notePerso;

        return $this;
    }

    public function getPrixVente(): ?string
    {
        return $this->prixVente;
    }

    public function setPrixVente(string $prixVente): self
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    public function getPrixAchat(): ?string
    {
        return $this->prixAchat;
    }

    public function setPrixAchat(string $prixAchat): self
    {
        $this->prixAchat = $prixAchat;

        return $this;
    }

    public function getPrixBRemboursementPPM(): ?string
    {
        return $this->prixBRemboursementPPM;
    }

    public function setPrixBRemboursementPPM(string $prixBRemboursementPPM): self
    {
        $this->prixBRemboursementPPM = $prixBRemboursementPPM;

        return $this;
    }

    public function getPrixHospitalier(): ?string
    {
        return $this->prixHospitalier;
    }

    public function setPrixHospitalier(string $prixHospitalier): self
    {
        $this->prixHospitalier = $prixHospitalier;

        return $this;
    }

    public function getBaseRembousementPPV(): ?string
    {
        return $this->baseRembousementPPV;
    }

    public function setBaseRembousementPPV(string $baseRembousementPPV): self
    {
        $this->baseRembousementPPV = $baseRembousementPPV;

        return $this;
    }

    public function getPrixBaseRemboursement(): ?string
    {
        return $this->prixBaseRemboursement;
    }

    public function setPrixBaseRemboursement(string $prixBaseRemboursement): self
    {
        $this->prixBaseRemboursement = $prixBaseRemboursement;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getCodeBarre(): ?string
    {
        return $this->codeBarre;
    }

    public function setCodeBarre(string $codeBarre): self
    {
        $this->codeBarre = $codeBarre;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSurdosage(): ?string
    {
        return $this->surdosage;
    }

    public function setSurdosage(string $surdosage): self
    {
        $this->surdosage = $surdosage;

        return $this;
    }

    public function getCodeAtc(): ?string
    {
        return $this->codeAtc;
    }

    public function setCodeAtc(string $codeAtc): self
    {
        $this->codeAtc = $codeAtc;

        return $this;
    }

    public function getNatureProduit(): ?string
    {
        return $this->natureProduit;
    }

    public function setNatureProduit(string $natureProduit): self
    {
        $this->natureProduit = $natureProduit;

        return $this;
    }

    public function getSubstancePsychoactive(): ?string
    {
        return $this->substancePsychoactive;
    }

    public function setSubstancePsychoactive(string $substancePsychoactive): self
    {
        $this->substancePsychoactive = $substancePsychoactive;

        return $this;
    }

    public function getUtilisation(): ?string
    {
        return $this->utilisation;
    }

    public function setUtilisation(string $utilisation): self
    {
        $this->utilisation = $utilisation;

        return $this;
    }

    public function getContreIndications(): ?string
    {
        return $this->contreIndications;
    }

    public function setContreIndications(string $contreIndications): self
    {
        $this->contreIndications = $contreIndications;

        return $this;
    }

    public function getEffetsIndesirables(): ?string
    {
        return $this->effetsIndesirables;
    }

    public function setEffetsIndesirables(string $effetsIndesirables): self
    {
        $this->effetsIndesirables = $effetsIndesirables;

        return $this;
    }

    public function getConditionsPrescription(): ?string
    {
        return $this->conditionsPrescription;
    }

    public function setConditionsPrescription(string $conditionsPrescription): self
    {
        $this->conditionsPrescription = $conditionsPrescription;

        return $this;
    }

    public function getIndications(): ?string
    {
        return $this->indications;
    }

    public function setIndications(string $indications): self
    {
        $this->indications = $indications;

        return $this;
    }

    public function getRisquePotentiel(): ?string
    {
        return $this->risquePotentiel;
    }

    public function setRisquePotentiel(string $risquePotentiel): self
    {
        $this->risquePotentiel = $risquePotentiel;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
