<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CrOperatoireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CrOperatoireRepository::class)
 */
class CrOperatoire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=TypeCrOperatoire::class, inversedBy="date_enregistement")
     */
    private $typeCrOperatoire;

    /**
     * @ORM\ManyToOne(targetEntity=CrOperatoire::class, inversedBy="crOperatoires")
     */
    private $CrOperatoire;

    /**
     * @ORM\OneToMany(targetEntity=CrOperatoire::class, mappedBy="CrOperatoire")
     */
    private $crOperatoires;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $objectCr;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="crOperatoires")
     */
    private $patient;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->crOperatoires = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTypeCrOperatoire(): ?TypeCrOperatoire
    {
        return $this->typeCrOperatoire;
    }

    public function setTypeCrOperatoire(?TypeCrOperatoire $typeCrOperatoire): self
    {
        $this->typeCrOperatoire = $typeCrOperatoire;

        return $this;
    }

    public function getCrOperatoire(): ?self
    {
        return $this->CrOperatoire;
    }

    public function setCrOperatoire(?self $CrOperatoire): self
    {
        $this->CrOperatoire = $CrOperatoire;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCrOperatoires(): Collection
    {
        return $this->crOperatoires;
    }

    public function addCrOperatoire(self $crOperatoire): self
    {
        if (!$this->crOperatoires->contains($crOperatoire)) {
            $this->crOperatoires[] = $crOperatoire;
            $crOperatoire->setCrOperatoire($this);
        }

        return $this;
    }

    public function removeCrOperatoire(self $crOperatoire): self
    {
        if ($this->crOperatoires->removeElement($crOperatoire)) {
            // set the owning side to null (unless already changed)
            if ($crOperatoire->getCrOperatoire() === $this) {
                $crOperatoire->setCrOperatoire(null);
            }
        }

        return $this;
    }

    public function getObjectCr(): ?string
    {
        return $this->objectCr;
    }

    public function setObjectCr(string $objectCr): self
    {
        $this->objectCr = $objectCr;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }
}
