<?php

namespace App\Entity;

use App\Repository\AnalyseLaboratoireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=AnalyseLaboratoireRepository::class)
 */
class AnalyseLaboratoire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $famille;

    /**
     * @ORM\OneToMany(targetEntity=BilanAnalyse::class, mappedBy="analyseLaboratoire")
     */
    private $bilanAnalyses;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->bilanAnalyses = new ArrayCollection();
    }

   

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getFamille(): ?string
    {
        return $this->famille;
    }

    public function setFamille(string $famille): self
    {
        $this->famille = $famille;

        return $this;
    }

    /**
     * @return Collection|BilanAnalyse[]
     */
    public function getBilanAnalyses(): Collection
    {
        return $this->bilanAnalyses;
    }

    public function addBilanAnalysis(BilanAnalyse $bilanAnalysis): self
    {
        if (!$this->bilanAnalyses->contains($bilanAnalysis)) {
            $this->bilanAnalyses[] = $bilanAnalysis;
            $bilanAnalysis->setAnalyseLaboratoire($this);
        }

        return $this;
    }

    public function removeBilanAnalysis(BilanAnalyse $bilanAnalysis): self
    {
        if ($this->bilanAnalyses->removeElement($bilanAnalysis)) {
            // set the owning side to null (unless already changed)
            if ($bilanAnalysis->getAnalyseLaboratoire() === $this) {
                $bilanAnalysis->setAnalyseLaboratoire(null);
            }
        }

        return $this;
    }

 

    
}
