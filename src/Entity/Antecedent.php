<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;

use App\Repository\AntecedentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AntecedentRepository::class)
 * @UniqueEntity("libelle")
 */
class Antecedent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $libelle;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $origine;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ordre;

    /**
     * @ORM\ManyToMany(targetEntity=LPatient::class, inversedBy="antecedents")
     */
    private $Lpatient;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->active = true;
        $this->Lpatient = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOrigine(): ?string
    {
        return $this->origine;
    }

    public function setOrigine(string $origine): self
    {
        $this->origine = $origine;

        return $this;
    }

    public function getOrdre(): ?float
    {
        return $this->ordre;
    }

    public function setOrdre(?float $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * @return Collection|LPatient[]
     */
    public function getLpatient(): Collection
    {
        return $this->Lpatient;
    }

    public function addLpatient(LPatient $lpatient): self
    {
        if (!$this->Lpatient->contains($lpatient)) {
            $this->Lpatient[] = $lpatient;
        }

        return $this;
    }

    public function removeLpatient(LPatient $lpatient): self
    {
        $this->Lpatient->removeElement($lpatient);

        return $this;
    }
}
