<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AmbulanceTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AmbulanceTypeRepository::class)
 */
class AmbulanceType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="ambulanceType")
     */
    private $UserAmbulanceType;

    public function __construct()
    {
        $this->UserAmbulanceType = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserAmbulanceType(): Collection
    {
        return $this->UserAmbulanceType;
    }

    public function addUserAmbulanceType(User $userAmbulanceType): self
    {
        if (!$this->UserAmbulanceType->contains($userAmbulanceType)) {
            $this->UserAmbulanceType[] = $userAmbulanceType;
            $userAmbulanceType->setAmbulanceType($this);
        }

        return $this;
    }

    public function removeUserAmbulanceType(User $userAmbulanceType): self
    {
        if ($this->UserAmbulanceType->removeElement($userAmbulanceType)) {
            // set the owning side to null (unless already changed)
            if ($userAmbulanceType->getAmbulanceType() === $this) {
                $userAmbulanceType->setAmbulanceType(null);
            }
        }

        return $this;
    }
}
