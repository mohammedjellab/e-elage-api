<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SurveillancePatientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SurveillancePatientRepository::class)
 */
class SurveillancePatient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;



    /**
     * @ORM\Column(type="datetime")
     */
    private $dateConsult;



    /**
     * @ORM\ManyToOne(targetEntity=ParamSurveillance::class, inversedBy="surveillancePatients")
     */
    private $paramSurveillance;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity=LPatient::class, inversedBy="surveillancePatients")
     */
    private $lpatient;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $valeur;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): ?string
    {
        return $this->id;
    }



    public function getDateConsult(): ?\DateTimeInterface
    {
        return $this->dateConsult;
    }

    public function setDateConsult(\DateTimeInterface $dateConsult): self
    {
        $this->dateConsult = $dateConsult;

        return $this;
    }



    public function getParamSurveillance(): ?ParamSurveillance
    {
        return $this->paramSurveillance;
    }

    public function setParamSurveillance(?ParamSurveillance $paramSurveillance): self
    {
        $this->paramSurveillance = $paramSurveillance;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getLpatient(): ?LPatient
    {
        return $this->lpatient;
    }

    public function setLpatient(?LPatient $lpatient): self
    {
        $this->lpatient = $lpatient;

        return $this;
    }

    public function getValeur(): ?Float
    {
        return $this->valeur;
    }

    public function setValeur(Float $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }


}
