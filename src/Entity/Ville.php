<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\VilleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=VilleRepository::class)
 */
class Ville
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="ville")
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity=CentreSoin::class, mappedBy="ville")
     */
    private $centreSoins;

    /**
     * @ORM\OneToMany(targetEntity=GPatient::class, mappedBy="ville")
     */
    private $gPatients;

   

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->ville = new ArrayCollection();
        $this->centreSoins = new ArrayCollection();
        $this->gPatients = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }



    /**
     * @return Collection|CentreSoin[]
     */
    public function getCentreSoins(): Collection
    {
        return $this->centreSoins;
    }

    public function addCentreSoin(CentreSoin $centreSoin): self
    {
        if (!$this->centreSoins->contains($centreSoin)) {
            $this->centreSoins[] = $centreSoin;
            $centreSoin->setVille($this);
        }

        return $this;
    }

    public function removeCentreSoin(CentreSoin $centreSoin): self
    {
        if ($this->centreSoins->removeElement($centreSoin)) {
            // set the owning side to null (unless already changed)
            if ($centreSoin->getVille() === $this) {
                $centreSoin->setVille(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GPatient[]
     */
    public function getGPatients(): Collection
    {
        return $this->gPatients;
    }

    public function addGPatient(GPatient $gPatient): self
    {
        if (!$this->gPatients->contains($gPatient)) {
            $this->gPatients[] = $gPatient;
            $gPatient->setVille($this);
        }

        return $this;
    }

    public function removeGPatient(GPatient $gPatient): self
    {
        if ($this->gPatients->removeElement($gPatient)) {
            // set the owning side to null (unless already changed)
            if ($gPatient->getVille() === $this) {
                $gPatient->setVille(null);
            }
        }

        return $this;
    }


}
