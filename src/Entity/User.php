<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;



    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity=TypeUser::class, inversedBy="users")
     */
    private $typeUser;

    /**
     * @ORM\ManyToOne(targetEntity=Specialite::class, inversedBy="users")
     */
    private $specialite;

    /**
     * @ORM\OneToMany(targetEntity=UserLog::class, mappedBy="userid")
     */
    private $userLogs;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="users")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;





  

    /**
     * @ORM\OneToMany(targetEntity=AgendaEvenement::class, mappedBy="praticien")
     */
    private $agendaEvenements;

    /**
     * @ORM\OneToMany(targetEntity=LPatient::class, mappedBy="praticien")
     */
    private $lPatients;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $document;

    /**
     * @ORM\ManyToOne(targetEntity=AmbulanceType::class, inversedBy="UserAmbulanceType")
     */
    private $ambulanceType;





 

    public function __construct(){
        $this->id = Uuid::v4();
        $this->active = true;
        $this->userLogs = new ArrayCollection();
        $this->agendaEvenements = new ArrayCollection();
        $this->lPatients = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }



    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTypeUser(): ?TypeUser
    {
        return $this->typeUser;
    }

    public function setTypeUser(?TypeUser $typeUser): self
    {
        $this->typeUser = $typeUser;

        return $this;
    }

    public function getSpecialite(): ?Specialite
    {
        return $this->specialite;
    }

    public function setSpecialite(?Specialite $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }

    /**
     * @return Collection|UserLog[]
     */
    public function getUserLogs(): Collection
    {
        return $this->userLogs;
    }

    public function addUserLog(UserLog $userLog): self
    {
        if (!$this->userLogs->contains($userLog)) {
            $this->userLogs[] = $userLog;
            $userLog->setUserid($this);
        }

        return $this;
    }

    public function removeUserLog(UserLog $userLog): self
    {
        if ($this->userLogs->removeElement($userLog)) {
            // set the owning side to null (unless already changed)
            if ($userLog->getUserid() === $this) {
                $userLog->setUserid(null);
            }
        }

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    




    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }









   

    /**
     * @return Collection|AgendaEvenement[]
     */
    public function getAgendaEvenements(): Collection
    {
        return $this->agendaEvenements;
    }

    public function addAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if (!$this->agendaEvenements->contains($agendaEvenement)) {
            $this->agendaEvenements[] = $agendaEvenement;
            $agendaEvenement->setPraticien($this);
        }

        return $this;
    }

    public function removeAgendaEvenement(AgendaEvenement $agendaEvenement): self
    {
        if ($this->agendaEvenements->removeElement($agendaEvenement)) {
            // set the owning side to null (unless already changed)
            if ($agendaEvenement->getPraticien() === $this) {
                $agendaEvenement->setPraticien(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LPatient[]
     */
    public function getLPatients(): Collection
    {
        return $this->lPatients;
    }

    public function addLPatient(LPatient $lPatient): self
    {
        if (!$this->lPatients->contains($lPatient)) {
            $this->lPatients[] = $lPatient;
            $lPatient->setPraticien($this);
        }

        return $this;
    }

    public function removeLPatient(LPatient $lPatient): self
    {
        if ($this->lPatients->removeElement($lPatient)) {
            // set the owning side to null (unless already changed)
            if ($lPatient->getPraticien() === $this) {
                $lPatient->setPraticien(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDocument(): ?string
    {
        return $this->document;
    }

    public function setDocument(?string $document): self
    {
        $this->document = $document;

        return $this;
    }

    public function getAmbulanceType(): ?AmbulanceType
    {
        return $this->ambulanceType;
    }

    public function setAmbulanceType(?AmbulanceType $ambulanceType): self
    {
        $this->ambulanceType = $ambulanceType;

        return $this;
    }



 

 






}
