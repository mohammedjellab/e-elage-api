<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SpecialiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SpecialiteRepository::class)
 * @UniqueEntity("name")
 */
class Specialite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="specialite")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="specialites")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToMany(targetEntity=TypeUser::class, inversedBy="specialites")
     */
    private $typeSpeciality;



    public function __construct(){
        $this->id = Uuid::v4();
        $this->active = true;
        $this->users = new ArrayCollection();
        $this->typeSpeciality = new ArrayCollection();
    }



    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }



    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            
        }

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|TypeUser[]
     */
    public function getTypeSpeciality(): Collection
    {
        return $this->typeSpeciality;
    }

    public function addTypeSpeciality(TypeUser $typeSpeciality): self
    {
        if (!$this->typeSpeciality->contains($typeSpeciality)) {
            $this->typeSpeciality[] = $typeSpeciality;
        }

        return $this;
    }

    public function removeTypeSpeciality(TypeUser $typeSpeciality): self
    {
        $this->typeSpeciality->removeElement($typeSpeciality);

        return $this;
    }





}
