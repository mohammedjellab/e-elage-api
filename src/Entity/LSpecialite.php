<?php

namespace App\Entity;

use App\Repository\LSpecialiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=LSpecialiteRepository::class)
 */
class LSpecialite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;




    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="lSpecialites")
     */
    private $centreSoin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=GSpecialite::class, inversedBy="lSpecialites")
     */
    private $gspecialite;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }





    

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function setCentreSoinC(?int $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }
    public function setCentreSoinCustum(?int $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getGspecialite(): ?GSpecialite
    {
        return $this->gspecialite;
    }

    public function setGspecialite(?GSpecialite $gspecialite): self
    {
        $this->gspecialite = $gspecialite;

        return $this;
    }
}
