<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FactureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $observation;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $typeMime;

    /**
     * @ORM\OneToMany(targetEntity=TypePaiement::class, mappedBy="facture")
     */
    private $typePaiement;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $etatFacture;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $numFacture;

    /**
     * @ORM\OneToMany(targetEntity=ModeleFacture::class, mappedBy="modeleFacture")
     */
    private $modeleFacture;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateFacture;


    /**
     * @ORM\OneToMany(targetEntity=LigneFacture::class, mappedBy="facture")
     */
    private $ligneFactures;

    /**
     * @ORM\ManyToOne(targetEntity=Mutuelle::class, inversedBy="factures")
     */
    private $mutuelle;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="factures")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=CentreSoin::class, inversedBy="factures")
     */
    private $centreSoin;


    public function __construct()
    {
        $this->modeleFacture = new ArrayCollection();
        $this->ligneFactures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getTypeMime(): ?string
    {
        return $this->typeMime;
    }

    public function setTypeMime(string $typeMime): self
    {
        $this->typeMime = $typeMime;

        return $this;
    }

    public function getEtatFacture(): ?string
    {
        return $this->etatFacture;
    }

    public function setEtatFacture(string $etatFacture): self
    {
        $this->etatFacture = $etatFacture;

        return $this;
    }

    public function getNumFacture(): ?string
    {
        return $this->numFacture;
    }

    public function setNumFacture(string $numFacture): self
    {
        $this->numFacture = $numFacture;

        return $this;
    }

    /**
     * @return Collection|ModeleFacture[]
     */
    public function getModeleFacture(): Collection
    {
        return $this->modeleFacture;
    }

    public function addModeleFacture(ModeleFacture $modeleFacture): self
    {
        if (!$this->modeleFacture->contains($modeleFacture)) {
            $this->modeleFacture[] = $modeleFacture;
            $modeleFacture->setModeleFacture($this);
        }

        return $this;
    }

    public function removeModeleFacture(ModeleFacture $modeleFacture): self
    {
        if ($this->modeleFacture->removeElement($modeleFacture)) {
            // set the owning side to null (unless already changed)
            if ($modeleFacture->getModeleFacture() === $this) {
                $modeleFacture->setModeleFacture(null);
            }
        }

        return $this;
    }

    public function getDateFacture(): ?\DateTimeInterface
    {
        return $this->dateFacture;
    }

    public function setDateFacture(\DateTimeInterface $dateFacture): self
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    



    /**
     * @return Collection|LigneFacture[]
     */
    public function getLigneFactures(): Collection
    {
        return $this->ligneFactures;
    }

    public function addLigneFacture(LigneFacture $ligneFacture): self
    {
        if (!$this->ligneFactures->contains($ligneFacture)) {
            $this->ligneFactures[] = $ligneFacture;
            $ligneFacture->setFacture($this);
        }

        return $this;
    }

    public function removeLigneFacture(LigneFacture $ligneFacture): self
    {
        if ($this->ligneFactures->removeElement($ligneFacture)) {
            // set the owning side to null (unless already changed)
            if ($ligneFacture->getFacture() === $this) {
                $ligneFacture->setFacture(null);
            }
        }

        return $this;
    }

    public function getMutuelle(): ?Mutuelle
    {
        return $this->mutuelle;
    }

    public function setMutuelle(?Mutuelle $mutuelle): self
    {
        $this->mutuelle = $mutuelle;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getCentreSoin(): ?CentreSoin
    {
        return $this->centreSoin;
    }

    public function setCentreSoin(?CentreSoin $centreSoin): self
    {
        $this->centreSoin = $centreSoin;

        return $this;
    }


}
