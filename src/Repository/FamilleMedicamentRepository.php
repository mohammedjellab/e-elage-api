<?php

namespace App\Repository;

use App\Entity\FamilleMedicament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FamilleMedicament|null find($id, $lockMode = null, $lockVersion = null)
 * @method FamilleMedicament|null findOneBy(array $criteria, array $orderBy = null)
 * @method FamilleMedicament[]    findAll()
 * @method FamilleMedicament[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilleMedicamentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FamilleMedicament::class);
    }

    // /**
    //  * @return FamilleMedicament[] Returns an array of FamilleMedicament objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FamilleMedicament
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
