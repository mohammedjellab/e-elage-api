<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\TypeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function searchCorrespandant($value)
    {
        return $this->createQueryBuilder('u')
            ->select('u.firstname','u.lastname','u.id')
            ->andWhere('u.lastname like :val or u.firstname like :val2')
            ->setParameter('val', "%$value%")
            ->setParameter('val2', "%$value%")
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllMedecinByEtablissment($value,$centre)
    {
        return $this->createQueryBuilder('u')
            ->select('u.firstname','u.lastname','u.id')
            ->where('u.typeUser = :val ')
            ->setParameter('val', $value)
            ->andWhere('u.centreSoin = :centreSoin ')
            ->setParameter('centreSoin', $centre)
            ->getQuery()
            ->getResult()
        ;
    }
    public function findAllUsers()
    {
        return $this->createQueryBuilder('u')
            ->select('u.firstname,u.lastname,u.id,u.tel , u.adresse, u.email' )
            //->where('u.typeUser = :val ')
            //->setParameter('val', $value)
            //->andWhere('u.centreSoin = :centreSoin ')
            //->setParameter('centreSoin', $centre)
            ->getQuery()
            ->getResult()
        ;
    }

    /* public function finUsersByType($lbl, $query)
    {
        return $this->createQueryBuilder('u')
            ->select(['u.firstname', 'u.lastname', 'u.id'])
            ->innerJoin('App\Entity\TypeUser', 'g', 'WITH','u.typeUser = g.id')
            ->where('g.libelle =:lbl')
            ->setParameter('lbl', $lbl)
            
            ->andWhere('u.firstname LIKE :query OR u.lastname LIKE :query')
             ->setParameter('query', "%".$query."%")
            ->getQuery()
            ->getResult();

         return $this->getEntityManager()
            ->createQuery(
                'SELECT u.firstname, u.lastname, u.id FROM App\Entity\User  u INNER JOIN App\Entity\TypeUser  g ON u.type_user_id = g.id WHERE g.libelle ="assistante"'
            )
            ->getResult(); 
    }  */

    public function finUsersByType($value) {
        $conn = $this->getEntityManager()
        ->getConnection();
        $sql = '
            SELECT id, name, type from (
                SELECT 
                    `user`.id, Concat(lastname, firstname) as name, `type_user`.`libelle` as "type" 
                FROM 
                    `user` 
                INNER JOIN 
                    `type_user` 
                ON 
                    type_user_id = `type_user`.id 
                WHERE 
                    libelle IN ("Médecin", "Infermiere", "Assistante")       
                UNION
                    SELECT id, name, "specialite" as "type" from specialite
                UNION
                    SELECT 
                        id, CONCAT(etablissement, "(", code, ")") as name, "cabinet" as "type" from centre_soin
                    WHERE
                        type_id IN (
                            SELECT id FROM `centre_soin_type` WHERE type = "Cabinet"
                        )

            ) as t
            WHERE 
                name LIKE :query 
            ORDER BY 
                type ASC
        ';
        $stmt = $conn
        ->prepare($sql);
        $stmt->bindValue(':query', "%" . $value . "%");
        return $stmt->executeQuery();
    }
    
    
}
