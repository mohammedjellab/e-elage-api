<?php

namespace App\Repository;

use App\Entity\Acte;
use App\Entity\Reglement;
use App\Entity\Consultation;
use App\Entity\LPatient;
use App\Entity\Patient;
use App\Entity\TypeConsultation;
use App\Entity\TypeConsultationGlobal;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Consultation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Consultation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Consultation[]    findAll()
 * @method Consultation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsultationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Consultation::class);
    }

    // /**
    //  * @return Consultation[] Returns an array of Consultation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Consultation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllFamilles()
    {
        return $this->createQueryBuilder('m')
            ->select('a.libelle,m.libelle AS famille, a.prix ')
            ->innerJoin(Acte::class, 'a')
            ->where('a.familleActe = m.id')
            ->getQuery()->getResult();
    }

    public function listConsultations($patient)
    {
        return $this->createQueryBuilder('c')
            ->select('c.dateConsultation, t.libelle, a.libelle AS acte ')
            
            ->leftJoin(TypeConsultation::class, 't', \Doctrine\ORM\Query\Expr\Join::WITH,'c.typeConsultation = t.id')
            ->leftJoin(Acte::class, 'a', \Doctrine\ORM\Query\Expr\Join::WITH,'c.acte = a.id')
            //->leftJoin(User::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'c.patient = u.id')
            ->andWhere('c.patient= :val')
            ->setParameter('val',$patient)
            ->getQuery()
            ->getResult()
        ;
    }

     //recuperer les informations pour régler dans l'agenda
     public function reglementByRdv($rdv)
     {
         $sql = "SELECT c.id consultation_id,rc.has_reglement_id,c.montant, 
                 FORMAT(rc.montant_regle,0)  AS montant_regle,
                 DATE_FORMAT(c.date_consultation,'%d/%m/%Y') as dateConsultation,
                 DATE_FORMAT(r.date_regement,'%d/%m/%Y') as dateReglement,
                 DATE_FORMAT(c.date_consultation,'%H:%i') as heureConsultation,
                 tg.libelle acte,
                 IFNULL(0,(FORMAT(r.montant_regle,0)/c.montant) *100) AS pourcentage,
                 FORMAT(c.montant,0) - FORMAT(rc.montant_regle,0)   as reste,
                 c.lpatient_id, c.centre_soin_id as centreSoin
                 FROM consultation c
                 LEFT JOIN consultation_has_reglemnt rc ON rc.has_reglement_id = c.id
                 LEFT JOIN reglement r ON r.id = rc.has_consultation_id
                 LEFT JOIN type_consultation t ON t.id = c.type_consultation_id
                 LEFT JOIN type_consultation_global tg ON tg.id = t.id_global_id
                 WHERE c.agenda_id = $rdv
                 ";
         $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
         $stmt->executeQuery();
         return $stmt->fetchAll();
     }
    //pour les deux tableaux dans la fiche patient
    public function listConsultationsByLpatient($patient)
    {
        return $this->createQueryBuilder('c')
            ->select("DATE_FORMAT(c.dateConsultation,'%d/%m/%Y') as dateConsultation,
                      DATE_FORMAT(c.dateConsultation,'%H:%i') as heureConsultation,
                      tg.libelle acte, c.montant,
                      c.id,
                      concat(u.lastname,' ' ,u.firstname) AS praticien ")
            ///->leftJoin(TypeConsultation::class, 't', \Doctrine\ORM\Query\Expr\Join::WITH,'c.typeConsultation = t.id')

            ->leftJoin(TypeConsultation::class,'t', 'WITH','t.id = c.typeConsultation')
            ->leftJoin(TypeConsultationGlobal::class,'tg', 'WITH','tg.id = t.idGlobal')

            ->leftJoin(LPatient::class, 'lp', \Doctrine\ORM\Query\Expr\Join::WITH,'c.lpatient = lp.id')
            ->leftJoin(User::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'lp.praticien = u.id')
            ->andWhere('lp.id= :val')
            ->setParameter('val',$patient)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findInformationReglement($consultation_id)
    {
        $entityManager = $this->getEntityManager();
        $result = $entityManager->createQuery('SELECT c
        FROM App\Entity\Consultation c
        INNER JOIN App\Entity\Reglement r ON r.
        WHERE c.id = :consultation_id
        ')
        ->setParameter('consultation_id',$consultation_id)
        ->getOneOrNullResult();
        return $result;
    }
    
    // list des consultations
    public function situationFinanciere($lpatient_id)
    {
        $sql = "SELECT c.id consultation_id,rc.has_reglement_id,c.montant, 
                FORMAT(SUM(rc.montant_regle),0)  AS montant_regle,
                DATE_FORMAT(c.date_consultation,'%d/%m/%Y') as dateConsultation,
                DATE_FORMAT(c.date_consultation,'%H:%i') as heureConsultation,
                tg.libelle acte,
                IFNULL(0,(FORMAT(sum(r.montant_regle),0)/c.montant) *100) AS pourcentage,
                FORMAT(c.montant,0) - FORMAT(SUM(rc.montant_regle),0)   as reste,
                c.lpatient_id, c.centre_soin_id as centreSoin
                FROM consultation c
                LEFT JOIN consultation_has_reglemnt rc ON rc.has_reglement_id = c.id
                LEFT JOIN reglement r ON r.id = rc.has_consultation_id
                LEFT JOIN type_consultation t ON t.id = c.type_consultation_id
                LEFT JOIN type_consultation_global tg ON tg.id = t.id_global_id
                WHERE c.lpatient_id = $lpatient_id
                GROUP BY c.id  ";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->executeQuery();
        return $stmt->fetchAll();
    }
    // list reglements
    public function listReglement($lpatient_id)
    {
        $sql = "SELECT c.id consultation_id,rc.has_reglement_id,c.montant, 
                FORMAT(rc.montant_regle,0)  AS montant_regle,
                DATE_FORMAT(c.date_consultation,'%d/%m/%Y') as dateConsultation,
                DATE_FORMAT(r.date_regement,'%d/%m/%Y') as dateReglement,
                DATE_FORMAT(c.date_consultation,'%H:%i') as heureConsultation,
                tg.libelle acte,
                IFNULL(0,(FORMAT(r.montant_regle,0)/c.montant) *100) AS pourcentage,
                FORMAT(c.montant,0) - FORMAT(rc.montant_regle,0)   as reste,
                c.lpatient_id, c.centre_soin_id as centreSoin
                FROM consultation c
                INNER JOIN consultation_has_reglemnt rc ON rc.has_reglement_id = c.id
                INNER JOIN reglement r ON r.id = rc.has_consultation_id
                LEFT JOIN type_consultation t ON t.id = c.type_consultation_id
                LEFT JOIN type_consultation_global tg ON tg.id = t.id_global_id
                WHERE c.lpatient_id = $lpatient_id
                ";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->executeQuery();
        return $stmt->fetchAll();
    }

    // situation financiere
    public function situationFinanciereOLd($lpatient_id)
    {
        $sql = " SELECT c.id consultation_id,rc.reglement_id,c.montant, 
FORMAT(SUM(r.montant_regle),0) + FORMAT(c.montant-SUM(r.montant_regle),0)  AS montant_regle,
        DATE_FORMAT(c.date_consultation,'%d/%m/%Y') as dateConsultation,
        DATE_FORMAT(c.date_consultation,'%H:%i') as heureConsultation,
        t.libelle acte,
        IFNULL(0,(FORMAT(sum(r.montant_regle),0)/c.montant) *100) AS pourcentage,
        FORMAT(SUM(r.montant_regle),0) + FORMAT(c.montant-SUM(r.montant_regle),0)-c.montant as reste,
        c.lpatient_id, c.centre_soin_id as centreSoin
        FROM consultation c
        LEFT JOIN reglement_consultation rc ON rc.consultation_id = c.id
        LEFT JOIN reglement r ON r.id = rc.reglement_id
        LEFT JOIN type_consultation t ON t.id = c.type_consultation_id
        WHERE c.lpatient_id = $lpatient_id
        GROUP BY c.id  ";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->executeQuery();
        return $stmt->fetchAll();
    }

}
//

//, concat(u.lastname,' ' ,u.firstname)AS praticien