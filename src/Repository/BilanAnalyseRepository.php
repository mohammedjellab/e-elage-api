<?php

namespace App\Repository;

use App\Entity\BilanAnalyse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BilanAnalyse|null find($id, $lockMode = null, $lockVersion = null)
 * @method BilanAnalyse|null findOneBy(array $criteria, array $orderBy = null)
 * @method BilanAnalyse[]    findAll()
 * @method BilanAnalyse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BilanAnalyseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BilanAnalyse::class);
    }

    // /**
    //  * @return BilanAnalyse[] Returns an array of BilanAnalyse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BilanAnalyse
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
