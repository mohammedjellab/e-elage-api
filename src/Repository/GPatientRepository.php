<?php

namespace App\Repository;

use App\Entity\AgendaEvenement;
use App\Entity\GPatient;
use App\Entity\LPatient;
use App\Entity\Mutuelle;
use App\Entity\User;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GPatient|null find($id, $lockMode = null, $lockVersion = null)
 * @method GPatient|null findOneBy(array $criteria, array $orderBy = null)
 * @method GPatient[]    findAll()
 * @method GPatient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GPatientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GPatient::class);
    }

    // /**
    //  * @return GPatient[] Returns an array of GPatient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GPatient
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /*public function getGPatient($id) {
        return $this->createQueryBuilder('g')
                    ->where('g.id = :id')
                    ->setParameter()
                    ->getQuery()
                    ->getResult();
    }*/

    public function searchPatient($patient)
    {
        return $this->createQueryBuilder('p')
            ->select('p.lastname, p.firstname , p.id, p.cin, p.civilite, p.adresse, p.tel, p.civilite, p.fixe, lp.id as lpatient_id')
            //->innerJoin(Mutuelle::class,'m')
            //->where('m.id = p.mutuelle')
            //->leftJoin(Mutuelle::class, 'm', \Doctrine\ORM\Query\Expr\Join::WITH,' p.mutuelle = m.id')
            //->leftJoin(Ville::class, 'v', \Doctrine\ORM\Query\Expr\Join::WITH,'p.ville = v.id')
            ->innerJoin(LPatient::class, 'lp', \Doctrine\ORM\Query\Expr\Join::WITH,'p.id = lp.gpatient')
            //->leftJoin(User::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'lp.praticien = u.id')
            ->andWhere('p.firstname like  :val')
            ->setParameter('val',"%".$patient."%")
            ->getQuery()
            ->getResult()
        ;
    }

    public function searchPatients($date,$centre,$praticien,$telephone,$lastname,$firstname,$num_dossier)
    {
         $q = $this->createQueryBuilder('p')
            ->select("p.lastname, p.firstname , p.id  as gpatient_id, p.cin, p.adresse, p.tel,
                      p.civilite, p.fixe, m.libelle as mutuelle, v.name AS ville,
                      lp.adressePar, concat(u.lastname,' ' ,u.firstname)AS praticien,lp.id as lpatient_id ")
            //->innerJoin(Mutuelle::class,'m')
            //->where('m.id = p.mutuelle')
            ->leftJoin(Mutuelle::class, 'm', \Doctrine\ORM\Query\Expr\Join::WITH,' p.mutuelle = m.id')
            ->leftJoin(Ville::class, 'v', \Doctrine\ORM\Query\Expr\Join::WITH,'p.ville = v.id')
            ->leftJoin(LPatient::class, 'lp', \Doctrine\ORM\Query\Expr\Join::WITH,'p.id = lp.gpatient')
            ->leftJoin(User::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'lp.praticien = u.id')
            ->leftJoin(AgendaEvenement::class, 'a', \Doctrine\ORM\Query\Expr\Join::WITH,'a.lpatient = lp.id')
            ->andWhere('lp.centreSoin= :centre')
            ->andWhere('p.active =  1');
            if($telephone){
                $q->andWhere('p.tel= :telephone')
                ->setParameter('telephone',$telephone);
            }
            if($lastname){
                $q->andWhere('p.lastname like :lastname')
                ->setParameter('lastname',"%$lastname%");
            }
            if($firstname){
                $q->andWhere('p.firstname like :firstname')
                ->setParameter('firstname',"%$firstname%");
            }
            /*if($firstname){
                $q->andWhere('p.firstname like :firstname')
                ->setParameter('firstname',"%$firstname%");
            }
            if($num_dossier){
                $q->andWhere('a.num_dossier = :num_dossier')
                ->setParameter('num_dossier',"%$num_dossier%");
            }
            if($date){
                $q->andWhere('a.dateDebut = :date')
                ->setParameter('date',"$date");
            }*/

            if($praticien){
                $q->andWhere('lp.praticien= :praticien')
                ->setParameter('praticien',$praticien);
            }
            
            $q->setParameter('centre',$centre);
             // ->groupBy('p.id')
        return $q->getQuery()->getResult();
    }

    //pour fiche patient
    public function findPatientById($gpatient,$centreSoin){
        $q = $this->createQueryBuilder('p')
            ->select("p.lastname, p.firstname , p.id  as gpatient_id, p.cin, p.adresse, p.tel,
                      p.civilite, p.fixe,p.recommendation,
                      p.sexe,p.situationFamille,p.profession,
                      DATE_FORMAT(p.dateNaissance,'%m/%d/%Y') as dateNaissance,
                      p.ramed,p.nbEnfant,p.email,p.photo,
                      m.libelle as mutuelleName,
                      m.id as mutuelle,
                      concat(u.lastname,' ' ,u.firstname) AS praticien,
                      u.id as medecin,
                      lp.id as lpatient_id,
                      lp.adressePar,
                      v.id AS ville,v.name AS villeName ")
            ->leftJoin(Mutuelle::class, 'm', \Doctrine\ORM\Query\Expr\Join::WITH,' p.mutuelle = m.id')
            ->leftJoin(Ville::class, 'v', \Doctrine\ORM\Query\Expr\Join::WITH,'p.ville = v.id')
            ->leftJoin(LPatient::class, 'lp', \Doctrine\ORM\Query\Expr\Join::WITH,'p.id = lp.gpatient')
            ->leftJoin(User::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'lp.praticien = u.id')
            //->leftJoin(AgendaEvenement::class, 'a', \Doctrine\ORM\Query\Expr\Join::WITH,'a.lpatient = lp.id')
            ->andWhere('lp.centreSoin= :centreSoin')
            ->andWhere('p.id= :gpatient')
            ->setParameter('centreSoin',$centreSoin)
            ->setParameter('gpatient',$gpatient);
            return $q->getQuery()->getOneOrNullResult();
    }
    //pour dossier médical
    public function findPatientByLpatientId($lpatient,$centreSoin){
        $q = $this->createQueryBuilder('p')
            ->select("p.lastname, p.firstname,
                      concat(p.civilite, ' ',p.lastname,' ', p.firstname) AS nom_complet,
                      p.id  as gpatient_id, p.cin, p.adresse, p.tel,
                      p.civilite, p.fixe,p.recommendation,
                      p.sexe,p.situationFamille,p.profession,
                      DATE_FORMAT(p.dateNaissance,'%m/%d/%Y') as dateNaissance,
                      p.ramed,p.nbEnfant,p.email,p.photo,
                      m.libelle as mutuelleName,
                      m.id as mutuelle,
                      concat(u.lastname,' ' ,u.firstname) AS praticien,
                      u.id as medecin,
                      lp.id as lpatient_id,
                      lp.adressePar,
                      v.id AS ville,v.name AS villeName ")
            ->leftJoin(Mutuelle::class, 'm', \Doctrine\ORM\Query\Expr\Join::WITH,' p.mutuelle = m.id')
            ->leftJoin(Ville::class, 'v', \Doctrine\ORM\Query\Expr\Join::WITH,'p.ville = v.id')
            ->leftJoin(LPatient::class, 'lp', \Doctrine\ORM\Query\Expr\Join::WITH,'p.id = lp.gpatient')
            ->leftJoin(User::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'lp.praticien = u.id')
            //->leftJoin(AgendaEvenement::class, 'a', \Doctrine\ORM\Query\Expr\Join::WITH,'a.lpatient = lp.id')
            ->andWhere('lp.centreSoin= :centreSoin')
            ->andWhere('lp.id= :lpatient')
            ->setParameter('centreSoin',$centreSoin)
            ->setParameter('lpatient',$lpatient);
            return $q->getQuery()->getOneOrNullResult();
    }

    
}
