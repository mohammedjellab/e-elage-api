<?php

namespace App\Repository;

use App\Entity\Dci;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dci|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dci|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dci[]    findAll()
 * @method Dci[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DciRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dci::class);
    }



    public function findByActive(bool $active = true) {
        $qb = $this->createQueryBuilder('d')
            ->where('d.active = ?1')
            ->setParameter('1', $active);
        $query = $qb->getQuery();
        return $query->getResult();
            
    }

    public function findAll()
    {
        return $this->createQueryBuilder('a')
            ->select(['a.id', 'a.libelle as name'])
            ->where('d.active = ?1')
            ->setParameter('1', true)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    // /**
    //  * @return Dci[] Returns an array of Dci objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dci
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
