<?php

namespace App\Repository;

use App\Entity\CrOperatoire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CrOperatoire|null find($id, $lockMode = null, $lockVersion = null)
 * @method CrOperatoire|null findOneBy(array $criteria, array $orderBy = null)
 * @method CrOperatoire[]    findAll()
 * @method CrOperatoire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CrOperatoireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CrOperatoire::class);
    }

    // /**
    //  * @return CrOperatoire[] Returns an array of CrOperatoire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CrOperatoire
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
