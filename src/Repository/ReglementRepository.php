<?php

namespace App\Repository;

use App\Entity\Reglement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reglement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reglement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reglement[]    findAll()
 * @method Reglement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReglementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reglement::class);
    }

    // /**
    //  * @return Reglement[] Returns an array of Reglement objects
    //  */
    /* */

   


    
    
    // list reglements
    public function searchReglement($typeConsultation,$date)
    {
        $sql = "SELECT c.id consultation_id,rc.has_reglement_id,c.montant, 
                FORMAT(rc.montant_regle,0)  AS montant_regle,
                DATE_FORMAT(c.date_consultation,'%d/%m/%Y') as dateConsultation,
                DATE_FORMAT(r.date_regement,'%d/%m/%Y') as dateReglement,
                DATE_FORMAT(c.date_consultation,'%H:%i') as heureConsultation,
                tg.libelle acte,
                IFNULL(0,(FORMAT(r.montant_regle,0)/c.montant) *100) AS pourcentage,
                FORMAT(c.montant,0) - FORMAT(rc.montant_regle,0)   as reste,
                c.lpatient_id, c.centre_soin_id as centreSoin,
                r.type_paiement as type_reglement,
                concat(gp.civilite,' ',gp.lastname ,' ', gp.firstname) AS nom_complet,
                gp.id AS gpatient_id,
                t.couleur as couleur
                FROM consultation c
                INNER JOIN consultation_has_reglemnt rc ON rc.has_reglement_id = c.id
                INNER JOIN reglement r ON r.id = rc.has_consultation_id
                INNER JOIN lpatient lp ON lp.id = c.lpatient_id
                INNER JOIN gpatient gp ON gp.id = lp.gpatient_id
                LEFT JOIN type_consultation t ON t.id = c.type_consultation_id
                LEFT JOIN type_consultation_global tg ON tg.id = t.id_global_id
                WHERE r.date_regement like '%$date%' ";
                if($typeConsultation){
                   $sql .= " and t.id = $typeConsultation ";
                } 
                
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->executeQuery();
        return $stmt->fetchAll();
    }

    // mes interventions 
    public function mesInterventions($typeConsultation,$date)
    {
        $sql = " SELECT c.id consultation_id,rc.has_reglement_id,c.montant, 
        FORMAT(rc.montant_regle,0)  AS montant_regle,
        DATE_FORMAT(c.date_consultation,'%d/%m/%Y') as dateConsultation,
        DATE_FORMAT(c.date_consultation,'%H:%i') as heureConsultation,
        tg.libelle acte,
        
        c.lpatient_id, c.centre_soin_id as centreSoin,
        r.type_paiement as type_reglement,
        concat(gp.civilite,' ',gp.lastname ,' ', gp.firstname) AS nom_complet,
        gp.id AS gpatient_id,
        t.couleur as couleur
        FROM consultation c
        LEFT JOIN consultation_has_reglemnt rc ON rc.has_reglement_id = c.id
        LEFT JOIN reglement r ON r.id = rc.has_consultation_id
        INNER JOIN lpatient lp ON lp.id = c.lpatient_id
        INNER JOIN gpatient gp ON gp.id = lp.gpatient_id
        LEFT JOIN type_consultation t ON t.id = c.type_consultation_id
        LEFT JOIN type_consultation_global tg ON tg.id = t.id_global_id
        WHERE c.date_consultation like '%$date%' ";
        if($typeConsultation){
            $sql .= " and t.id = $typeConsultation ";
        } 
        //GROUP BY c.id ";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->executeQuery();
        return $stmt->fetchAll();
        //FORMAT(c.montant,0) - FORMAT(SUM(rc.montant_regle),0)   as reste,
    }

    

    
}
