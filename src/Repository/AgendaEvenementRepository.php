<?php

namespace App\Repository;

use App\Entity\AgendaEvenement;
use App\Entity\GPatient;
use App\Entity\Legende;
use App\Entity\LPatient;
use App\Entity\Mutuelle;
use App\Entity\Salle;
use App\Entity\TypeConsultation;
use App\Entity\TypeConsultationGlobal;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AgendaEvenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgendaEvenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgendaEvenement[]    findAll()
 * @method AgendaEvenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgendaEvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgendaEvenement::class);
    }

    // /**
    //  * @return AgendaEvenement[] Returns an array of AgendaEvenement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgendaEvenement
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getHolidays(){
        return $this->createQueryBuilder('h')
            ->select("h.start as start2, h.end as end2 , h.type ,  h.couleur")
            //->andWhere('h.exampleField = :val')
            //->setParameter('val', $value)
            //->orderBy('h.id', 'ASC')
            //->setMaxResults(10)DATE_FORMAT(h.end,'%d/%m/%Y %H:%i') end,
            ->getQuery()
            ->getResult()
        ;
    }
    
    //cette methode pour calculer le nbre d'acte entre deux date et grouper par jour
    //dans l'agenda je l'afficher par couleur le nombre dans le petit calendrier
    public function nbreActeByInterval($start,$end)
    {
        return $this->createQueryBuilder('a')
            ->select(" count(a.id) nbre, IDENTITY(a.typeConsultation) as acte , DATE_FORMAT(a.dateDebut,'%m/%d/%Y') as date  ")
            ->where(' a.dateDebut <= :end')
            ->andWhere('a.dateDebut >= :start')
            ->setParameter('start', $start." 00:00:00")
            ->setParameter('end', $end." 23:59:59")
            ->groupBy('date')
            ->getQuery()
            ->getResult()
        ;
    }
    public function nbreRdvByTypeConsultation($centre)
    {
        return $this->createQueryBuilder('a')
            ->select('c.libelle, count(c.libelle) nbre')
            ->innerJoin(TypeConsultation::class,'c')
            ->where('c.id = a.typeConsultation')
            ->andWhere('a.centreSoin = :val')
            ->setParameter('val', $centre)
            ->groupBy('c.id')
            ->getQuery()
            ->getResult()
        ;
    }
    
    
    
    public function findRdvById($id){
        $q = $this->createQueryBuilder('a')
                ->select(" a.dateDebut,substring(a.dateDebut,1,4) AS year_start ,
                substring( a.dateDebut,6,2) AS month_start,
                substring( a.dateDebut,9,2) AS day_start,
                substring( a.heureArrivee,1,2) AS hours_start,
                substring( a.heureArrivee,4,2) AS minute_start,

                a.dateDebut,substring(a.dateFin,1,4) AS year_end ,
                substring( a.dateFin,6,2) AS month_end,
                substring( a.dateFin,9,2) AS day_end,
                substring( a.heureSortie,1,2) AS hours_end,
                substring( a.heureSortie,4,2) AS minute_end,
                
                concat(lg.civilite,' ',lg.lastname ,' ', lg.firstname) AS nom_complet,
                substring( a.heureArrivee,1,5) AS heureArrivee,
                substring( a.heureSortie,1,5) AS heureSortie,
                lg.lastname , lg.firstname,a.id,lp.id as lpatient_id,lg.id as gpatient_id ,
                t.id as typeConsultation,
                tg.libelle as acte,
                m.id AS mutuelle,
                DATE_FORMAT(a.dateDebut,'%m/%d/%Y') as start,
                DATE_FORMAT(a.dateFin,'%m/%d/%Y') as end ,
                lg.civilite AS civilite,
                a.sansRdv as sansRdv ,
                s.id as salle,
                a.statut,
                l.id AS legende,
                l.color as color,
                lg.cin as cin,
                lg.tel as tel,
                IDENTITY(a.praticien) AS praticien,
                a.active AS annule,

                a.lastname patient_ss_rdv_lastname, a.firstname patient_ss_rdv_firstname, a.civilite patient_ss_rdv_civilite")
                ->leftJoin(LPatient::class,'lp','WITH' ,'a.lpatient = lp.id')
                ->leftJoin(GPatient::class,'lg', 'WITH','lg.id = lp.gpatient')
                ->leftJoin(TypeConsultation::class,'t', 'WITH','t.id = a.typeConsultation')
                ->leftJoin(TypeConsultationGlobal::class,'tg', 'WITH','tg.id = t.idGlobal')
                ->leftJoin(Mutuelle::class,'m', 'WITH','m.id = lg.mutuelle')
                ->leftJoin(Salle::class,'s', 'WITH','s.id = a.salle')
                ->innerJoin(Legende::class,'l', 'WITH','l.id = a.legende')
                ->andWhere('a.id = :id')
                ->setParameter('id',$id);
        return $q->getQuery()->getResult();

       
        
                
    }



    public function calendarRdv($date,$start,$end,$centre,$salle,$praticien,$type_journee,$term,$en_salle){
        $m = "14:00:00";
        //, a.typeConsultation, lg.mutuelle as mutuelle , lg.dateNaissance as , a.nbEnfant AS nbEnfant
        $q = $this->createQueryBuilder('a')
                ->select(" a.id,a.dateDebut,substring(a.dateDebut,1,4) AS year_start ,
                substring( a.dateDebut,6,2) AS month_start,
                substring( a.dateDebut,9,2) AS day_start,
                substring( a.heureArrivee,1,2) AS hours_start,
                substring( a.heureArrivee,4,2) AS minute_start,
                
                a.dateDebut,substring(a.dateFin,1,4) AS year_end ,
                substring( a.dateFin,6,2) AS month_end,
                substring( a.dateFin,9,2) AS day_end,
                substring( a.heureSortie,1,2) AS hours_end,
                substring( a.heureSortie,4,2) AS minute_end,

                concat(lg.civilite,' ',lg.lastname ,' ', lg.firstname) AS nom_complet,
                concat(a.civilite,' ',a.lastname ,' ', a.firstname) AS nom_complet_ss_rdv,
                lg.lastname , lg.firstname,lp.id as lpatient_id,lg.id as gpatient_id ,
                l.id AS legende,
                a.sansRdv,
                a.statut,
                a.active AS annule,
                t.couleur as color,
                l.color as colorLegende,
                t.id as typeConsultation,
                tg.libelle as acte,
                u.id AS praticien,
                s.id AS salle,
                DATE_FORMAT(a.dateDebut, '%Y-%m-%d' ) AS dateJour,  
                DAYNAME(a.dateDebut) AS dayName,
                lg.cin as cin,
                lg.tel as tel,
                a.lastname patient_ss_rdv_lastname, a.firstname patient_ss_rdv_firstname, a.civilite patient_ss_rdv_civilite")
                ->leftJoin(LPatient::class,'lp','WITH' ,'a.lpatient = lp.id')
                ->leftJoin(GPatient::class,'lg', 'WITH','lg.id = lp.gpatient')
                ->leftJoin(TypeConsultation::class,'t', 'WITH','t.id = a.typeConsultation')
                ->leftJoin(TypeConsultationGlobal::class,'tg', 'WITH','tg.id = t.idGlobal')
                ->innerJoin(Legende::class,'l', 'WITH','l.id = a.legende')
                ->leftJoin(Salle::class,'s', 'WITH','s.id = a.salle')
                ->leftJoin(User::class,'u', 'WITH','u.id = a.praticien')
                ->andWhere('a.centreSoin = :centre')
                //->andWhere('a.active = 1 ')
                ->setParameter('centre',$centre)
                ->orderBy('a.dateDebut', 'ASC');
            if($date){
                $q->andWhere('a.dateDebut LIKE :date_debut')
                ->setParameter('date_debut',"%$date%");
            }
           // $start = '2021-09-20';
           // $end = '2021-09-26';
            if($start && $end){
                //`date_debut` >= '2021-09-20' AND `date_fin` <= '2021-09-26 17:32:50'
                $q->andWhere('a.dateDebut >= :start ')
                ->setParameter('start',$start.' 00:00:00');
                $q->andWhere('a.dateFin <= :end ')
                ->setParameter('end',$end.' 23:59:59 ');
            }

            if($salle){
                $q->andWhere('a.salle = :salle')
                ->setParameter('salle',$salle);
            }
            if($praticien){
                $q->andWhere('a.praticien = :praticien')
                ->setParameter('praticien',$praticien);
            }
            if($term){
                $q->andWhere('(lg.firstname like   :term OR lg.lastname like   :term)')
                ->setParameter('term',"%$term%");
            }

            if($type_journee=="matin"){

                $q->andWhere('a.dateDebut < :matin')
                ->setParameter('matin',"$date $m");
            }
            if($type_journee=="midi"){

                $q->andWhere('a.dateDebut > :midi')
                ->setParameter('midi',"$date $m");
            }
            
                
        return $q->getQuery()->getResult();

            
                
    }

    public function searchRdv($date,$centre,$salle,$praticien,$type_journee,$term,$en_salle){
        $m = "14";
        //, a.typeConsultation, lg.mutuelle as mutuelle , lg.dateNaissance as , a.nbEnfant AS nbEnfant
        $q = $this->createQueryBuilder('a')
                ->select("  concat(lg.civilite,' ',lg.lastname ,' ', lg.firstname) AS nom_complet,
                            concat(a.civilite,' ',a.lastname ,' ', a.firstname) AS nom_complet_ss_patient,
                            lg.lastname , lg.firstname,a.id,lp.id as lpatient_id,
                            lg.id as gpatient_id , t.id as typeConsulation,m.id AS mutuelle")

                ->leftJoin(LPatient::class,'lp','WITH' ,'a.lpatient = lp.id')
                
                ->leftJoin(GPatient::class,'lg', 'WITH','lg.id = lp.gpatient')
                ->leftJoin(TypeConsultation::class,'t', 'WITH','t.id = a.typeConsultation')
                ->leftJoin(Mutuelle::class,'m', 'WITH','m.id = lg.mutuelle')
                ->andWhere('a.centreSoin = :centre')
                ->setParameter('centre',$centre);
            if($date){
                $q->andWhere('a.dateDebut LIKE :date_debut')
                ->setParameter('date_debut',"%$date%");
            }
            if($salle){
                $q->andWhere('a.salle = :salle')
                ->setParameter('salle',$salle);
            }
            if($praticien){
                $q->andWhere('a.praticien = :praticien')
                ->setParameter('praticien',$praticien);
            }
            if($term){
                $q->andWhere('(lg.firstname like   :term OR lg.lastname like   :term)')
                ->setParameter('term',"%$term%");
            }

            if($type_journee=="matin"){

                $q->andWhere('substring(a.heureArrivee,1,2) < :matin')
                ->setParameter('matin',"$m");
            }
            if($type_journee=="midi"){

                $q->andWhere('substring(a.heureArrivee,1,2) >= :val')
                ->setParameter('val',"$m");
            }
            
                
        return $q->getQuery()->getResult();

            
                
    }

    /*public function getSpecialiteByCentre($centre){
        return $this->createQueryBuilder('l')
                    ->select('l.id AS lspecialite_id,g.id  AS gspecialite_id,g.name ')
                    ->innerJoin(GSpecialite::class,'g')
                    ->where('l.gspecialite = g.id')
                    ->andWhere('l.centreSoin = :val')
                    ->setParameter('val',$centre)
                    ->getQuery()
                    ->getResult();
    }*/

}
