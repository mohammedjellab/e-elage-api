<?php

namespace App\Repository;

use App\Entity\ParamSurveillance;
use App\Entity\SurveillancePatient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SurveillancePatient|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveillancePatient|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveillancePatient[]    findAll()
 * @method SurveillancePatient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveillancePatientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SurveillancePatient::class);
    }

    // /**
    //  * @return SurveillancePatient[] Returns an array of SurveillancePatient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SurveillancePatient
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getConstanteMiniGraph($lpatient)
    {
        return $this->createQueryBuilder('p')
            ->select("p.valeur, c.libelle as libelle,  c.abreviation as abreviation, c.id as constanteId, DATE_FORMAT(p.dateConsult,'%m/%d/%Y %H:%i') as date ")
            ->innerJoin(ParamSurveillance::class,"c")
            ->where("c.id = p.paramSurveillance")
            ->andWhere('p.lpatient = :val')
            ->setParameter('val', $lpatient)
            ->setParameter('val', $lpatient)
            ->orderBy('p.dateConsult', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
//