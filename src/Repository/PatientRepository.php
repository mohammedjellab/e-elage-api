<?php

namespace App\Repository;

use App\Entity\Patient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Patient|null find($id, $lockMode = null, $lockVersion = null)
 * @method Patient|null findOneBy(array $criteria, array $orderBy = null)
 * @method Patient[]    findAll()
 * @method Patient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Patient::class);
    }

    // /**
    //  * @return Patient[] Returns an array of Patient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Patient
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getPatients()
    {
        return $this->createQueryBuilder('p')
            ->select('p.lastname, p.firstname , p.id, p.cin, p.adresse, p.tel, p.civilite')
            //->leftJoin(TypeConsultation::class, 't', \Doctrine\ORM\Query\Expr\Join::WITH,'c.typeConsultation = t.id')
            //->leftJoin(Acte::class, 'a', \Doctrine\ORM\Query\Expr\Join::WITH,'c.acte = a.id')
            //->leftJoin(User::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'c.patient = u.id')
            //->andWhere('c.patient= :val')
            //->setParameter('val',$patient)
            ->getQuery()
            ->getResult()
        ;
    }
}
