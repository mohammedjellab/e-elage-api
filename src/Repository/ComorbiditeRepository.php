<?php

namespace App\Repository;

use App\Entity\Comorbidite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comorbidite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comorbidite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comorbidite[]    findAll()
 * @method Comorbidite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComorbiditeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comorbidite::class);
    }

    // /**
    //  * @return Comorbidite[] Returns an array of Comorbidite objects
    //  */
    
    public function findAllComorbidite()
    {
        return $this->createQueryBuilder('c')
            ->select('c.id,c.libelle')
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Comorbidite
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
