<?php

namespace App\Repository;

use App\Entity\CentreSoinType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CentreSoinType|null find($id, $lockMode = null, $lockVersion = null)
 * @method CentreSoinType|null findOneBy(array $criteria, array $orderBy = null)
 * @method CentreSoinType[]    findAll()
 * @method CentreSoinType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CentreSoinTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CentreSoinType::class);
    }

    // /**
    //  * @return CentreSoinType[] Returns an array of CentreSoinType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CentreSoinType
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
