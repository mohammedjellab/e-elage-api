<?php

namespace App\Repository;

use App\Entity\GSpecialite;
use App\Entity\LSpecialite;
use App\Entity\CentreSoin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LSpecialite|null find($id, $lockMode = null, $lockVersion = null)
 * @method LSpecialite|null findOneBy(array $criteria, array $orderBy = null)
 * @method LSpecialite[]    findAll()
 * @method LSpecialite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LSpecialiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LSpecialite::class);
    }

    // /**
    //  * @return LSpecialite[] Returns an array of LSpecialite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LSpecialite
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getSpecialiteByCentre($centre){
        return $this->createQueryBuilder('l')
                    ->select('l.id AS lspecialite_id,g.id  AS gspecialite_id,g.name ')
                    ->innerJoin(GSpecialite::class,'g')
                    ->where('l.gspecialite = g.id')
                    ->andWhere('l.centreSoin = :val')
                    ->setParameter('val',$centre)
                    ->getQuery()
                    ->getResult();
    }

    
    public function getLSpecialiteAndCentre(){
        return $this->createQueryBuilder('l')
                    ->select('l.id AS lspecialite_id,g.id  AS gspecialite_id,g.name, c.etablissement ')
                    ->innerJoin(GSpecialite::class,'g')
                    ->where('l.centreSoin = c.id')
                    ->innerJoin(CentreSoin::class,'c')
                    ->andWhere('l.gspecialite = g.id')
                    ->getQuery()
                    ->getResult();
    }

    
}
