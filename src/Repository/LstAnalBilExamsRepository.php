<?php

namespace App\Repository;

use App\Entity\LstAnalBilExams;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LstAnalBilExams|null find($id, $lockMode = null, $lockVersion = null)
 * @method LstAnalBilExams|null findOneBy(array $criteria, array $orderBy = null)
 * @method LstAnalBilExams[]    findAll()
 * @method LstAnalBilExams[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LstAnalBilExamsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LstAnalBilExams::class);
    }

    // /**
    //  * @return LstAnalBilExams[] Returns an array of LstAnalBilExams objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LstAnalBilExams
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
