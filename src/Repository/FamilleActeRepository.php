<?php

namespace App\Repository;

use App\Entity\Acte;
use App\Entity\FamilleActe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FamilleActe|null find($id, $lockMode = null, $lockVersion = null)
 * @method FamilleActe|null findOneBy(array $criteria, array $orderBy = null)
 * @method FamilleActe[]    findAll()
 * @method FamilleActe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilleActeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FamilleActe::class);
    }

    // /**
    //  * @return FamilleActe[] Returns an array of FamilleActe objects
    //  */
    /**/
    public function findAllFamille()
    {
        return $this->createQueryBuilder('f')
            ->select('f.id, f.libelle')
            //->andWhere('f.exampleField = :val')
            //->setParameter('val', $value)
            ->orderBy('f.id', 'DESC')
            //->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?FamilleActe
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function searchByName($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.libelle like :val')
            ->setParameter('val', "%$value%")
            ->getQuery()
            ->getResult()
        ;
    }
    
    
}
