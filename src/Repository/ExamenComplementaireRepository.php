<?php

namespace App\Repository;

use App\Entity\ExamenComplementaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExamenComplementaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExamenComplementaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExamenComplementaire[]    findAll()
 * @method ExamenComplementaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExamenComplementaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExamenComplementaire::class);
    }

    // /**
    //  * @return ExamenComplementaire[] Returns an array of ExamenComplementaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExamenComplementaire
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
