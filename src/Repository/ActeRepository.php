<?php

namespace App\Repository;

use App\Entity\Acte;
use App\Entity\FamilleActe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Acte|null find($id, $lockMode = null, $lockVersion = null)
 * @method Acte|null findOneBy(array $criteria, array $orderBy = null)
 * @method Acte[]    findAll()
 * @method Acte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Acte::class);
    }

    // /**
    //  * @return Acte[] Returns an array of Acte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Acte
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
     public function findAllFamilles()
    {
        return $this->createQueryBuilder('m')
            ->select('a.libelle,m.libelle AS famille, a.prix ')
            ->innerJoin(Acte::class, 'a')
            ->where('a.familleActe = m.id')
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function findAllFamilles()
    {
        return $this->createQueryBuilder('a')
            ->select('a.libelle,f.libelle AS famille, a.prix, a.couleur ')
            ->leftJoin(FamilleActe::class, 'f', \Doctrine\ORM\Query\Expr\Join::WITH,'a.familleActe = f.id')
            ->getQuery()
            ->getResult()
        ;
    }

/*public function getHistory($users) {
    $qb = $this->entityManager->createQueryBuilder();
    $qb
        ->select('a', 'u')
        ->from('Credit\Entity\UserCreditHistory', 'a')
        ->leftJoin('a.user', 'u')
        ->where('u = :user')
        ->setParameter('user', $users)
        ->orderBy('a.created_at', 'DESC');

    return $qb->getQuery()->getResult();
}*/
}
