<?php

namespace App\Repository;

use App\Entity\ParamSurveillance;
use App\Entity\Unite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParamSurveillance|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParamSurveillance|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParamSurveillance[]    findAll()
 * @method ParamSurveillance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParamSurveillanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParamSurveillance::class);
    }

    // /**
    //  * @return ParamSurveillance[] Returns an array of ParamSurveillance objects
    //  */
    public function findAllActive()
    {
        return $this->createQueryBuilder('p')
            ->select('p.id,p.libelle,p.abreviation,u.id as unite,p.couleur,p.borneMax,p.BorneMin as borneMin')
            ->leftJoin(Unite::class, 'u', \Doctrine\ORM\Query\Expr\Join::WITH,'u.id = p.unite')
            ->where('p.active = :val')
            ->setParameter('val', true)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

   

    
}
