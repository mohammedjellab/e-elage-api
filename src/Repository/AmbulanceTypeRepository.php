<?php

namespace App\Repository;

use App\Entity\AmbulanceType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AmbulanceType|null find($id, $lockMode = null, $lockVersion = null)
 * @method AmbulanceType|null findOneBy(array $criteria, array $orderBy = null)
 * @method AmbulanceType[]    findAll()
 * @method AmbulanceType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmbulanceTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AmbulanceType::class);
    }

    // /**
    //  * @return AmbulanceType[] Returns an array of AmbulanceType objects
    //  */
    
    public function findAll()
    {
        return $this->createQueryBuilder('a')
            ->select(['a.id', 'a.type as name'])
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
   

    /*
    public function findOneBySomeField($value): ?AmbulanceType
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
