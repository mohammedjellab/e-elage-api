<?php

namespace App\Repository;

use App\Entity\PatientConfigCentre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatientConfigCentre|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatientConfigCentre|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatientConfigCentre[]    findAll()
 * @method PatientConfigCentre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatientConfigCentreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatientConfigCentre::class);
    }

    // /**
    //  * @return PatientConfigCentre[] Returns an array of PatientConfigCentre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PatientConfigCentre
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
