<?php

namespace App\Repository;

use App\Entity\LPatient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LPatient|null find($id, $lockMode = null, $lockVersion = null)
 * @method LPatient|null findOneBy(array $criteria, array $orderBy = null)
 * @method LPatient[]    findAll()
 * @method LPatient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LPatientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LPatient::class);
    }

    // /**
    //  * @return LPatient[] Returns an array of LPatient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LPatient
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function findAllAntecedantByPatient($p)
    {
        $entityManager = $this->getEntityManager();
        $result = $entityManager->createQuery('SELECT p
        FROM App\Entity\LPatient p
        WHERE p.id = :patient
        ')
        ->setParameter('patient',$p)
        ->getOneOrNullResult();
        return $result;
    }
}
