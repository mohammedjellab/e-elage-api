<?php

namespace App\Repository;

use App\Entity\GSpecialite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GSpecialite|null find($id, $lockMode = null, $lockVersion = null)
 * @method GSpecialite|null findOneBy(array $criteria, array $orderBy = null)
 * @method GSpecialite[]    findAll()
 * @method GSpecialite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GSpecialiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GSpecialite::class);
    }

    // /**
    //  * @return GSpecialite[] Returns an array of GSpecialite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GSpecialite
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
