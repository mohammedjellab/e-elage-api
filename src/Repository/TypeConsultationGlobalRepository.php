<?php

namespace App\Repository;

use App\Entity\Duree;
use App\Entity\TypeConsultation;
use App\Entity\TypeConsultationGlobal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeConsultationGlobal|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeConsultationGlobal|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeConsultationGlobal[]    findAll()
 * @method TypeConsultationGlobal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeConsultationGlobalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeConsultationGlobal::class);
    }

    // /**
    //  * @return TypeConsultationGlobal[] Returns an array of TypeConsultationGlobal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*for configuration et agenda*/
    public function findAllByCentreSoin($centreSoin)
    {
        return $this->createQueryBuilder('t')
            ->select("tg.libelle, tg.id id_global, l.id id_local, l.id as id,
              l.nbreParJour as nbre, l.couleur,FORMAT( l.tarif,2) AS tarif, d.id  AS duree_id,
              'true' AS isSelected ,d.name AS duree_name,
              IDENTITY(l.centreSoin) as centreSoin,
              IDENTITY(l.familleActe) as familleActe ,
              IDENTITY(l.duree) as duree ")
            ///->innerJoin(TypeConsultation::class,'l', 'WITH','l.idGlobal = t.id' )

            ->innerJoin(TypeConsultation::class,'l', 'WITH','l.idGlobal = t.id')
            ->leftJoin(Duree::class,'d', 'WITH','l.duree = d.id')
            ->leftJoin(TypeConsultationGlobal::class,'tg', 'WITH','tg.id = l.idGlobal')
            //->andWhere('a.id = :id')
            ->andWhere('l.centreSoin = :centreSoin')
            ->andWhere('l.active = 1')
            ->setParameter('centreSoin', $centreSoin)
            ->getQuery()
            ->getResult()
        ;
    }

 
    
}
