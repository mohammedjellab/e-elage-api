<?php

namespace App\Repository;

use App\Entity\ModeleConsultation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModeleConsultation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModeleConsultation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModeleConsultation[]    findAll()
 * @method ModeleConsultation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModeleConsultationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModeleConsultation::class);
    }

    // /**
    //  * @return ModeleConsultation[] Returns an array of ModeleConsultation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModeleConsultation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
