<?php

namespace App\Repository;

use App\Entity\Specialite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Specialite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Specialite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Specialite[]    findAll()
 * @method Specialite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecialiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Specialite::class);
    }

    // /**
    //  * @return Specialite[] Returns an array of Specialite objects
    //  */
    
    public function findAllSpecialite()
    {
        return $this->createQueryBuilder('s')
            ->select('s.name as libelle , s.id')
            ->andWhere('s.active = true ')
            //->andWhere('s.exampleField = :val')
            //->setParameter('val', $value)
            ->orderBy('s.name', 'ASC')
           // ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    /**/

    public function searchByName($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.libelle like :val')
            ->setParameter('val', "%$value%")
            ->getQuery()
            ->getResult()
        ;
    }
}
