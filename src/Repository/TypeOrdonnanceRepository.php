<?php

namespace App\Repository;

use App\Entity\TypeOrdonnance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeOrdonnance|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOrdonnance|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOrdonnance[]    findAll()
 * @method TypeOrdonnance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOrdonnanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeOrdonnance::class);
    }

    // /**
    //  * @return TypeOrdonnance[] Returns an array of TypeOrdonnance objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeOrdonnance
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
