<?php

namespace App\Repository;

use App\Entity\Holiday;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Holiday|null find($id, $lockMode = null, $lockVersion = null)
 * @method Holiday|null findOneBy(array $criteria, array $orderBy = null)
 * @method Holiday[]    findAll()
 * @method Holiday[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HolidayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Holiday::class);
    }

    // /**
    //  * @return Holiday[] Returns an array of Holiday objects
    //  */
    //pour afficher dans paramétrage
    public function findAllHoliday()
    {
        return $this->createQueryBuilder('h')
            ->select("h.start as start2,h.end as end2, DATE_FORMAT(h.start,'%H:%i') heureStart, DATE_FORMAT(h.end,'%H:%i') heureEnd, h.type ,  h.couleur")
            //->andWhere('h.exampleField = :val')
            //->setParameter('val', $value)
            //->orderBy('h.id', 'ASC')
            //->setMaxResults(10)DATE_FORMAT(h.end,'%d/%m/%Y %H:%i') end,
            ->getQuery()
            ->getResult()
        ;
    }
    ///pour aficher dans agenda
    public function getHolidays(){
        return $this->createQueryBuilder('h')
            ->select("h.start as start2, h.end as end2 , h.type ,  h.couleur")
            //->andWhere('h.exampleField = :val')
            //->setParameter('val', $value)
            //->orderBy('h.id', 'ASC')
            //->setMaxResults(10)DATE_FORMAT(h.end,'%d/%m/%Y %H:%i') end,
            ->getQuery()
            ->getResult()
        ;
    }
   /* */

    /*
    public function findOneBySomeField($value): ?Holiday
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
