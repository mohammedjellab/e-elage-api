<?php

namespace App\Repository;

use App\Entity\MedicamentFamille;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MedicamentFamille|null find($id, $lockMode = null, $lockVersion = null)
 * @method MedicamentFamille|null findOneBy(array $criteria, array $orderBy = null)
 * @method MedicamentFamille[]    findAll()
 * @method MedicamentFamille[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicamentFamilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MedicamentFamille::class);
    }

    // /**
    //  * @return MedicamentFamille[] Returns an array of MedicamentFamille objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MedicamentFamille
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
