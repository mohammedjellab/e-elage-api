<?php

namespace App\Repository;

use App\Entity\Medicament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Medicament|null find($id, $lockMode = null, $lockVersion = null)
 * @method Medicament|null findOneBy(array $criteria, array $orderBy = null)
 * @method Medicament[]    findAll()
 * @method Medicament[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicamentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Medicament::class);
    }

    public function findByActive(bool $active = true) {
        $qb = $this->createQueryBuilder('m')
            /*->select('m.id, m.designation, m.posologie, m.forme, m.prixVente, m.prixAchat, m.prixBRemboursementPPM, m.prixHospitalier, m.baseRembousementPPV')
            ->from('medicament', 'm')
            /*->innerJoin('m.dcis', 'd')
            ->where('m.dcis = d.id')
            ->andWhere('m.active = ?1')*/
            ->where('m.active = ?1')
            ->setParameter('1', $active);
        $query = $qb->getQuery();
        return $query->getResult();
            
    }


    // /**
    //  * @return Medicament[] Returns an array of Medicament objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Medicament
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function searchInDesignationAndClass($query)
    {
        $conn = $this->getEntityManager()
        ->getConnection();
        $sql = '
            SELECT id, name, type from (
                SELECT 
                    id, designation as name, "medicament" as "type" 
                FROM 
                    `medicament`        
                UNION
                    SELECT id, name, "labo" as "type" from laboratoire
            
            ) as t
            WHERE 
                name LIKE :query 
            ORDER BY 
                type ASC
        ';
        $stmt = $conn
        ->prepare($sql);
        $stmt->bindValue(':query', "%" . $query . "%");
        return $stmt->executeQuery();
    }
}
