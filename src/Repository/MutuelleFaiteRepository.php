<?php

namespace App\Repository;

use App\Entity\Mutuelle;
use App\Entity\MutuelleFaite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MutuelleFaite|null find($id, $lockMode = null, $lockVersion = null)
 * @method MutuelleFaite|null findOneBy(array $criteria, array $orderBy = null)
 * @method MutuelleFaite[]    findAll()
 * @method MutuelleFaite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MutuelleFaiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MutuelleFaite::class);
    }

    // /**
    //  * @return MutuelleFaite[] Returns an array of MutuelleFaite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByPatient($patient)
    {
        return $this->createQueryBuilder('m')
            ->select("m.id ,m.faite as remplie , m.valider, DATE_FORMAT(m.date,'%m/%d/%Y') as date, c.libelle ")
            //->leftJoin(TypeConsultation::class, 't', \Doctrine\ORM\Query\Expr\Join::WITH,'c.typeConsultation = t.id')
            ->innerJoin(Mutuelle::class,"c")
            ->where("c.id = m.mutuelle")
            ->andWhere('m.lpatient= :val')
            ->setParameter('val',$patient)
            ->getQuery()
            ->getResult()
        ;
    }
}
