<?php

namespace App\Repository;

use App\Entity\ConsultationHasReglemnt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConsultationHasReglemnt|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConsultationHasReglemnt|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConsultationHasReglemnt[]    findAll()
 * @method ConsultationHasReglemnt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsultationHasReglemntRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsultationHasReglemnt::class);
    }

    // /**
    //  * @return ConsultationHasReglemnt[] Returns an array of ConsultationHasReglemnt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConsultationHasReglemnt
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
