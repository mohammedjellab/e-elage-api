<?php

namespace App\Repository;

use App\Entity\MotifConsultation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MotifConsultation|null find($id, $lockMode = null, $lockVersion = null)
 * @method MotifConsultation|null findOneBy(array $criteria, array $orderBy = null)
 * @method MotifConsultation[]    findAll()
 * @method MotifConsultation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotifConsultationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MotifConsultation::class);
    }

    // /**
    //  * @return MotifConsultation[] Returns an array of MotifConsultation objects
    //  */
    /**/
    public function findAllMotif()
    {
        return $this->createQueryBuilder('m')
            ->select('m.libelle, m.id ')
            ->andWhere('m.active = 1')
            //->andWhere('m.centreSoin = :val')
           // ->setParameter('val', $centre)
            
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?MotifConsultation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
