<?php

namespace App\Repository;

use App\Entity\TypeCrOperatoire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeCrOperatoire|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCrOperatoire|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCrOperatoire[]    findAll()
 * @method TypeCrOperatoire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCrOperatoireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeCrOperatoire::class);
    }

    // /**
    //  * @return TypeCrOperatoire[] Returns an array of TypeCrOperatoire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeCrOperatoire
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
