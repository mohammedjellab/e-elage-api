<?php

namespace App\Repository;

use App\Entity\HistoPaiement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HistoPaiement|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoPaiement|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoPaiement[]    findAll()
 * @method HistoPaiement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoPaiementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoPaiement::class);
    }

    // /**
    //  * @return HistoPaiement[] Returns an array of HistoPaiement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HistoPaiement
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
