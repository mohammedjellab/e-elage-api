<?php

namespace App\Repository;

use App\Entity\AgendaConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AgendaConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgendaConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgendaConfig[]    findAll()
 * @method AgendaConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgendaConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgendaConfig::class);
    }

    // /**
    //  * @return AgendaConfig[] Returns an array of AgendaConfig objects
    //  */
    /* */
    public function configByCentreSoin($value)
    {
        return $this->createQueryBuilder('a')
            ->select("a.id, a.firstDay, a.modeAffichage,  DATE_FORMAT(a.heureOuverture, '%H:%i:%s') AS heureOuverture,
                      DATE_FORMAT(a.heureFermeture, '%H:%i:%s') AS heureFermeture,
                      DATE_FORMAT(a.heureDebutOuvrable, '%H:%i:%s') AS heureDebutOuvrable,
                      DATE_FORMAT(a.heureFinOuvrable, '%H:%i:%s') AS heureFinOuvrable,
                      a.maxActe ,
                      DATE_FORMAT(a.granularite, '%H:%i:%s') granularite, a.zoom")
            ->andWhere('a.centreSoin = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
   

    /*
    public function findOneBySomeField($value): ?AgendaConfig
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
