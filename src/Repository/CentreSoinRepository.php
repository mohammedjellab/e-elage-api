<?php

namespace App\Repository;

use App\Entity\CentreSoin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CentreSoin|null find($id, $lockMode = null, $lockVersion = null)
 * @method CentreSoin|null findOneBy(array $criteria, array $orderBy = null)
 * @method CentreSoin[]    findAll()
 * @method CentreSoin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CentreSoinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CentreSoin::class);
    }

    // /**
    //  * @return CentreSoin[] Returns an array of CentreSoin objects
    //  */
    /**/
    public function findAllEtablissment()
    {
        return $this->createQueryBuilder('c')
            ->select('c.etablissement, c.activite, c.adresse, c.ice, c.inpe, c.code,c.id')
            /*->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)*/
            ->getQuery()
            ->getResult()
        ;
    }


    public function findPharmacies($query)
    {
        return $this->createQueryBuilder('c')
            ->select('c.etablissement as name, c.id, c.code')
            ->innerJoin('App\Entity\CentreSoinType', 'g', 'WITH','c.type = g.id')
            ->andWhere('g.type = :type')
            ->setParameter('type', "Pharmacie")
            ->andWhere('c.etablissement LIKE :query')
            ->setParameter('query', "%".$query."%")
            ->getQuery()
            ->getResult()
        ;
    }

    public function findLaboratories($query)
    {
        return $this->createQueryBuilder('c')
            ->select('c.etablissement as name, c.id, c.code')
            ->innerJoin('App\Entity\CentreSoinType', 'g', 'WITH','c.type = g.id')
            ->andWhere('g.type = :type')
            ->setParameter('type', "Laboratoire")
            ->andWhere('c.etablissement LIKE :query')
            ->setParameter('query', "%".$query."%")
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?CentreSoin
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
