<?php

namespace App\Repository;

use App\Entity\ModeleFacture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModeleFacture|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModeleFacture|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModeleFacture[]    findAll()
 * @method ModeleFacture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModeleFactureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModeleFacture::class);
    }

    // /**
    //  * @return ModeleFacture[] Returns an array of ModeleFacture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModeleFacture
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
