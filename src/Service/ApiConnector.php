<?php 
namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class ApiConnector
{

        //private $url ="192.168.123.214";//CCAB
        //private $url ="192.168.14.130";//CJO_demohis
        //private $url ="192.168.153.3";//Achat
        private $url ="192.168.137.13"; //local
        //private $url ="192.168.183.3"; //cioc

    private  function initWebService(){
        set_time_limit(-1);
        $ch = curl_init();
        return $ch;
    }
    public function useWebService($name){
        $ch = $this->initWebService();
        curl_setopt($ch, CURLOPT_URL, "http://$this->url/webservice/interfacage/$name");
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-type: application/json;charset=UTF-8"));
        curl_setopt($ch, CURLOPT_POST, 1 );
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,100);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        $postResult = utf8_decode(curl_exec($ch));
        $postResult = json_decode($postResult,true);
        return $postResult;
    }

  

    

    public function useWebServiceAndSendParam($name,$object){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://$this->url/webservice/interfacage/$name");    
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($ch, CURLOPT_POST, 1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $object);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);    
        $postResult = curl_exec($ch);    
        $postResult = json_decode($postResult,true);
        return $postResult;
    }
    public function transformJsonBody(Request $request){
        $data = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return null;
        }
        if ($data === null) {
            return $request;
        }
        $request->request->replace($data);
        return $request;
    }

}